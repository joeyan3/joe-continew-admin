/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgrapi.ums.merchant;

import top.continew.admin.mgr.ums.service.BalanceService;
import top.continew.starter.extension.crud.enums.Api;

import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.web.bind.annotation.*;

import top.continew.starter.extension.crud.annotation.CrudRequestMapping;
import top.continew.starter.extension.crud.controller.BaseController;
import top.continew.admin.common.model.query.BalanceQuery;
import top.continew.admin.common.model.req.BalanceReq;
import top.continew.admin.common.model.resp.BalanceDetailResp;
import top.continew.admin.common.model.resp.BalanceResp;

/**
 * 余额管理 API
 *
 * @author joe
 * @since 2024/07/21 22:04
 */
@Tag(name = "余额管理 API")
@RestController
@CrudRequestMapping(value = "/system/balance", api = {Api.PAGE, Api.GET, Api.ADD, Api.UPDATE, Api.DELETE, Api.EXPORT})
public class BalanceController extends BaseController<BalanceService, BalanceResp, BalanceDetailResp, BalanceQuery, BalanceReq> {}