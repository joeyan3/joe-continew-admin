/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgrapi.ums.store;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import top.continew.admin.mgr.ums.service.ShippingTemplatesRegionService;
import top.continew.starter.extension.crud.enums.Api;

import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.web.bind.annotation.*;

import top.continew.starter.extension.crud.annotation.CrudRequestMapping;
import top.continew.starter.extension.crud.controller.BaseController;
import top.continew.admin.common.model.query.ShippingTemplatesRegionQuery;
import top.continew.admin.common.model.req.ShippingTemplatesRegionReq;
import top.continew.admin.common.model.resp.ShippingTemplatesRegionDetailResp;
import top.continew.admin.common.model.resp.ShippingTemplatesRegionResp;
import top.continew.starter.web.model.R;

import java.util.List;

/**
 * 运费模板指定区域费用管理 API
 *
 * @author joe
 * @since 2024/08/08 12:14
 */
@Tag(name = "mgr 运费模板指定区域费用管理 API")
@RestController
@CrudRequestMapping(value = "/store/shipping/region", api = {Api.PAGE, Api.GET, Api.ADD, Api.UPDATE, Api.DELETE,
    Api.EXPORT})
public class ShippingTemplatesRegionController extends BaseController<ShippingTemplatesRegionService, ShippingTemplatesRegionResp, ShippingTemplatesRegionDetailResp, ShippingTemplatesRegionQuery, ShippingTemplatesRegionReq> {
    @Operation(summary = "根据模板id查询数据", description = "根据模板id查询数据")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @GetMapping("/{id}/list")
    public R<List<ShippingTemplatesRegionResp>> getList(@PathVariable Long id) {
        return R.ok(baseService.getListGroup(id));
    }
}