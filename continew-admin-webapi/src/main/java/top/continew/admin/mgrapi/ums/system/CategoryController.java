/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgrapi.ums.system;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.springframework.validation.annotation.Validated;
import top.continew.admin.common.model.entity.CategoryDO;
import top.continew.admin.common.model.vo.CategoryTreeVo;
import top.continew.admin.common.util.CommonUtil;
import top.continew.starter.extension.crud.enums.Api;

import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.web.bind.annotation.*;

import top.continew.starter.extension.crud.annotation.CrudRequestMapping;
import top.continew.starter.extension.crud.controller.BaseController;
import top.continew.admin.common.model.query.CategoryQuery;
import top.continew.admin.common.model.req.CategoryReq;
import top.continew.admin.common.model.resp.CategoryDetailResp;
import top.continew.admin.common.model.resp.CategoryResp;
import top.continew.admin.mgr.ums.service.CategoryService;
import top.continew.starter.web.model.R;

import java.util.List;

/**
 * 分类管理 API
 *
 * @author joe
 * @since 2024/08/14 13:03
 */
@Tag(name = "mgr 分类管理 API")
@RestController
@CrudRequestMapping(value = "/system/category", api = {Api.PAGE, Api.GET, Api.ADD, Api.UPDATE, Api.DELETE, Api.EXPORT})
public class CategoryController extends BaseController<CategoryService, CategoryResp, CategoryDetailResp, CategoryQuery, CategoryReq> {

    @Operation(summary = "新增分类表", description = "新增分类表")
    @PostMapping("/create")
    public R<Void> createNew(@Validated @RequestBody CategoryReq req) {
        if (baseService.create(req)) {
            return R.ok("新增分类成功");
        } else {
            return R.fail("新增分类失败");
        }
    }

    @Operation(summary = "删除分类", description = "删除分类")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @PatchMapping("/{id}/delete")
    public R<Void> delete(@PathVariable Long id) {
        if (baseService.delete(id) > 0) {
            return R.ok("删除分类成功");
        } else {
            return R.fail("删除分类失败");
        }
    }

    @Operation(summary = "修改分类", description = "修改分类")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @PatchMapping("/{id}/update")
    public R<Void> updateTemplate(@Validated @RequestBody CategoryReq req, @PathVariable Long id) {
        //      todo 添加systemAttachmentService
        //      req.setExtra(systemAttachmentService.clearPrefix(req.getExtra()));

        baseService.updateCategory(req, id);
        return R.ok("修改模板信息成功");
    }

    @Operation(summary = "获取分类信息", description = "获取分类信息")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @GetMapping("/{id}/info")
    public R<CategoryDetailResp> getInfo(@PathVariable Long id) {
        return R.ok(baseService.get(id));
    }

    @Operation(summary = "获取tree结构的列表", description = "获取tree结构的列表")
    @GetMapping("/list/tree")
    public R<List<CategoryTreeVo>> getListTree(@RequestParam(name = "type") Integer type,
                                               @RequestParam(name = "status") Integer status,
                                               @RequestParam(name = "name", required = false) String name) {
        List<CategoryTreeVo> listTree = baseService.getListTree(type, status, name);
        return R.ok(listTree);
    }

    @Operation(summary = "根据id集合获取分类列表", description = "根据id集合获取分类列表")
    @GetMapping("/list/ids")
    public R<List<CategoryDO>> getByIds(@Validated @RequestParam(name = "ids") String ids) {
        return R.ok(baseService.getByIds(CommonUtil.stringToArrayLong(ids)));
    }

    @Operation(summary = "更改分类状态", description = "更改分类状态")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @PatchMapping("/{id}/update/status")
    public R<Void> updateStatus(@PathVariable Long id) {

        if (baseService.updateStatus(id)) {
            return R.ok("更改分类状态成功");
        } else {
            return R.fail("更改分类状态失败");
        }
    }

}