/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgrapi.ums.merchant;

import top.continew.admin.mgr.ums.service.PersonShopIdentificationService;
import top.continew.starter.extension.crud.enums.Api;

import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.web.bind.annotation.*;

import top.continew.starter.extension.crud.annotation.CrudRequestMapping;
import top.continew.starter.extension.crud.controller.BaseController;
import top.continew.admin.common.model.query.PersonShopIdentificationQuery;
import top.continew.admin.common.model.req.PersonShopIdentificationReq;
import top.continew.admin.common.model.resp.PersonShopIdentificationDetailResp;
import top.continew.admin.common.model.resp.PersonShopIdentificationResp;

/**
 * 商铺认证管理 API
 *
 * @author joe
 * @since 2024/07/30 15:48
 */
@Tag(name = "商铺认证管理 API")
@RestController
@CrudRequestMapping(value = "/merchant/personShopIdentification", api = {Api.PAGE, Api.GET, Api.ADD, Api.UPDATE,
    Api.DELETE, Api.EXPORT})
public class PersonShopIdentificationController extends BaseController<PersonShopIdentificationService, PersonShopIdentificationResp, PersonShopIdentificationDetailResp, PersonShopIdentificationQuery, PersonShopIdentificationReq> {}