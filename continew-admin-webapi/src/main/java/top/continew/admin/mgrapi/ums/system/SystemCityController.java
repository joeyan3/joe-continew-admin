/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgrapi.ums.system;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.continew.admin.common.model.entity.SystemCityDO;
import top.continew.admin.common.model.query.SystemCityQuery;
import top.continew.admin.common.model.req.SystemCityReq;
import top.continew.admin.common.model.resp.SystemCityDetailResp;
import top.continew.admin.common.model.resp.SystemCityResp;
import top.continew.admin.common.model.vo.SystemCityTreeVO;
import top.continew.admin.mgr.ums.service.SystemCityService;
import top.continew.starter.extension.crud.annotation.CrudRequestMapping;
import top.continew.starter.extension.crud.controller.BaseController;
import top.continew.starter.extension.crud.enums.Api;
import top.continew.starter.extension.crud.model.query.PageQuery;
import top.continew.starter.extension.crud.model.resp.PageResp;
import top.continew.starter.web.model.R;

import java.util.List;

/**
 * 城市表 API
 *
 * @author joe
 * @since 2024/08/08 12:14
 */
@Tag(name = "mgr 城市表 API")
@RestController
@CrudRequestMapping(value = "/system/city", api = {Api.PAGE, Api.GET, Api.ADD, Api.UPDATE, Api.DELETE, Api.EXPORT})
public class SystemCityController extends BaseController<SystemCityService, SystemCityResp, SystemCityDetailResp, SystemCityQuery, SystemCityReq> {
    @Override
    public R<PageResp<SystemCityResp>> page(SystemCityQuery query, @Validated PageQuery pageQuery) {
        return R.ok(baseService.page(query, pageQuery));
    }

    @Operation(summary = "修改城市", description = "修改城市")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @PatchMapping("/{id}/update")
    public R<Void> updateSystemCity(@Validated @RequestBody SystemCityReq req, @PathVariable Long id) {
        baseService.update(id, req);
        return R.ok("修改城市成功");
    }

    @Operation(summary = "修改城市状态", description = "修改城市状态")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @PatchMapping("/{id}/update/status")
    public R<Void> updateSystemCityStatus(@Validated @RequestBody SystemCityReq req, @PathVariable Long id) {
        baseService.updateStatus(id, req.getIsShow());
        return R.ok("修改城市状态成功");
    }

    @Operation(summary = "城市详情", description = "城市详情")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @GetMapping("/{id}/info")
    public R<SystemCityDO> getSystemCityInfo(@PathVariable Long id) {
        return R.ok(baseService.getCityByCityId(id));
    }

    @Operation(summary = "获取tree结构的列表", description = "获取tree结构的列表")
    @GetMapping("/list/tree")
    public R<List<SystemCityTreeVO>> getSystemCityTree() {
        return R.ok(baseService.getListTree());
    }

    @Operation(summary = "获取tree结构的父列表", description = "获取tree结构的列表")
    @GetMapping("/list/tree/top")
    public R<List<SystemCityTreeVO>> getSystemCityTreeTop() {
        baseService.getListTree();
        return R.ok(baseService.getListTreeTop());
    }

    @Operation(summary = "获取tree结构的子列表", description = "获取tree结构的列表")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @GetMapping("/{id}/list/tree/child")
    public R<List<SystemCityTreeVO>> getSystemCityTreeChild(@PathVariable Long id) {
        return R.ok(baseService.getListTreeChild(id));
    }

    @Operation(summary = "获取顶层节点下所有的子节点", description = "获取顶层节点下所有的子节点")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @GetMapping("/{id}/list/tree/descendants")
    public R<List<SystemCityTreeVO>> getAllDescendants(@PathVariable Long id) {
        return R.ok(baseService.getAllDescendants(id));
    }

}