/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgrapi.ums.store;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.springframework.validation.annotation.Validated;
import top.continew.admin.common.model.resp.*;
import top.continew.admin.mgr.ums.service.StoreProductService;
import top.continew.starter.extension.crud.enums.Api;

import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.web.bind.annotation.*;

import top.continew.starter.extension.crud.annotation.CrudRequestMapping;
import top.continew.starter.extension.crud.controller.BaseController;
import top.continew.admin.common.model.query.StoreProductQuery;
import top.continew.admin.common.model.req.StoreProductReq;
import top.continew.starter.extension.crud.model.query.PageQuery;
import top.continew.starter.extension.crud.model.resp.PageResp;
import top.continew.starter.web.model.R;

import java.util.List;

/**
 * 商品管理 API
 *
 * @author joe
 * @since 2024/08/17 17:00
 */
@Tag(name = "mgr 商品管理 API")
@RestController
@CrudRequestMapping(value = "/store/product", api = {Api.PAGE, Api.GET, Api.ADD, Api.UPDATE, Api.DELETE, Api.EXPORT})
public class StoreProductController extends BaseController<StoreProductService, StoreProductResp, StoreProductDetailResp, StoreProductQuery, StoreProductReq> {
    @Operation(summary = "新增商品", description = "新增商品")
    @PostMapping("/create")
    public R<Void> createNew(@Validated @RequestBody StoreProductReq req) {

        if (baseService.save(req)) {
            return R.ok("新增商品成功！");
        } else {
            return R.fail("新增商品失败");
        }
    }

    @Operation(summary = "商品修改", description = "商品修改")
    @PostMapping("/update")
    public R<Void> update(@Validated @RequestBody StoreProductReq req) {

        if (baseService.update(req)) {
            return R.ok("编辑商品成功！");
        } else {
            return R.fail("编辑商品失败");
        }
    }

    @Operation(summary = "商品详情", description = "商品详情")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @GetMapping("/{id}/info")
    public R<StoreProductInfoResp> getInfo(@PathVariable Long id) {
        return R.ok(baseService.getInfo(id));
    }

    @Operation(summary = "商品表头数量", description = "商品表头数量")
    @GetMapping("/tabs/headers")
    public R<List<StoreProductTabsHeaderResp>> getTabsHeader() {
        return R.ok(baseService.getTabsHeader());
    }

    @Operation(summary = "下架商品", description = "下架商品")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @PatchMapping("/{id}/shell/off")
    public R<Void> offShell(@PathVariable Long id) {
        if (baseService.offShelf(id)) {
            return R.ok("商品下架成功！");
        } else {
            return R.fail("商品下架失败");
        }

    }

    @Operation(summary = "上架商品", description = "上架商品")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @GetMapping("/{id}/shell/on")
    public R<Void> onShell(@PathVariable Long id) {
        if (baseService.putOnShelf(id)) {
            return R.ok("商品上架成功！");
        } else {
            return R.fail("商品上架失败");
        }

    }

    @Override
    public R<PageResp<StoreProductResp>> page(StoreProductQuery query, @Validated PageQuery pageQuery) {
        return R.ok(baseService.getAdminList(query, pageQuery));
    }

}