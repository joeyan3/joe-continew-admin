/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgrapi.ums.store;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.validation.annotation.Validated;
import top.continew.admin.mgr.ums.service.StoreCouponService;
import top.continew.starter.extension.crud.enums.Api;

import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.web.bind.annotation.*;

import top.continew.starter.extension.crud.annotation.CrudRequestMapping;
import top.continew.starter.extension.crud.controller.BaseController;
import top.continew.admin.common.model.query.StoreCouponQuery;
import top.continew.admin.common.model.req.StoreCouponReq;
import top.continew.admin.common.model.resp.StoreCouponDetailResp;
import top.continew.admin.common.model.resp.StoreCouponResp;
import top.continew.starter.web.model.R;

/**
 * 优惠券管理 API
 *
 * @author joe
 * @since 2024/08/19 16:14
 */
@Tag(name = "mgr 优惠券管理 API")
@RestController
@CrudRequestMapping(value = "/store/coupon", api = {Api.PAGE, Api.GET, Api.ADD, Api.UPDATE, Api.DELETE, Api.EXPORT})
public class StoreCouponController extends BaseController<StoreCouponService, StoreCouponResp, StoreCouponDetailResp, StoreCouponQuery, StoreCouponReq> {
    @Operation(summary = "新增优惠券", description = "新增优惠券")
    @PostMapping("/create")
    public R<Void> createNew(@Validated @RequestBody StoreCouponReq req) {
        if (baseService.create(req)) {
            return R.ok("新增优惠券成功！");
        }
        return R.fail("新增优惠券失败");
    }
}