/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgrapi.ums.merchant;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import top.continew.admin.common.util.SecureUtils;
import top.continew.admin.common.model.query.AccountQuery;
import top.continew.admin.common.model.resp.AccountDetailResp;
import top.continew.admin.common.model.resp.AccountResp;
import top.continew.admin.common.model.req.AccountAddReq;
import top.continew.admin.common.model.req.AccountPasswordResetReq;
import top.continew.admin.common.model.req.AccountPasswordUpdateReq;
import top.continew.admin.common.model.req.AccountReq;
import top.continew.admin.mgr.ums.service.AccountService;
import top.continew.admin.mgr.ums.service.BalanceService;
import top.continew.admin.mgr.ums.service.InvitationCodeService;
import top.continew.starter.core.util.ExceptionUtils;
import top.continew.starter.core.util.validate.ValidationUtils;
import top.continew.starter.extension.crud.annotation.CrudRequestMapping;
import top.continew.starter.extension.crud.controller.BaseController;
import top.continew.starter.extension.crud.enums.Api;

import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.web.bind.annotation.*;
import top.continew.starter.extension.crud.model.query.PageQuery;
import top.continew.starter.extension.crud.model.resp.PageResp;
import top.continew.starter.web.model.R;

import java.lang.reflect.InvocationTargetException;

/**
 * 商户列表管理 API
 *
 * @author joe
 * @since 2024/07/17 12:14
 */
@Tag(name = "商户列表管理 API")
@Validated
@RequiredArgsConstructor
@RestController
@CrudRequestMapping(value = "/merchant/account", api = {Api.PAGE, Api.GET, Api.ADD, Api.UPDATE, Api.DELETE, Api.EXPORT})
public class AccountController extends BaseController<AccountService, AccountResp, AccountDetailResp, AccountQuery, AccountReq> {
    private static final String DECRYPT_FAILED = "当前密码解密失败";
    private final BalanceService balanceService;
    private final InvitationCodeService invitationCodeService;

    @Override
    public R<PageResp<AccountResp>> page(AccountQuery query, @Validated PageQuery pageQuery) {
        return R.ok(baseService.page(query, pageQuery));
    }

    @Operation(summary = "添加新商户", description = "添加新商户")
    @PostMapping("/register")
    public R<Void> registerNewAccount(@Validated @RequestBody AccountAddReq req) throws InvocationTargetException, IllegalAccessException {
        String rawPassword = ExceptionUtils.exToNull(() -> SecureUtils.decryptByRsaPrivateKey(req.getPassword()));
        ValidationUtils.throwIfNull(rawPassword, "密码解密失败");
        //        ValidationUtils.throwIf(!ReUtil
        //                .isMatch(RegexConstants.PASSWORD, rawPassword), "密码长度为 8-32 个字符，支持大小写字母、数字、特殊字符，至少包含字母和数字");
        //
        req.setPassword(rawPassword);
        baseService.registerNewAccount(req);

        return R.ok("添加新商户成功");
    }

    @Operation(summary = "重置密码", description = "重置商户登录密码")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @PatchMapping("/{id}/reset/password")
    public R<Void> resetPassword(@Validated @RequestBody AccountPasswordResetReq req, @PathVariable Long id) {
        String rawNewPassword = ExceptionUtils.exToNull(() -> SecureUtils.decryptByRsaPrivateKey(req.getNewPassword()));
        ValidationUtils.throwIfNull(rawNewPassword, "新密码解密失败");
        //        ValidationUtils.throwIf(!ReUtil.isMatch(RegexConstants.PASSWORD, rawNewPassword), "密码长度为 8-32 个字符，支持大小写字母、数字、特殊字符，至少包含字母和数字");
        req.setNewPassword(rawNewPassword);
        baseService.resetPassword(req, id);
        return R.ok("重置密码成功");
    }

    @Operation(summary = "修改密码", description = "修改商户登录密码")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @PatchMapping("/{id}/update/password")
    public R<Void> updatePassword(@Validated @RequestBody AccountPasswordUpdateReq updateReq, @PathVariable Long id) {
        String rawOldPassword = ExceptionUtils.exToNull(() -> SecureUtils.decryptByRsaPrivateKey(updateReq
            .getOldPassword()));
        ValidationUtils.throwIfNull(rawOldPassword, DECRYPT_FAILED);
        String rawNewPassword = ExceptionUtils.exToNull(() -> SecureUtils.decryptByRsaPrivateKey(updateReq
            .getNewPassword()));
        ValidationUtils.throwIfNull(rawNewPassword, "新密码解密失败");
        baseService.updatePassword(rawOldPassword, rawNewPassword, id);
        return R.ok("修改成功，请牢记你的新密码");
    }

    @Operation(summary = "冻结商户", description = "冻结商户")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @PatchMapping("/{id}/freeze")
    public R<Void> freezeAccount(@PathVariable Long id) {
        baseService.freezeAccount(id);
        return R.ok("冻结商户成功");
    }

    @Operation(summary = "解冻商户", description = "解冻商户")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @PatchMapping("/{id}/unfreeze")
    public R<Void> unfreezeAccount(@PathVariable Long id) {
        baseService.unfreezeAccount(id);
        return R.ok("解冻商户成功");
    }

}