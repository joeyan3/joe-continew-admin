/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgrapi.ums.store;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import top.continew.admin.common.model.entity.ShippingTemplatesDO;
import top.continew.admin.mgr.ums.service.ShippingTemplatesService;
import top.continew.starter.extension.crud.enums.Api;

import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.web.bind.annotation.*;

import top.continew.starter.extension.crud.annotation.CrudRequestMapping;
import top.continew.starter.extension.crud.controller.BaseController;
import top.continew.admin.common.model.query.ShippingTemplatesQuery;
import top.continew.admin.common.model.req.ShippingTemplatesReq;
import top.continew.admin.common.model.resp.ShippingTemplatesDetailResp;
import top.continew.admin.common.model.resp.ShippingTemplatesResp;
import top.continew.starter.extension.crud.model.query.PageQuery;
import top.continew.starter.extension.crud.model.resp.PageResp;
import top.continew.starter.web.model.R;

import java.util.List;

/**
 * 运费模板管理 API
 *
 * @author joe
 * @since 2024/08/08 10:04
 */
@Tag(name = "mgr 运费模板管理 API")
@Validated
@RequiredArgsConstructor
@RestController
@CrudRequestMapping(value = "/store/shipping", api = {Api.PAGE, Api.GET, Api.ADD, Api.UPDATE, Api.DELETE, Api.EXPORT})
public class ShippingTemplatesController extends BaseController<ShippingTemplatesService, ShippingTemplatesResp, ShippingTemplatesDetailResp, ShippingTemplatesQuery, ShippingTemplatesReq> {
    @Override
    public R<PageResp<ShippingTemplatesResp>> page(ShippingTemplatesQuery query, @Validated PageQuery pageQuery) {
        return R.ok(baseService.page(query, pageQuery));
    }

    @Operation(summary = "获取所有模板", description = "获取所有模板")
    @GetMapping("/all")
    public R<List<ShippingTemplatesDO>> getAllTemplates() {

        return R.ok(baseService.getAll());
    }

    @Operation(summary = "新增模板", description = "新增模板")
    @PostMapping("/create")
    public R<Void> createNewTemplate(@Validated @RequestBody ShippingTemplatesReq req) {

        baseService.createNewTemplate(req);
        return R.ok("新增模板成功");
    }

    @Operation(summary = "修改模板信息", description = "修改模板信息")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @PatchMapping("/{id}/update")
    public R<Void> updateTemplate(@Validated @RequestBody ShippingTemplatesReq req, @PathVariable Long id) {
        baseService.updateTemplate(id, req);
        return R.ok("修改模板信息成功");
    }

    @Operation(summary = "获取模板信息", description = "获取模板信息")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @GetMapping("/{id}/info")
    public R<ShippingTemplatesDO> getInfo(@PathVariable Long id) {
        return R.ok(baseService.getInfo(id));
    }

    @Operation(summary = "删除运费模板", description = "删除运费模板")
    @Parameter(name = "id", description = "ID", example = "1", in = ParameterIn.PATH)
    @PatchMapping("/{id}/remove")
    public R<Void> removeTemplate(@PathVariable Long id) {
        baseService.remove(id);
        return R.ok("删除运费模板成功");
    }

}