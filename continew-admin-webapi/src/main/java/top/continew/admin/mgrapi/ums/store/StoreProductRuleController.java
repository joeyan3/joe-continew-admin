/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgrapi.ums.store;

import io.swagger.v3.oas.annotations.Operation;
import top.continew.admin.common.model.entity.StoreProductRuleDO;
import top.continew.admin.mgr.ums.service.StoreProductRuleService;
import top.continew.starter.extension.crud.enums.Api;

import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.web.bind.annotation.*;

import top.continew.starter.extension.crud.annotation.CrudRequestMapping;
import top.continew.starter.extension.crud.controller.BaseController;
import top.continew.admin.common.model.query.StoreProductRuleQuery;
import top.continew.admin.common.model.req.StoreProductRuleReq;
import top.continew.admin.common.model.resp.StoreProductRuleDetailResp;
import top.continew.admin.common.model.resp.StoreProductRuleResp;
import top.continew.starter.web.model.R;

import java.util.List;

/**
 * 商品规则值(规格)管理 API
 *
 * @author joe
 * @since 2024/09/09 08:38
 */
@Tag(name = "mgr 商品规则值(规格)管理 API")
@RestController
@CrudRequestMapping(value = "/store/product/rule", api = {Api.PAGE, Api.GET, Api.ADD, Api.UPDATE, Api.DELETE,
    Api.EXPORT})
public class StoreProductRuleController extends BaseController<StoreProductRuleService, StoreProductRuleResp, StoreProductRuleDetailResp, StoreProductRuleQuery, StoreProductRuleReq> {

    @Operation(summary = "获取所有商品规则值(规格)", description = "获取所有商品规则值(规格)")
    @GetMapping("/all")
    public R<List<StoreProductRuleDO>> getAllProductRules() {

        return R.ok(baseService.getAll());
    }
}