/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.restapi.ums;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaIgnore;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.continew.admin.auth.model.resp.LoginResp;
import top.continew.admin.common.model.dto.LoginMember;
import top.continew.admin.common.model.dto.LoginUser;
import top.continew.admin.common.model.req.AccountAddReq;
import top.continew.admin.common.model.req.AccountRestLoginReq;
import top.continew.admin.common.util.SecureUtils;
import top.continew.admin.common.util.StpUserUtil;
import top.continew.admin.common.util.helper.LoginHelper;
import top.continew.admin.common.util.helper.LoginMemberHelper;
import top.continew.admin.rest.ums.service.MemberService;
import top.continew.admin.rest.ums.service.RestAccountService;
import top.continew.admin.common.model.resp.AccountDetailResp;
import top.continew.starter.core.util.ExceptionUtils;
import top.continew.starter.core.util.validate.ValidationUtils;
import top.continew.starter.log.core.annotation.Log;
import top.continew.starter.messaging.websocket.model.CurrentUser;
import top.continew.starter.web.model.R;

import java.lang.reflect.InvocationTargetException;

/**
 * 会员中心
 *
 * @author Charles7c
 * @since 2022/12/21 20:37
 */
@Log(module = "会员中心")
@Tag(name = "会员中心")
@RestController
@RequiredArgsConstructor
@RequestMapping("/ums")
public class MemberController {

    private final MemberService memberService;
    private final RestAccountService accountService;

    @SaIgnore
    @Operation(summary = "账号登录", description = "根据账号和密码进行登录认证")
    @PostMapping("/login")
    public R<LoginResp> accountLogin(@Validated @RequestBody AccountRestLoginReq loginReq, HttpServletRequest request) {
        // 用户登录
        String rawPassword = ExceptionUtils.exToNull(() -> SecureUtils.decryptByRsaPrivateKey(loginReq.getPassword()));
        ValidationUtils.throwIfBlank(rawPassword, "密码解密失败");
        String token = memberService.accountLogin(loginReq.getAccountName(), rawPassword, request);
        return R.ok(LoginResp.builder().token(token).build());
    }

    @SaIgnore
    @Operation(summary = "商户注册", description = "注册商户")
    @PostMapping("/registerNewAccount")
    public R<Void> registerNewAccount(@Validated @RequestBody AccountAddReq req) throws InvocationTargetException, IllegalAccessException {
        String rawPassword = ExceptionUtils.exToNull(() -> SecureUtils.decryptByRsaPrivateKey(req.getPassword()));
        ValidationUtils.throwIfNull(rawPassword, "密码解密失败");
        //        ValidationUtils.throwIf(!ReUtil
        //                .isMatch(RegexConstants.PASSWORD, rawPassword), "密码长度为 8-32 个字符，支持大小写字母、数字、特殊字符，至少包含字母和数字");
        //
        req.setPassword(rawPassword);
        accountService.registerNewAccount(req);

        return R.ok("添加新商户成功");
    }

    @SaIgnore
    @Operation(summary = "获取验证码", description = "根据手机号发送验证码")
    @PostMapping("/code")
    public R<String> getCode(@Validated @Parameter String accountName) {
        boolean flag = accountService.sendCode(accountName, "code:");
        if (flag) {
            return R.ok("验证码发送成功");
        }
        return R.fail("验证码发送失败");
    }

    // 通过type属性指定此注解校验的是我们自定义的`StpUserUtil`，而不是原生`StpUtil`
    @SaCheckLogin(type = StpUserUtil.TYPE)
    @Operation(summary = "用户信息", description = "获取前端用户信息")
    @PostMapping("/account/info")
    public R<AccountDetailResp> getAccountInfo() {
        LoginMember loginMember = LoginMemberHelper.getLoginUser();
        AccountDetailResp accountDetailResp = accountService.get(loginMember.getId());
        return R.ok(accountDetailResp);
    }

    @SaCheckLogin(type = StpUserUtil.TYPE)
    @Operation(summary = "注销用户", description = "前端用户注销账号")
    @PostMapping("/account/del")
    public R<String> delAccount(HttpServletRequest request) {
        String token = getTokenFromRequest(request);
        LoginMember loginMember = LoginMemberHelper.getLoginUser(token);
        boolean flag = accountService.softDelAccount(loginMember.getId());
        if (flag) {
            return R.ok("注销成功");
        }
        return R.fail("注销失败");
    }

    public String getTokenFromRequest(HttpServletRequest request) {
        String authHeader = request.getHeader("Authorization");
        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            return authHeader.substring(7); // 去掉 "Bearer " 前缀，获取 token
        }
        return null; // 或者抛出异常，根据你的需求
    }

    public CurrentUser getCurrentUser(ServletServerHttpRequest request) {
        HttpServletRequest servletRequest = request.getServletRequest();
        String token = servletRequest.getParameter("token");
        LoginUser loginUser = LoginHelper.getLoginUser(token);
        CurrentUser currentUser = new CurrentUser();
        currentUser.setUserId(loginUser.getToken());
        return currentUser;
    }

}