/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import top.continew.starter.data.mybatis.plus.base.IBaseEnum;

/**
 * 账户类型
 *
 * @author Charles7c
 * @since 2022/12/29 21:59
 */
@Getter
@RequiredArgsConstructor
public enum PayTypeEnum implements IBaseEnum<String> {

    /**
     * 注册赠送（积分）
     */
    REGISTER_BONUS("01", "注册赠送（积分）"),

    /**
     * 推荐赠送（积分）
     */
    REFERRAL_BONUS("02", "推荐赠送（积分）"),

    /**
     * 消费赠送（积分）
     */
    PURCHASE_BONUS("03", "消费赠送（积分）"),

    /**
     * 店铺收益（余额）10%
     */
    STORE_EARNINGS("04", "店铺收益（余额）10%"),

    /**
     * 商品购买
     */
    PRODUCT_PURCHASE("05", "商品购买"),

    /**
     * 商品退款
     */
    PRODUCT_REFUND("06", "商品退款"),

    /**
     * 余额提现
     */
    BALANCE_WITHDRAWAL("07", "余额提现"),

    /**
     * 系统充值
     */
    SYSTEM_RECHARGE("08", "系统充值"),

    /**
     * 每日签到
     */
    DAILY_SIGN_IN("09", "每日签到"),

    /**
     * 积分抵扣
     */
    POINTS_DEDUCTION("10", "积分抵扣");
    ;

    private final String value;
    private final String description;

}
