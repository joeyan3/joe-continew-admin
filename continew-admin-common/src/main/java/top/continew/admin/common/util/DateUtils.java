/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.util;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import top.continew.admin.common.constant.StoreConstants;

/**
 * @author nz
 * @Description: 日期处理
 * @ClassName: DateUtils
 * @date 2017年12月24日 下午3:56:02
 */
public class DateUtils {
    private static final TimeZone TIME_ZONE = TimeZone.getTimeZone("Etc/GMT-8");

    private static String[] formatters = new String[] {"yyyyMMddHHmmss", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm",
        "yyyy-MM-dd", "yyyy年MM月dd日", "yyyyMMdd", "yyyy-MM"};

    /**
     * 获取当前日期,指定格式
     * 描述:<描述函数实现的功能>.
     *
     * @return
     */
    public static Date nowDateTime() {
        return strToDate(nowDateTimeStr(), StoreConstants.DATE_FORMAT);
    }

    /**
     * compare two date String with a pattern
     *
     * @param date1
     * @param date2
     * @param pattern
     * @return
     */
    public static int compareDate(String date1, String date2, String pattern) {
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(pattern);
        try {
            Date dt1 = DATE_FORMAT.parse(date1);
            Date dt2 = DATE_FORMAT.parse(date2);
            if (dt1.getTime() > dt2.getTime()) {
                return 1;
            } else if (dt1.getTime() < dt2.getTime()) {
                return -1;
            } else {
                return 0;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * convert a date to string in a specifies fromat.
     *
     * @param date
     * @param DATE_FORMAT
     * @return
     */
    public static String dateToStr(Date date, String DATE_FORMAT) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat myFormat = new SimpleDateFormat(DATE_FORMAT);
        return myFormat.format(date);
    }

    /**
     * parse a String to Date in a specifies fromat.
     *
     * @param dateStr
     * @param DATE_FORMAT
     * @return
     * @throws ParseException
     */
    public static Date strToDate(String dateStr, String DATE_FORMAT) {
        SimpleDateFormat myFormat = new SimpleDateFormat(DATE_FORMAT);
        try {
            return myFormat.parse(dateStr);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 获取当前日期,指定格式
     * 描述:<描述函数实现的功能>.
     *
     * @return
     */
    public static String nowDateTimeStr() {
        return nowDate(StoreConstants.DATE_FORMAT);
    }

    /**
     * 获取当前日期,指定格式
     * 描述:<描述函数实现的功能>.
     *
     * @return
     */
    public static String nowDate(String DATE_FORMAT) {
        SimpleDateFormat dft = new SimpleDateFormat(DATE_FORMAT);
        return dft.format(new Date());
    }

    /**
     * 是否是当天
     *
     * @param date
     * @return
     */
    public static boolean isToday(Date date) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        if (fmt.format(date).toString().equals(fmt.format(new Date()).toString())) {//格式1653化为相版同格式
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取某个时间段内所有日期
     *
     * @param begin
     * @param end
     * @return
     * @throws ParseException
     */
    public static List<String> getDayBetweenDates(String begin, String end) throws ParseException {
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
        Date dBegin = sd.parse(begin);
        Date dEnd = sd.parse(end);
        List<String> lDate = new ArrayList<String>();
        lDate.add(sd.format(dBegin));
        Calendar calBegin = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calBegin.setTime(dBegin);
        Calendar calEnd = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calEnd.setTime(dEnd);
        // 测试此日期是否在指定日期之后
        while (dEnd.after(calBegin.getTime())) {
            // 根据日历的规则，为给定的日历字段添加或减去指定的时间量
            calBegin.add(Calendar.DAY_OF_MONTH, 1);
            lDate.add(sd.format(calBegin.getTime()));
        }
        return lDate;
    }

    /**
     * 判断时间是否在时间段内
     *
     * @param nowTime
     * @param beginTime
     * @param endTime
     * @return
     */
    public static boolean belongCalendar(Date nowTime, Date beginTime, Date endTime) {
        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);
        Calendar begin = Calendar.getInstance();
        begin.setTime(beginTime);
        Calendar end = Calendar.getInstance();
        end.setTime(endTime);
        if (date.after(begin) && date.before(end)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 图表中的日期格式，格式为“yyyy年MM月dd日”
     *
     * @return
     */
    public static DateFormat getDateFormat() {
        return DateFormat.getDateInstance(1, Locale.CHINA);
    }

    /**
     * 将日期格式化为"yyyy-MM"字符串
     *
     * @param date
     * @return
     */
    public static String formatYearMonth(Date date) {
        return doFormatDate(date, "yyyy-MM");
    }

    /**
     * 将日期格式化为"yyyyMMddHHmmss"字符串
     *
     * @return
     */
    public static String getCurrenDate() {
        return doFormatDate(new Date(), "yyyyMMddHHmmss");
    }

    public static Date convertToDate(String string, String formatString) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(formatString);
        return sdf.parse(string);
    }

    public static String formYYYMMDD(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatters[3]);
        return sdf.format(date);
    }

    public static String format(Date date, String formatString) {
        FastDateFormat format = FastDateFormat.getInstance(formatString, TimeZone.getTimeZone("Etc/GMT-8"));
        return format.format(date);
    }

    /**
     * 返回日期所在的季度
     *
     * @param date
     * @return
     */
    public static int getSeasons(Date date) {
        int m = getMonth(date);
        if (m <= 0) {
            return 0;
        } else if (m < 4) {
            return 1;
        } else if (m < 7) {
            return 2;
        } else if (m < 10) {
            return 3;
        } else if (m < 13) {
            return 4;
        } else {
            return 0;
        }
    }

    /**
     * 给定日期所在的季度，并返回该季度的第一天日期,如果指定日期错误，返回null
     *
     * @param date
     * @return
     */
    public static Date getNowSeasonsFirstDay(Date date) {
        int m = getSeasons(date);
        if (m > 0) {
            if (m == 1) {
                return stringToDate(getYear(date) + "-01-01");
            } else if (m == 2) {
                return stringToDate(getYear(date) + "-04-01");
            } else if (m == 3) {
                return stringToDate(getYear(date) + "-07-01");
            } else {
                return stringToDate(getYear(date) + "-10-01");
            }
        }
        return null;
    }

    /**
     * 得到某年的最后一天的日期
     *
     * @param year
     * @return
     */
    public static Date getYearLastDay(String year) {
        if (year == null || "".equals(year)) {
            return null;
        }
        Date nd = stringToDate(year + "-01-01");
        return addDay(addYear(nd, 1), -1);
    }

    /**
     * 得到下个月的第一天
     *
     * @param year
     * @param month
     * @return
     */
    public static Date getNextMonthFirstDay(String year, String month) {
        if (year == null || "".equals(year) || month == null || "".equals(month)) {
            return null;
        }
        Date nd = stringToDate(year + "-" + month + "-01");
        return addMonth(nd, 1);
    }

    /**
     * 得到某年月的最后一天的日期
     *
     * @param year
     * @param month
     * @return
     */
    public static Date getMonthLastDay(String year, String month) {
        if (year == null || "".equals(year) || month == null || "".equals(month)) {
            return null;
        }
        Date nd = stringToDate(year + "-" + month + "-01");
        return addDay(addMonth(nd, 1), -1);
    }

    /**
     * 给一个时间加上N秒钟或减去N秒钟后得到一个新的日期
     *
     * @param startDate 需要增加的日期时间
     * @param addnos    添加的秒钟数，可以是正数也可以是负数
     * @return 操作后的日期
     */
    public static Date addSecond(Date startDate, int addnos) {
        if (startDate == null) {
            return null;
        }
        Calendar cc = Calendar.getInstance();
        cc.setTime(startDate);
        cc.add(Calendar.SECOND, addnos);
        return cc.getTime();
    }

    /**
     * 给一个时间加上N分种或减去N分种后得到一个新的日期
     *
     * @param startDate 需要增加的日期时间
     * @param addnos    添加的分钟数，可以是正数也可以是负数
     * @return 操作后的日期
     */
    public static Date addMinute(Date startDate, int addnos) {
        if (startDate == null) {
            return null;
        }
        Calendar cc = Calendar.getInstance();
        cc.setTime(startDate);
        cc.add(Calendar.MINUTE, addnos);
        return cc.getTime();
    }

    /**
     * 给一个时间加上一定的小时得到一个新的日期
     *
     * @param startDate 需要增加的日期时间
     * @param addnos    添加的小时数，可以是正数也可以是负数
     * @return 操作后的日期
     */
    public static Date addHour(Date startDate, int addnos) {
        if (startDate == null) {
            return null;
        }
        Calendar cc = Calendar.getInstance();
        cc.setTime(startDate);
        cc.add(Calendar.HOUR, addnos);
        return cc.getTime();
    }

    /**
     * 给一个日期加上N天或减去N天得到一个新的日期
     *
     * @param startDate 需要增加的日期时间
     * @param addnos    添加的天数，可以是正数也可以是负数
     * @return 操作后的日期
     */
    public static Date addDay(Date startDate, int addnos) {
        if (startDate == null) {
            return null;
        }
        Calendar cc = Calendar.getInstance();
        cc.setTime(startDate);
        cc.add(Calendar.DATE, addnos);
        return cc.getTime();
    }

    /**
     * 给一个日期加上N月后或减去N月后得到的一个新日期
     *
     * @param startDate 需要增加的日期时间
     * @param addnos    添加的月数，可以是正数也可以是负数
     * @return 操作后的日期
     */
    public static Date addMonth(Date startDate, int addnos) {
        if (startDate == null) {
            return null;
        }
        Calendar cc = Calendar.getInstance();
        cc.setTime(startDate);
        cc.add(Calendar.MONTH, addnos);
        return cc.getTime();

    }

    /**
     * 给一个日期加上N年后或减去N年后得到的一个新日期
     *
     * @param startDate 需要增加的日期时间
     * @param addnos    添加的年数，可以是正数也可以是负数
     * @return 操作后的日期
     */
    public static Date addYear(Date startDate, int addnos) {
        if (startDate == null) {
            return null;
        }
        Calendar cc = Calendar.getInstance();
        cc.setTime(startDate);
        cc.add(Calendar.YEAR, addnos);
        return cc.getTime();
    }

    /**
     * 计算两个日期相差的月数
     *
     * @param st  起始日期
     * @param end 结束日期
     * @return
     */
    public static int compareMonth(Date st, Date end) {
        int y = Math.abs((getYear(end) < 0 ? 0 : getYear(end)) - (getYear(st) < 0 ? 0 : getYear(st)));
        int m = 0;
        if (y > 0) {
            y--;
            m = Math.abs(12 - getMonth(st) + getMonth(end));
        } else {
            m = Math.abs(getMonth(end) - getMonth(st));
        }
        return (y * 12) + m;
    }

    /**
     * 获取当前日期,指定格式
     * 描述:<描述函数实现的功能>.
     *
     * @return
     */
    public static Integer getNowTime() {
        long t = (System.currentTimeMillis() / 1000L);
        return Integer.parseInt(String.valueOf(t));
    }

    /**
     * 计算两个日期相差的毫秒数
     *
     * @param start 启始时间
     * @param end   结束时间
     * @return
     */
    public static long compare(Date start, Date end) {
        if (start != null && end != null) {
            return end.getTime() - start.getTime();
        }
        return 0l;
    }

    /**
     * 判断给的日期，是否是当前的前一天以及更早的日期，若是，返回true，否则返回false
     *
     * @param date
     * @return
     */
    public static boolean compareDate(Date date) {
        if (date != null) {
            return date.before(stringToDate(doFormatDate(new Date(), false)));
        }
        return false;
    }

    /**
     * 自定义格式化日期输出
     *
     * @param date
     * @param format
     * @return
     */
    public static String doFormatDate(Date date, String format) {
        if (date == null) {
            return null;
        }
        return (new SimpleDateFormat(format)).format(date);
    }

    /**
     * 对日期进行格式化，格式化后的样式：YYYY-MM-DD/YYYY-MM-DD HH:MM:SS
     *
     * @param date 要进行格式化的日期
     * @param b    为True时，返回长格式的，为Falsh时返回短格式的
     * @return
     */
    public static String doFormatDate(Date date, boolean b) {
        if (date == null) {
            return null;
        }
        if (b) {
            return (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(date);
        } else {
            return (new SimpleDateFormat("yyyy-MM-dd")).format(date);
        }
    }

    /**
     * 将字符串格式的日期转为日期类型，如果不能正确转换则返回null，<br>
     * 如果含有“:”则会按“yyyy-MM-dd HH:mm:ss”来转换，否则按“yyyy-MM-dd”转换
     *
     * @param datestr
     * @return
     */
    public static Date stringToDate(String datestr) {
        if (datestr != null && !"".equals(datestr)) {
            SimpleDateFormat sdf;
            if (datestr.indexOf(":") != -1) {
                sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
            } else {
                sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
            }
            Date date = null;
            try {
                date = sdf.parse(datestr);
            } catch (Exception e) {
            }
            return date;
        } else {
            return null;
        }
    }

    /**
     * 得到当前系统的日期或时间
     *
     * @param b 为true 时返回详细时间格式，为false时返回日期格式，不含时分秒
     * @return 当前的日期或时间
     */
    public static String getDates(boolean b) {
        return doFormatDate(new Date(), b);
    }

    /**
     * 获取当前的年,如果是-1，则表示错误
     *
     * @return
     */
    public static int getYear() {
        return getYear(new Date());
    }

    /**
     * 获取指定日期的年,如果是-1，则表示错误
     *
     * @param date
     * @return
     */
    public static int getYear(Date date) {
        if (date == null) {
            return -1;
        }
        return DateToCalendar(date).get(Calendar.YEAR);
    }

    /**
     * 获取当前月，如果返回"0"，则表示错误
     *
     * @return
     */
    public static int getMonth() {
        return getMonth(new Date());
    }

    /**
     * 获取当前月，如果返回"0"，则表示错误
     *
     * @param date
     * @return
     */
    public static int getMonth(Date date) {
        if (date == null) {
            return 0;
        }
        return DateToCalendar(date).get(Calendar.MONTH) + 1;
    }

    /**
     * 获取当天日,如果返回"0",表示该日期无效或为null
     *
     * @return
     */
    public static int getDay() {
        return getDay(new Date());
    }

    /**
     * 取一个日期的日,如果返回"0",表示该日期无效或为null
     *
     * @param da
     * @return
     */
    public static int getDay(Date da) {
        if (da == null) {
            return 0;
        }
        return DateToCalendar(da).get(Calendar.DATE);
    }

    /**
     * 将java.util.Date类型的日期格式转换成java.util.Calendar格式的日期
     *
     * @param dd
     * @return
     */
    public static Calendar DateToCalendar(Date dd) {
        Calendar cc = Calendar.getInstance();
        cc.setTime(dd);
        return cc;
    }

    /**
     * 将一个长整型数据转为日期
     *
     * @param datenum
     * @return
     */
    public static Date longToDate(long datenum) {
        Calendar cc = Calendar.getInstance();
        cc.setTimeInMillis(datenum);
        return cc.getTime();
    }

    /**
     * 将一个长整型数据转为日期格式的字符串
     *
     * @param datenum
     * @return
     */
    public static String longToDateString(long datenum) {
        return doFormatDate(longToDate(datenum), true);
    }

    /**
     * 得到给定日期的前一个周日的日期
     *
     * @param date
     * @return
     */
    public static Date getUpWeekDay(Date date) {
        if (date == null) {
            return null;
        } else {
            Calendar cc = Calendar.getInstance();
            cc.setTime(date);
            int week = cc.get(Calendar.DAY_OF_WEEK);
            return DateUtils.addDay(date, (1 - week));
        }
    }

    /**
     * 得到给定日期所在周的周一日期
     *
     * @param date
     * @return
     */
    public static Date getMonday(Date date) {
        if (date == null) {
            return null;
        } else {
            Calendar cc = Calendar.getInstance();
            cc.setTime(date);
            int week = cc.get(Calendar.DAY_OF_WEEK);
            return DateUtils.addDay(date, (2 - week));
        }
    }

    /**
     * 得到指定日期所在的周（1-7），惹指定的日期不存在，则返回“-1”
     *
     * @param date
     * @return -1 or 1-7
     */
    public static int getWeek(Date date) {
        if (date == null) {
            return -1;
        } else {
            Calendar cc = Calendar.getInstance();
            cc.setTime(date);
            int week = cc.get(Calendar.DAY_OF_WEEK);
            if (week == 1) {
                week = 7;
            } else {
                week--;
            }
            return week;
        }
    }

    /**
     * 产生随机数
     *
     * @param lo
     * @return
     */
    public static String getRandNum(int lo) {
        if (lo < 1) {
            lo = 4;
        }
        StringBuffer temp = new StringBuffer();
        Random rand = new Random();
        for (int i = 0; i < lo; i++) {
            temp.append(String.valueOf(rand.nextInt(10)));
        }
        return temp.toString();
    }

    /**
     * 产生文件函数名，以当然日期+4位随机码为主
     */
    public static String getDataName() {
        return DateUtils.doFormatDate(new Date(), "yyyyMMddHHmmss") + getRandNum(4);
    }

    /**
     * 将DATE转为数据库的Timestamp类型
     *
     * @param dt
     * @return
     */
    public static Timestamp dateToTime(Date dt) {
        if (dt == null) {
            return null;
        }
        return new Timestamp(dt.getTime());
    }

    /**
     * method 将字符串类型的日期转换为一个timestamp（时间戳记java.sql.Timestamp）
     *
     * @param dateString 需要转换为timestamp的字符串
     * @return dataTime timestamp
     */
    @SuppressWarnings("static-access")
    public static Timestamp string2Time(String dateString) throws java.text.ParseException {
        DateFormat dateFormat;
        dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss.SSS", Locale.CHINESE);// 设定格式
        dateFormat.setLenient(false);
        // 我做这块的时候下边的不对，后来我把dateFormat后边加上了.getDateInstance()就好了
        // java.util.Date timeDate = dateFormat.parse(dateString);//util类型
        Date ywybirt = dateFormat.getDateInstance().parse(dateString);// util类型
        Timestamp dateTime = new Timestamp(ywybirt.getTime());// Timestamp类型,timeDate.getTime()返回一个long型
        return dateTime;
    }

    /**
     * String(yyyy-MM-dd HH:mm:ss)转10位时间戳
     *
     * @param time
     * @return
     */
    public static Integer StringToTimestamp(String time) {

        int times = 0;
        try {
            times = (int)((Timestamp.valueOf(time).getTime()) / 1000);
        } catch (Exception e) {
        }
        if (times == 0) {
            System.out.println("String转10位时间戳失败");
        }
        return times;

    }

    /**
     * 将日期格式转为java.sql.Date
     *
     * @param de
     * @return
     */
    public static java.sql.Date dateToSqlDate(Date de) {
        return new java.sql.Date(de.getTime());
    }

    /**
     * 格式化日期字符串 yyyymmddhh24miss
     *
     * @param date
     * @return
     */
    public static String formatDS(String date) {
        if (date == null) {
            return "";
        }
        return date.replace("-", "").replace(":", "").replace(" ", "");
    }

    /**
     * 将公安部的时间格式转为日期格式
     *
     * @param datestr
     * @return
     */
    public static Date strTdate(String datestr) {
        if (datestr != null && !"".equals(datestr)) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
            Date date = null;
            try {
                date = sdf.parse(datestr);
            } catch (Exception e) {
            }
            return date;
        } else {
            return null;
        }
    }

    /**
     * 以公安部的日期格式返回当前系统时间
     *
     * @return
     */
    public static String getGabDate() {
        return doFormatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
    }

    public static String str2StrDate(String datestr) {
        Date dd = strTdate(datestr);
        if (dd != null) {
            return DateUtils.doFormatDate(dd, true);
        }
        return null;
    }

    /**
     * 获得往年同期
     *
     * @param datetime
     * @param year
     * @return
     */
    public static String DateAddYear(String datetime, int year) {
        String rtnVal = "";

        Date date = null;
        date = addYear(strTdate(datetime), year);
        rtnVal = formatDS(doFormatDate(date, true));

        return rtnVal;
    }

    public static String getChDate(Date date) {
        return doFormatDate(date, "yyyy年MM月dd日 ahh:mm:ss");
    }

    /**
     * 字符串日期加月
     *
     * @param date
     * @param month
     * @param time
     * @return
     */
    public static String DateAddMonth(String date, int month, String time) {
        String rtnVal = "";

        date = formatDS(date);

        Date datetime = null;
        String temp = formatDS(date) + time;
        datetime = addMonth(strTdate(temp), month);
        temp = doFormatDate(datetime, true);
        rtnVal = formatDS(temp);

        return rtnVal;
    }

    /**
     * 将公安部的14位的时间字符串转换成"YYYY-MM-DD hh:mm:ss"
     *
     * @param time 14位的时间字符串 如20130405210204
     * @return YYYY-MM-DD hh:mm:ss格式的时间字符串
     * @author yali
     * @created 2013-7-30 下午01:19:00
     * @lastModified
     * @history
     */
    public static String timeFormate(String time) {
        if (time == null || time.length() < 14) {
            return time;
        } else {
            StringBuilder str = new StringBuilder();
            str.append(time.substring(0, 4))
                .append("-")
                .append(time.substring(4, 6))
                .append("-")
                .append(time.substring(6, 8))
                .append(" ")
                .append(time.substring(8, 10))
                .append(":")
                .append(time.substring(10, 12))
                .append(":")
                .append(time.substring(12, 14));
            return str.toString();
        }
    }

    /**
     * 将14为字符串转换为“yyyy-mm-dd”格式的时间字符
     *
     * @param time
     * @return
     * @author yali
     * @created 2013-7-31 下午07:42:15
     * @lastModified
     * @history
     */
    public static String datetimeFormate(String time) {
        if (StringUtils.isBlank(time)) {
            return time;
        } else if (time.length() == 14 || time.length() == 8) {
            StringBuilder str = new StringBuilder();
            str.append(time.substring(0, 4))
                .append("-")
                .append(time.substring(4, 6))
                .append("-")
                .append(time.substring(6, 8));
            return str.toString();
        } else {
            return time;
        }
    }

    /**
     * 将14为字符串转换为“yyyy-mm-dd HH:MM”格式的时间字符
     *
     * @param time
     * @return
     * @author yali
     * @created 2013-7-31 下午07:44:55
     * @lastModified
     * @history
     */
    public static String dayHourtimeFormate(String time) {
        if (StringUtils.isBlank(time)) {
            return time;
        } else if (time.length() == 14 || time.length() == 8) {
            StringBuilder str = new StringBuilder();
            str.append(time.substring(0, 4))
                .append("-")
                .append(time.substring(4, 6))
                .append("-")
                .append(time.substring(6, 8))
                .append(" ")
                .append(time.substring(8, 10))
                .append(":")
                .append(time.substring(10, 12));
            return str.toString();
        } else {
            return time;
        }
    }

    public static Date stringToDate1(String datestr, String format) {
        Date date = null;
        SimpleDateFormat df = new SimpleDateFormat(format);
        try {
            date = df.parse(datestr);
        } catch (Exception e) {
        }

        return date;
    }

    public static boolean inSameDay(Date d1, Date d2) {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");

        String d1Txt = df.format(d1);
        String d2Txt = df.format(d2);
        return StringUtils.equals(d1Txt, d2Txt);
    }

    /**
     * 根据格式化字符串格式化 字符串为时间
     *
     * @param dateString 日期的字符串形式
     * @param formatType 格式化字符串类型
     * @return
     * @author
     * @created 2014-12-28 下午03:06:29
     * @lastModified
     * @history
     */
    public static Date getDateTimeByFormat(String dateString, int formatType) {
        if (StringUtils.isBlank(dateString)) {
            return null;
        }
        String formatter = formatters[formatType];
        return stringToDate1(dateString, formatter);
    }

    /**
     * 根据格式化字符串格式化 将日期转换成字符串形式
     *
     * @param date       日期
     * @param formatType 格式化字符串类型
     * @return
     * @author
     * @created 2014-12-28 下午03:08:21
     * @lastModified
     * @history
     */
    public static String doFormatDateByFormat(Date date, int formatType) {
        if (date == null) {
            return null;
        }
        String formatter = formatters[formatType];
        return (new SimpleDateFormat(formatter)).format(date);
    }

    /**
     * 根据源时间字符串和相应的格式化类型获取目标时间字符串
     *
     * @param srcString     源时间字符串
     * @param srcFormatType 源字符串格式化类型
     * @param desFormatType 目标字符串格式化类型
     * @return
     * @author f
     * @created 2014-12-28 下午03:13:54
     * @lastModified
     * @history
     */
    public static String getFormatStringByFormatString(String srcString, int srcFormatType, int desFormatType) {
        if (StringUtils.isBlank(srcString)) {
            return null;
        }
        Date d = getDateTimeByFormat(srcString, srcFormatType);
        return doFormatDateByFormat(d, desFormatType);
    }

    /**
     * 获得日期的年份第一天日期
     *
     * @param date 日期
     * @return
     * @author
     * @created 2014-12-28 下午03:13:54
     * @lastModified
     * @history
     */
    public static Date getDateStartOfYear(Date date) {
        Calendar calendar = DateToCalendar(date);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获得日期的年份最后一天日期
     *
     * @param date 日期
     * @return
     * @author
     * @created 2014-12-28 下午03:13:54
     * @lastModified
     * @history
     */
    public static Date getDateEndOfYear(Date date) {
        Calendar calendar = DateToCalendar(date);
        calendar.add(Calendar.YEAR, 1);
        Date newDate = getDateStartOfYear(calendar.getTime());
        calendar = DateToCalendar(newDate);
        calendar.add(Calendar.SECOND, -1);
        return calendar.getTime();
    }

    /**
     * 计算两个时间的月份差，如果年份差大于1年，则返回9999
     *
     * @param from 开始时间
     * @param to   结束时间
     * @return
     * @author
     * @created 2015-5-19 下午03:40:57
     * @lastModified
     * @history
     */
    public static int getMonthCount(Date from, Date to) {
        int fromYear = getYear(from);
        int toYear = getYear(to);
        int fromMonth = getMonth(from);
        int toMonth = getMonth(to);
        if (fromYear == toYear) {
            return (toMonth - fromMonth);
        } else if ((toYear - fromYear) == 1) {
            return ((12 - fromMonth) + toMonth);
        }
        return 9999;
    }

    public static Date formatString(String datestr) {
        for (int i = 0; i < formatters.length; i++) {
            try {
                String strformat = formatters[i];
                SimpleDateFormat sdf = new SimpleDateFormat(strformat);
                return sdf.parse(datestr);
            } catch (Exception e) {

            }
        }
        return null;
    }

    /**
     * 根据源时间字符串和相应的格式化类型获取目标时间字符串
     *
     * @param srcString     源时间字符串
     * @param srcFormatType 源字符串格式化类型
     * @param desFormatType 目标字符串格式化类型
     * @return
     * @author
     * @created 2014-12-28 下午03:13:54
     */
    public static String getFormatStringByFormatString(String srcString, String srcFormatType, String desFormatType) {
        if (StringUtils.isBlank(srcString)) {
            return null;
        }
        Date d = getDateTimeByFormat(srcString, srcFormatType);
        return doFormatDateByFormat(d, desFormatType);
    }

    /**
     * 根据格式化字符串格式化 字符串为时间
     *
     * @param dateString 日期的字符串形式
     * @param formatType 格式化字符串类型
     * @return
     * @author
     * @created 2014-12-28 下午03:06:29
     */
    public static Date getDateTimeByFormat(String dateString, String formatType) {
        if (StringUtils.isBlank(dateString)) {
            return null;
        }
        return stringToDate1(dateString, formatType);
    }

    /**
     * 根据格式化字符串格式化 将日期转换成字符串形式
     *
     * @param date       日期
     * @param formatType 格式化字符串类型
     * @return
     * @author
     * @created 2014-12-28 下午03:08:21
     */
    public static String doFormatDateByFormat(Date date, String formatType) {
        if (date == null) {
            return null;
        }
        return (new SimpleDateFormat(formatType)).format(date);
    }

    /**
     * 根据出生日期获取年龄信息
     *
     * @param csrq   出生日期
     * @param format 年龄精度（yyyy，年；yyyy[-]MM，月；yyyy[-]MM[-]dd，日）
     * @return
     * @author
     * @created 2016-5-25 下午04:19:00
     */
    public static int getAgeByCsrq(Date csrq, String format, Date compareDate) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        String csrqString = df.format(csrq);
        try {
            csrq = df.parse(csrqString);
        } catch (ParseException e) {

        }
        Calendar csrqCalendar = Calendar.getInstance();
        csrqCalendar.setTime(csrq);
        Calendar nowTime = Calendar.getInstance();
        nowTime.setTime(compareDate);
        int yearMinus = nowTime.get(Calendar.YEAR) - csrqCalendar.get(Calendar.YEAR);
        // 月份是否大于出生日期
        boolean monthCompareFlag = (nowTime.get(Calendar.MONTH) - csrqCalendar.get(Calendar.MONTH)) >= 0;
        if (!monthCompareFlag) {
            yearMinus -= 1;
            return yearMinus;
        }
        // 月份所在天数是否大于出生日期
        boolean dayOfMonthCompareFlag = (nowTime.get(Calendar.DAY_OF_MONTH) - csrqCalendar
            .get(Calendar.DAY_OF_MONTH)) >= 0;
        if (!dayOfMonthCompareFlag) {
            yearMinus -= 1;
            return yearMinus;
        }
        return yearMinus;
    }

    public static Date addYears(final Date date, final int amount) {
        return add(date, Calendar.YEAR, amount);
    }

    public static Date addMonths(final Date date, final int amount) {
        return add(date, Calendar.MONTH, amount);
    }

    private static Date add(final Date date, final int calendarField, final int amount) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        final Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(calendarField, amount);
        return c.getTime();
    }

    public static int diffDays(Date date1, Date date2) throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        long time1 = cal.getTimeInMillis();
        cal.setTime(date2);
        long time2 = cal.getTimeInMillis();
        float between_days = (float)(time2 - time1) * 1.0F / 86400000.0F;

        if ((between_days < 0.0F) && (between_days > -1.0F)) {
            if (isSameDay(date1, date2)) {
                return 0;
            }
            return -1;
        }
        if ((between_days > 0.0F) && (between_days < 1.0F)) {
            if (isSameDay(date1, date2)) {
                return 0;
            }
            return 1;
        }
        if (between_days == 0.0F)
            return 0;
        if (between_days < 0.0F) {
            return (int)Math.floor(between_days);
        }
        return (int)Math.ceil(between_days);
    }

    /**
     * 使用Calendar类 辅助完成天数的差值
     * 在不涉及到跨年的情况，此种方法是没问题的
     * 但是设计跨年的情况，此种方法回出问题的哦
     * <p>
     * 使用此方法注意测试临界时间点是否符合自己需求
     */
    public static BigDecimal diffDays2(Date date1, Date date2) {

        BigDecimal days = new BigDecimal(date1.getTime() - date2.getTime())
            .divide(new BigDecimal(1000 * 3600 * 24), 1, BigDecimal.ROUND_HALF_UP);
        return days;
    }

    public static BigDecimal diffMin(Date date1, Date date2) {

        BigDecimal days = new BigDecimal(date1.getTime() - date2.getTime())
            .divide(new BigDecimal(1000 * 60), 1, BigDecimal.ROUND_HALF_UP);
        return days;
    }

    public static boolean isSameDay(Calendar c1, Calendar c2) {
        return (c1.get(1) == c2.get(1)) && (c1.get(2) == c2.get(2)) && (c1.get(5) == c2.get(5));
    }

    public static boolean isSameDay(Date d1, Date d2) {
        Calendar c1 = Calendar.getInstance(TIME_ZONE);
        c1.setTime(d1);
        Calendar c2 = Calendar.getInstance(TIME_ZONE);
        c2.setTime(d2);
        return isSameDay(c1, c2);
    }

    public static int getHours(Date date) {
        if (date == null) {
            return -1;
        }
        return DateToCalendar(date).get(Calendar.HOUR_OF_DAY);
    }

    public static int getSecondOfDay(Date date) {
        if (date == null) {
            return -1;
        }
        Calendar midnight = Calendar.getInstance();
        midnight.setTime(date);
        midnight.set(Calendar.HOUR_OF_DAY, 0);
        midnight.set(Calendar.MINUTE, 0);
        midnight.set(Calendar.SECOND, 0);
        midnight.set(Calendar.MILLISECOND, 0);
        return (int)((date.getTime() - midnight.getTime().getTime()) / 1000);
    }

    public static String parseDateBySeconds(int seconds) {
        int hour = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        return String.format("%02d:%02d", hour, minutes);
    }

    public static void main(String[] args) {
        System.out.println(parseDateBySeconds(getSecondOfDay(new Date())));
    }

    /**
     * 获取精确到秒的时间戳
     *
     * @return
     */
    public static int getSecondTimestamp(Date date) {
        if (null == date) {
            return 0;
        }
        String timestamp = String.valueOf(date.getTime());
        int length = timestamp.length();
        if (length > 3) {
            return Integer.valueOf(timestamp.substring(0, length - 3));
        } else {
            return 0;
        }
    }

    public static int differentDaysByDate(Date date1, Date date2) {
        int days = (int)((date2.getTime() - date1.getTime()) / (1000 * 3600 * 24));
        return days;
    }
}
