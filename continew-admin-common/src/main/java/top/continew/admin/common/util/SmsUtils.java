/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import top.continew.starter.cache.redisson.util.RedisUtils;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

/**
 * https://platform.18sms.com/#/dashboard
 * 帐号：97ra07m3
 * 登入密码：vyehohuw52s854j
 * 接口密码：znubi63hri14xcr
 * 签名：【慧品甄选】
 */
@Component
@Lazy(false)
public class SmsUtils {

    public static String uri = "http://121.41.11.164:8099/msg/send";// 应用地址
    public static String account = "97ra07m3";// 示远账号
    public static String pswd = "znubi63hri14xcr";// 示远密码

    /**
     * 发送验证码
     *
     * @param mobile 电话号码
     * @param key    redis秘钥前缀
     */
    public static boolean sendSms(String mobile, String key) {
        if (StringUtils.isEmpty(key)) {
            key = "code:";
        }
        // 随机生成验证码
        String verifyCode = String.valueOf((int)((Math.random() * 9 + 1) * 100000));
        String verifyCodeTime = "600";

        String phone = mobile.replaceFirst("^\\+?(86)?", "");

        String msg = "您的验证码是：" + verifyCode + "，请在30分钟内完成验证，如非本人操作，请忽略本短信。";
        Map<String, String> data = new HashMap();
        data.put("user_name", account);
        data.put("password", pswd);
        data.put("send_model", "1");
        data.put("needstatus", "1");
        data.put("product_type", "1");
        data.put("phone", phone);
        data.put("sms_detail", msg);
        try {
            String returnString = HttpCommonUtil.sendPostForm(uri, data);
            System.out.println(returnString);
            // 将验证码存入redis，用于验证
            RedisUtils.set(key + phone, verifyCode, Duration.ofSeconds(Integer.parseInt(verifyCodeTime)));
            return true;
        } catch (Exception e) {
            // TODO 处理异常
            e.printStackTrace();
        }

        return false;
    }

    /**
     * 发送短信内容
     *
     * @param mobile  电话号码
     * @param content 短信内容
     */
    public static String sendMsg(String mobile, String content) {
        if (StringUtils.isEmpty(mobile) || StringUtils.isEmpty(content)) {
            return "手机号或者内容不能为空";
        }
        try {
            String phone = mobile.replaceFirst("^\\+?(86)?", "");
            Map<String, String> data = new HashMap();
            data.put("user_name", account);
            data.put("password", pswd);
            data.put("send_model", "1");
            data.put("needstatus", "1");
            data.put("product_type", "1");
            data.put("phone", phone);
            data.put("sms_detail", content);
            String returnString = HttpCommonUtil.sendPostForm(uri, data);
            return returnString;
        } catch (Exception ex) {
            // TODO 处理异常
            ex.printStackTrace();
            return ex.getMessage();
        }
    }

    public static void main(String[] args) throws Exception {
        SmsUtils.sendSms("18795679990", "");
    }
}