/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import top.continew.starter.data.mybatis.plus.base.IBaseEnum;

/**
 * 账户类型
 *
 * @author Charles7c
 * @since 2022/12/29 21:59
 */
@Getter
@RequiredArgsConstructor
public enum SystemTypeEnum implements IBaseEnum<String> {

    /**
     * 用户
     */
    USER("0", "用户"),

    /**
     * 系统
     */
    SYS("1", "系统"),;

    private final String value;
    private final String description;

}
