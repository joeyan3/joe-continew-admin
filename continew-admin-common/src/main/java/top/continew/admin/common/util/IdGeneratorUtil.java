/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.util;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class IdGeneratorUtil {
    private static final long MIN_ID = 1L;
    private static final long MAX_ID = 999999L;
    private static Set<Long> generatedIds = new HashSet<>();
    private static Random random = new Random();

    public static synchronized long generateSmallId() {
        long id;
        do {
            id = MIN_ID + (long)(random.nextDouble() * (MAX_ID - MIN_ID + 1));
        } while (generatedIds.contains(id));
        generatedIds.add(id);
        return id;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println(generateSmallId());
        }
    }

}
