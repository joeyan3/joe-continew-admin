/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.util;

import org.apache.commons.lang3.StringUtils;
import top.continew.starter.cache.redisson.util.RedisUtils;

import java.util.Date;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class SequenceUtil {
    private static long remapSequence(long seq) {
        Random random = new Random(seq);
        int nextLong = random.nextInt();
        return ((nextLong & 0xffff0000L) >> 16) | ((nextLong & 0x0000ffffL) << 16);
    }

    public static String generateOrderNo() {
        return DateUtils.format(new Date(), "yyyyMMdd") + StringUtils.leftPad(String.valueOf(remapSequence(RedisUtils
            .incr("channel:order_no"))).toString(), 6, "0");
    }

    public static String generatenonceStr() {
        return DateUtils.format(new Date(), "yyyyMMdd") + StringUtils.leftPad(String.valueOf(remapSequence(RedisUtils
            .incr("nonceStr:order_no"))), 6, "0");
    }

    public static String generateTransOrder() {
        return DateUtils.format(new Date(), "yyyyMMdd") + StringUtils.leftPad(String.valueOf(remapSequence(RedisUtils
            .incr("transOrder:order_no"))), 6, "0");
    }

    public static String generateDrawOrderNo() {
        return DateUtils.format(new Date(), "yyyyMMdd") + StringUtils.leftPad(String.valueOf(remapSequence(RedisUtils
            .incr("withdraw:order_no"))), 6, "0");
    }

    public static void main(String[] args) {
        for (long i = 0, s = 10000000; i < 20; i++) {
            long size = LongStream.range(s * (2 * i - 1), s * (2 * i + 1))
                .parallel()
                .mapToObj(SequenceUtil::remapSequence)
                .collect(Collectors.toSet())
                .size();
            if (size != s * 2) {
                System.out.println(size);
            } else {
                System.out.println("pass: " + i);
            }
            System.gc();
        }
    }
}
