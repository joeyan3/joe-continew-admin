/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import java.io.Serial;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import top.continew.starter.extension.crud.model.resp.BaseDetailResp;

/**
 * 分类详情信息
 *
 * @author joe
 * @since 2024/08/14 13:03
 */
@Data
@ExcelIgnoreUnannotated
@Schema(description = "分类详情信息")
public class CategoryDetailResp extends BaseDetailResp {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 父级ID
     */
    @Schema(description = "父级ID")
    @ExcelProperty(value = "父级ID")
    private Long pid;

    /**
     * 路径
     */
    @Schema(description = "路径")
    @ExcelProperty(value = "路径")
    private String path;

    /**
     * 分类名称
     */
    @Schema(description = "分类名称")
    @ExcelProperty(value = "分类名称")
    private String name;

    /**
     * 类型，1 产品分类，2 附件分类，3 文章分类， 4 设置分类， 5 菜单分类，6 配置分类， 7 秒杀配置
     */
    @Schema(description = "类型，1 产品分类，2 附件分类，3 文章分类， 4 设置分类， 5 菜单分类，6 配置分类， 7 秒杀配置")
    @ExcelProperty(value = "类型，1 产品分类，2 附件分类，3 文章分类， 4 设置分类， 5 菜单分类，6 配置分类， 7 秒杀配置")
    private Integer type;

    /**
     * 地址
     */
    @Schema(description = "地址")
    @ExcelProperty(value = "地址")
    private String url;

    /**
     * 扩展字段 Jsos格式
     */
    @Schema(description = "扩展字段 Jsos格式")
    @ExcelProperty(value = "扩展字段 Jsos格式")
    private String extra;

    /**
     * 状态, 1正常，0失效
     */
    @Schema(description = "状态, 1正常，0失效")
    @ExcelProperty(value = "状态, 1正常，0失效")
    private Integer status;

    /**
     * 排序
     */
    @Schema(description = "排序")
    @ExcelProperty(value = "排序")
    private Integer sort;
}