/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.vo;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

import top.continew.starter.extension.crud.model.entity.BaseDO;

/**
 * 城市树型结构
 *
 * @author joe
 * @since 2024/08/08 21:46
 */
@Data
public class SystemCityTreeVO extends BaseDO {

    @Serial
    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 城市id
     */
    private Long cityId;

    /**
     * 省市级别
     */
    private Long level;

    /**
     * 父级id
     */
    private Long parentId;

    /**
     * 区号
     */
    private String areaCode;

    /**
     * 名称
     */
    private String name;

    /**
     * 合并名称
     */
    private String mergerName;

    /**
     * 经度
     */
    private String lng;

    /**
     * 纬度
     */
    private String lat;

    /**
     * 是否展示
     */
    private Boolean isShow;

    private List<SystemCityTreeVO> child = new ArrayList<>();
}