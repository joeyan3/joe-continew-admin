/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import top.continew.admin.common.model.resp.ShippingTemplatesFreeResp;
import top.continew.starter.data.mybatis.plus.base.BaseMapper;
import top.continew.admin.common.model.entity.ShippingTemplatesFreeDO;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * 运费模板包邮 Mapper
 *
 * @author joe
 * @since 2024/08/08 10:40
 */
public interface ShippingTemplatesFreeMapper extends BaseMapper<ShippingTemplatesFreeDO> {

    /**
     * 根据tempId查找
     *
     * @param tempId Long 运费模板id
     */
    @Select("SELECT * FROM t_shipping_templates_free WHERE temp_id = #{tempId}")
    List<ShippingTemplatesFreeResp> getBytempId(@Param("tempId") Long tempId);

    /**
     * 把模板下的所有数据标记为无效
     *
     * @param tempId Long 运费模板id
     */
    @Update("UPDATE t_shipping_templates_free SET status = 0 WHERE temp_id = #{tempId}")
    void updateStatus(@Param("tempId") Long tempId);

    /**
     * 删除模板下的无效数据
     *
     * @param tempId Long 运费模板id
     */
    @Delete("DELETE FROM t_shipping_templates_free WHERE temp_id = #{tempId} AND status = 0")
    void delete(@Param("tempId") Long tempId);

    /**
     * 分组查询
     * 
     * @param tempId Long 运费模板id
     */
    @Select("SELECT group_concat(`city_id`) AS city_id, title, `number`, price, uniqid FROM t_shipping_templates_free where temp_id = #{tempId} GROUP BY `uniqid` ORDER BY id ASC")
    List<ShippingTemplatesFreeResp> getListGroup(@Param("tempId") Long tempId);

    /**
     * 根据多个id查询
     *
     * @param idList
     */
    @Select("SELECT * FROM t_shipping_templates_free WHERE temp_id IN #(idList) ORDER BY city_id ASC")
    List<ShippingTemplatesFreeResp> getListByIds(@Param("idList") Collection<? extends Serializable> idList);

    /**
     * 根据多个id查询
     *
     */
    @Select("SELECT * FROM t_shipping_templates_free WHERE temp_id = #{tempId} AND city_id = #{cityId} AND status = 1 ORDER BY id DESC LIMIT 1")
    ShippingTemplatesFreeResp getByTempIdAndCityId(@Param("tempId") Long tempId, @Param("cityId") Long cityId);

}