/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import java.io.Serial;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import top.continew.starter.extension.crud.model.resp.BaseResp;

/**
 * 商品属性详情信息
 *
 * @author joe
 * @since 2024/08/17 22:07
 */
@Data
@Schema(description = "商品属性详情信息")
public class StoreProductAttrResultResp extends BaseResp {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "商品ID")
    private Integer productId;

    @Schema(description = "商品属性参数")
    private String result;

    @Schema(description = "上次修改时间")
    private Integer changeTime;

    @Schema(description = "活动类型 0=商品，1=秒杀，2=砍价，3=拼团")
    private Integer type;
}