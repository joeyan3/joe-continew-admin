/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import top.continew.starter.extension.crud.model.entity.BaseDO;

import java.io.Serial;

/**
 * 商铺认证实体
 *
 * @author joe
 * @since 2024/07/30 15:48
 */
@Data
@TableName("t_person_shop_identification")
public class PersonShopIdentificationDO extends BaseDO {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    private Long accountId;

    /**
     * 企业名称
     */
    private String companyName;

    /**
     * 企业税号
     */
    private String companyTax;

    /**
     * 银行卡号
     */
    private String cardNo;

    /**
     * 开户行支行地址
     */
    private String bankAddress;

    /**
     * 法人姓名
     */
    private String name;

    /**
     * 身份证号码
     */
    private String personId;

    /**
     * 支付宝账号
     */
    private String alipayNo;

    /**
     * 店铺名称
     */
    private String shopName;

    /**
     * 主营类目
     */
    private String businessCategory;

    /**
     * 营业执照副本
     */
    private String businessLicense;

    /**
     * 法人身份证正面
     */
    private String passportFaceImg;

    /**
     * 法人身份证背面
     */
    private String passportSideImg;

    /**
     * 法人身份证手持
     */
    private String passportHandImg;

    /**
     * 公户银行卡正面
     */
    private String bankcardFront;

    /**
     * 公户银行卡背面
     */
    private String bankcardEnd;

    /**
     * 审核不通过次数
     */
    private Integer failNum;

    /**
     * 审核状态
     */
    private String status;

    /**
     * 错误信息
     */
    private String errMsg;
}