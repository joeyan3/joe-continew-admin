/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.query;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.math.BigDecimal;
import java.util.List;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import top.continew.starter.data.core.annotation.Query;
import top.continew.starter.data.core.enums.QueryType;

/**
 * 交易记录查询条件
 *
 * @author joe
 * @since 2024/08/05 22:48
 */
@Data
@Schema(description = "交易记录查询条件")
public class RecordQuery implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 发起者地址
     */
    @Schema(description = "发起者地址")
    @Query(type = QueryType.EQ)
    private String fromAddress;

    /**
     * 接收者地址
     */
    @Schema(description = "接收者地址")
    @Query(type = QueryType.EQ)
    private String toAddress;

    /**
     * 交易金额
     */
    @Schema(description = "交易金额")
    @Query(type = QueryType.EQ)
    private BigDecimal amount;

    /**
     * 交易状态
     */
    @Schema(description = "交易状态")
    @Query(type = QueryType.EQ)
    private String status;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    @Query(type = QueryType.BETWEEN)
    private List<LocalDateTime> createTime;
}