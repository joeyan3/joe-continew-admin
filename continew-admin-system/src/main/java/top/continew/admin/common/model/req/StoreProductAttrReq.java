/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.req;

import java.io.Serial;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import top.continew.starter.extension.crud.model.req.BaseReq;

/**
 * 创建或修改商品属性信息
 *
 * @author joe
 * @since 2024/08/17 21:58
 */
@Data
@Schema(description = "创建或修改商品属性信息")
public class StoreProductAttrReq extends BaseReq {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "attrID|新增时不填，修改时必填")
    private Integer id;

    @Schema(description = "属性名")
    @NotBlank(message = "属性名不能为空")
    private String attrName;

    @Schema(description = "属性值|逗号分隔")
    @NotBlank(message = "属性值不能为空")
    private String attrValues;
}