/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import top.continew.starter.data.mybatis.plus.base.BaseMapper;
import top.continew.admin.common.model.entity.CategoryDO;

import java.util.List;

/**
 * 分类 Mapper
 *
 * @author joe
 * @since 2024/08/14 13:03
 */
public interface CategoryMapper extends BaseMapper<CategoryDO> {
    /**
     * 通过id集合获取列表
     *
     * @param idList id列表
     */
    @Select("SELECT * FROM t_category WHERE id IN (#{idList})")
    List<CategoryDO> getByIds(@Param("idList") List<Long> idList);

    /**
     * 查询id和url是否存在
     */
    @Select("SELECT * FROM t_category WHERE id IN (#{pathIdList}) AND url = #{uri}")
    List<CategoryDO> selectByIdsAndUrl(@Param("pathIdList") List<Long> pathIdList, @Param("uri") String uri);

    /**
     * 修改分类以及子类的状态
     * 
     * @param pid Integer
     * @return bool
     */
    @Update("UPDATE t_category SET status = #{status} WHERE path LIKE CONCAT('%/', #{pid}, '/%')")
    int updateStatusByPid(@Param("pid") Long pid, @Param("status") boolean status);

    /**
     * 获取分类下子类的数量
     * 
     * @param pid Long
     * @return bool
     */
    @Select("SELECT COUNT(*) FROM t_category WHERE path LIKE CONCAT('%/', #{pid}, '/%')")
    int getChildCountByPid(@Param("pid") Long pid);

    List<CategoryDO> selectCategoryTree(@Param("type") Integer type,
                                        @Param("status") Integer status,
                                        @Param("name") String name,
                                        @Param("categoryIdList") List<Integer> categoryIdList);

    /**
     * 获取分类下子类
     *
     * @param pid
     */
    @Select("SELECT * FROM t_category WHERE status = 1 AND path LIKE CONCAT('%/', #{pid}, '/%')")
    List<CategoryDO> getChildVoListByPid(@Param("pid") Long pid);

    int checkName(@Param("name") String name, @Param("type") Integer type);

    @Select("SELECT COUNT(*) FROM t_category WHERE url = #{uri}")
    int checkUrl(@Param("uri") String uri);

    @Select("SELECT id, name FROM t_category WHERE type = #{categoryType} AND status = true ORDER BY sort DESC, id ASC")
    List<CategoryDO> findArticleCategoryList(@Param("categoryType") Integer categoryType);
}