/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.req;

import java.io.Serial;
import java.util.List;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import org.hibernate.validator.constraints.Length;
import top.continew.starter.extension.crud.model.req.BaseReq;

/**
 * 创建或修改商品信息
 *
 * @author joe
 * @since 2024/08/17 17:00
 */
@Data
@Schema(description = "创建或修改商品信息")
public class StoreProductReq extends BaseReq {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 商品id
     */
    @Schema(description = "商品id|添加时不填，修改时必填", example = "")
    private Long id;

    /**
     * 商品图片
     */
    @Schema(description = "商品图片", example = "crmebimage/public/maintain/2021/12/25/63969148b6c4447d918124fd810c1da5m2h9aiylly.jpg")
    @NotBlank(message = "商品图片不能为空")
    private String image;

    /**
     * 轮播图
     */
    @Schema(description = "轮播图", example = "[\\\"crmebimage/public/maintain/2021/12/25/63969148b6c4447d918124fd810c1da5m2h9aiylly.jpg\\\"]")
    @NotBlank(message = "轮播图不能为空")
    private String sliderImage;

    /**
     * 商品名称
     */
    @Schema(description = "商品名称", example = "LOFREE洛斐 奶茶无线蓝牙键鼠套装")
    @NotBlank(message = "商品名称不能为空")
    private String storeName;

    /**
     * 商品简介
     */
    @Schema(description = "商品简介", example = "LOFREE洛斐 奶茶无线蓝牙键鼠套装")
    @NotBlank(message = "商品简介不能为空")
    private String storeInfo;

    /**
     * 关键字
     */
    @Schema(description = "关键字", example = "无线蓝牙 键鼠 套装")
    @NotBlank(message = "关键字不能为空")
    private String keyword;

    /**
     * 分类id
     */
    @Schema(description = "分类id|逗号分隔", example = "614799235560243211")
    @NotBlank(message = "商品分类不能为空")
    private String cateId;

    /**
     * 单位名
     */
    @Schema(description = "单位名", example = "个")
    @NotBlank(message = "单位名不能为空")
    private String unitName;

    /**
     * 排序
     */
    @Schema(description = "排序")
    private Integer sort;

    /**
     * 是否热卖
     */
    @Schema(description = "是否热卖")
    private Boolean isHot;

    /**
     * 是否优惠
     */
    @Schema(description = "是否优惠")
    private Boolean isBenefit;

    /**
     * 是否精品
     */
    @Schema(description = "是否精品")
    private Boolean isBest;

    /**
     * 是否新品
     */
    @Schema(description = "是否新品")
    private Boolean isNew;

    /**
     * 获得积分
     */
    @Schema(description = "获得积分")
    private Integer giveIntegral;

    /**
     * 是否优品推荐
     */
    @Schema(description = "是否优品推荐")
    private Boolean isGood;

    /**
     * 是否单独分佣
     */
    @Schema(description = "是否单独分佣", example = "true")
    @NotNull(message = "是否单独分佣不能为空")
    private Boolean isSub;

    /**
     * 虚拟销量
     */
    @Schema(description = "虚拟销量")
    private Integer ficti;

    /**
     * 运费模板ID
     */
    @Schema(description = "运费模板ID", example = "614028701595599130")
    @NotNull(message = "运费模板ID不能为空")
    private Long tempId;

    /**
     * 规格 0单 1多
     */
    @Schema(description = "规格 0单 1多", example = "0")
    @NotNull(message = "规格不能为空")
    private Boolean specType;

    /**
     * 活动显示排序0=默认, 1=秒杀，2=砍价，3=拼团
     */
    @Schema(description = "活动显示排序0=默认, 1=秒杀，2=砍价，3=拼团")
    private List<String> activity;

    /**
     * 展示图
     */
    @Schema(description = "展示图")
    private String flatPattern;

    @Schema(description = "商品属性")
    //    @NotBlank(message = "商品属性不能为空")
    private List<StoreProductAttrReq> attr;

    @Schema(description = "商品属性详情")
    //    @NotBlank(message = "商品属性详情不能为空")
    private List<StoreProductAttrValueReq> attrValue;

    @Schema(description = "商品描述")
    private String content;

    @Schema(description = "优惠券id集合")
    private List<Long> couponIds;
}