/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.req;

import java.io.Serial;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import top.continew.starter.extension.crud.model.req.BaseReq;

/**
 * 商品库存
 *
 * @author joe
 * @since 2024/08/17 17:00
 */
@Data
@Schema(description = "库存修改")
public class StoreProductStockReq extends BaseReq {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "商品ID")
    @NotBlank(message = "请选择商品")
    private Long productId;

    private Long seckillId;

    private Long bargainId;

    private Long combinationId;

    @Schema(description = "商品属性ID集合")
    @NotEmpty(message = "请选择商品属性id集合")
    private Long attrId;

    @Schema(description = "类型， 增加 add | 减少 diff")
    @NotBlank(message = "请选择类型")
    private String operationType;

    @Schema(description = "数量")
    @Min(value = 0, message = "请填写数量")
    private Integer num;

    @Schema(description = "商品类型 0=普通 1=秒杀")
    private Integer type;

    @Schema(description = "商品SKU信息")
    private String suk;
}