/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.entity;

import java.io.Serial;
import java.math.BigDecimal;

import lombok.Data;

import com.baomidou.mybatisplus.annotation.TableName;

import top.continew.starter.extension.crud.model.entity.BaseDO;

/**
 * 运费模板包邮实体
 *
 * @author joe
 * @since 2024/08/08 10:40
 */
@Data
@TableName("t_shipping_templates_free")
public class ShippingTemplatesFreeDO extends BaseDO {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 模板ID
     */
    private Long tempId;

    /**
     * 城市ID
     */
    private Long cityId;

    /**
     * 描述
     */
    private String title;

    /**
     * 包邮件数
     */
    private BigDecimal number;

    /**
     * 包邮金额
     */
    private BigDecimal price;

    /**
     * 计费方式
     */
    private Integer type;

    /**
     * 分组唯一值
     */
    private String uniqid;

    /**
     * 是否无效
     */
    private Boolean status;
}