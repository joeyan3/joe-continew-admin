/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import top.continew.starter.extension.crud.model.resp.BaseDetailResp;

import java.io.Serial;
import java.math.BigDecimal;

/**
 * 余额详情信息
 *
 * @author joe
 * @since 2024/07/21 22:04
 */
@Data
@ExcelIgnoreUnannotated
@Schema(description = "余额详情信息")
public class BalanceDetailResp extends BaseDetailResp {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 账户id
     */
    @Schema(description = "账户id")
    @ExcelProperty(value = "账户id")
    private Long accountId;

    /**
     * 币种
     */
    @Schema(description = "币种")
    @ExcelProperty(value = "币种")
    private String currency;

    /**
     * 金额
     */
    @Schema(description = "金额")
    @ExcelProperty(value = "金额")
    private BigDecimal amount;
}