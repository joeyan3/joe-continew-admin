/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.entity;

import java.io.Serial;

import lombok.Data;

import com.baomidou.mybatisplus.annotation.TableName;

import top.continew.starter.extension.crud.model.entity.BaseDO;

/**
 * 分类实体
 *
 * @author joe
 * @since 2024/08/14 13:03
 */
@Data
@TableName("t_category")
public class CategoryDO extends BaseDO {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 父级ID
     */
    private Long pid;

    /**
     * 路径
     */
    private String path;

    /**
     * 分类名称
     */
    private String name;

    /**
     * 类型，1 产品分类，2 附件分类，3 文章分类， 4 设置分类， 5 菜单分类，6 配置分类， 7 秒杀配置
     */
    private Integer type;

    /**
     * 地址
     */
    private String url;

    /**
     * 扩展字段 Jsos格式
     */
    private String extra;

    /**
     * 状态, 1正常，0失效
     */
    private Boolean status;

    /**
     * 排序
     */
    private Integer sort;
}