/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import top.continew.admin.common.model.resp.StoreProductCouponResp;
import top.continew.starter.data.mybatis.plus.base.BaseMapper;
import top.continew.admin.common.model.entity.StoreProductCouponDO;

import java.util.List;

/**
 * 商品优惠券 Mapper
 *
 * @author joe
 * @since 2024/08/20 14:33
 */
public interface StoreProductCouponMapper extends BaseMapper<StoreProductCouponDO> {
    @Delete("DELETE FROM t_store_product_coupon WHERE product_id = #{productId}")
    void deleteByProductId(@Param("productId") Long productId);

    @Select("SELECT * FROM t_store_product_coupon WHERE product_id = #{productId}")
    List<StoreProductCouponResp> getListByProductId(@Param("productId") Long productId);

}
