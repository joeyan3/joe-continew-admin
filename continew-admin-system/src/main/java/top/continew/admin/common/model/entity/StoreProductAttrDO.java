/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.entity;

import java.io.Serial;

import lombok.Data;

import com.baomidou.mybatisplus.annotation.TableName;

import top.continew.starter.extension.crud.model.entity.BaseDO;

/**
 * 商品属性实体
 *
 * @author joe
 * @since 2024/08/17 21:58
 */
@Data
@TableName("t_store_product_attr")
public class StoreProductAttrDO extends BaseDO {

    @Serial
    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 商品ID
     */
    private Long productId;

    /**
     * 属性名
     */
    private String attrName;

    /**
     * 属性值
     */
    private String attrValues;

    /**
     * 活动类型 0=商品，1=秒杀，2=砍价，3=拼团
     */
    private Integer type;

    /**
     * 是否删除,0-否，1-是
     */
    private Boolean isDel;
}