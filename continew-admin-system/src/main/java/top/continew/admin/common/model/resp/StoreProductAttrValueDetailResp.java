/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import java.io.Serial;
import java.math.BigDecimal;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import top.continew.starter.extension.crud.model.resp.BaseDetailResp;

/**
 * 商品属性值详情信息
 *
 * @author joe
 * @since 2024/08/17 22:19
 */
@Data
@ExcelIgnoreUnannotated
@Schema(description = "商品属性值详情信息")
public class StoreProductAttrValueDetailResp extends BaseDetailResp {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 商品ID
     */
    @Schema(description = "商品ID")
    @ExcelProperty(value = "商品ID")
    private Long productId;

    /**
     * 商品属性索引值 (attr_value|attr_value[|....])
     */
    @Schema(description = "商品属性索引值 (attr_value|attr_value[|....])")
    @ExcelProperty(value = "商品属性索引值 (attr_value|attr_value[|....])")
    private String suk;

    /**
     * 属性对应的库存
     */
    @Schema(description = "属性对应的库存")
    @ExcelProperty(value = "属性对应的库存")
    private Integer stock;

    /**
     * 销量
     */
    @Schema(description = "销量")
    @ExcelProperty(value = "销量")
    private Integer sales;

    /**
     * 属性金额
     */
    @Schema(description = "属性金额")
    @ExcelProperty(value = "属性金额")
    private BigDecimal price;

    /**
     * 图片
     */
    @Schema(description = "图片")
    @ExcelProperty(value = "图片")
    private String image;

    /**
     * 唯一值
     */
    @Schema(description = "唯一值")
    @ExcelProperty(value = "唯一值")
    private String uniqueValue;

    /**
     * 成本价
     */
    @Schema(description = "成本价")
    @ExcelProperty(value = "成本价")
    private BigDecimal cost;

    /**
     * 商品条码
     */
    @Schema(description = "商品条码")
    @ExcelProperty(value = "商品条码")
    private String barCode;

    /**
     * 原价
     */
    @Schema(description = "原价")
    @ExcelProperty(value = "原价")
    private BigDecimal otPrice;

    /**
     * 重量
     */
    @Schema(description = "重量")
    @ExcelProperty(value = "重量")
    private BigDecimal weight;

    /**
     * 体积
     */
    @Schema(description = "体积")
    @ExcelProperty(value = "体积")
    private BigDecimal volume;

    /**
     * 一级返佣
     */
    @Schema(description = "一级返佣")
    @ExcelProperty(value = "一级返佣")
    private BigDecimal brokerage;

    /**
     * 二级返佣
     */
    @Schema(description = "二级返佣")
    @ExcelProperty(value = "二级返佣")
    private BigDecimal brokerageTwo;

    /**
     * 活动类型 0=商品，1=秒杀，2=砍价，3=拼团
     */
    @Schema(description = "活动类型 0=商品，1=秒杀，2=砍价，3=拼团")
    @ExcelProperty(value = "活动类型 0=商品，1=秒杀，2=砍价，3=拼团")
    private Integer type;

    /**
     * 活动限购数量
     */
    @Schema(description = "活动限购数量")
    @ExcelProperty(value = "活动限购数量")
    private Integer quota;

    /**
     * 活动限购数量显示
     */
    @Schema(description = "活动限购数量显示")
    @ExcelProperty(value = "活动限购数量显示")
    private Integer quotaShow;

    /**
     * attr_values 创建更新时的属性对应
     */
    @Schema(description = "attr_values 创建更新时的属性对应")
    @ExcelProperty(value = "attr_values 创建更新时的属性对应")
    private String attrValue;

    /**
     * 是否删除,0-否，1-是
     */
    @Schema(description = "是否删除,0-否，1-是")
    @ExcelProperty(value = "是否删除,0-否，1-是")
    private Boolean isDel;
}