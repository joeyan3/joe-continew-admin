/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.req;

import java.io.Serial;

import jakarta.validation.constraints.*;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import org.hibernate.validator.constraints.Length;

import top.continew.starter.extension.crud.model.req.BaseReq;

/**
 * 创建或修改商品描述信息
 *
 * @author joe
 * @since 2024/08/18 14:17
 */
@Data
@Schema(description = "创建或修改商品描述信息")
public class StoreProductDescriptionReq extends BaseReq {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 商品ID
     */
    @Schema(description = "商品ID")
    @NotNull(message = "商品ID不能为空")
    private Long productId;

    /**
     * 商品详情
     */
    @Schema(description = "商品详情")
    @NotBlank(message = "商品详情不能为空")
    @Length(max = 65535, message = "商品详情长度不能超过 {max} 个字符")
    private String description;

    /**
     * 商品类型
     */
    @Schema(description = "商品类型")
    @NotNull(message = "商品类型不能为空")
    private Integer type;
}