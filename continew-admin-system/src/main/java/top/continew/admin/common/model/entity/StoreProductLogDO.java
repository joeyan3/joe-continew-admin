/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.entity;

import java.io.Serial;
import java.math.BigDecimal;

import lombok.Data;

import com.baomidou.mybatisplus.annotation.TableName;

import top.continew.starter.extension.crud.model.entity.BaseDO;

/**
 * 商品日志实体
 *
 * @author joe
 * @since 2024/08/18 15:01
 */
@Data
@TableName("t_store_product_log")
public class StoreProductLogDO extends BaseDO {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 类型visit,cart,order,pay,collect,refund
     */
    private String type;

    /**
     * 商品ID
     */
    private Long productId;

    /**
     * 用户ID
     */
    private Long uid;

    /**
     * 是否浏览
     */
    private Boolean visitNum;

    /**
     * 加入购物车数量
     */
    private Integer cartNum;

    /**
     * 下单数量
     */
    private Integer orderNum;

    /**
     * 支付数量
     */
    private Integer payNum;

    /**
     * 支付金额
     */
    private BigDecimal payPrice;

    /**
     * 商品成本价
     */
    private BigDecimal costPrice;

    /**
     * 支付用户ID
     */
    private Integer payUid;

    /**
     * 退款数量
     */
    private Integer refundNum;

    /**
     * 退款金额
     */
    private BigDecimal refundPrice;

    /**
     * 收藏
     */
    private Boolean collectNum;

    /**
     * 添加时间
     */
    private Long addTime;
}