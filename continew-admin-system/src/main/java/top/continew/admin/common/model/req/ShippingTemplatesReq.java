/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.req;

import java.io.Serial;
import java.util.List;

import jakarta.validation.constraints.*;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import org.hibernate.validator.constraints.Length;

import top.continew.starter.extension.crud.model.req.BaseReq;

/**
 * 创建或修改运费模板信息
 *
 * @author joe
 * @since 2024/08/08 10:04
 */
@Data
@Schema(description = "创建或修改运费模板信息")
public class ShippingTemplatesReq extends BaseReq {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 模板名称
     */
    @Schema(description = "模板名称")
    @NotBlank(message = "模板名称不能为空")
    @Length(max = 255, message = "模板名称长度不能超过 {max} 个字符")
    private String name;

    /**
     * 计费方式
     */
    @Schema(description = "计费方式")
    @NotNull(message = "计费方式不能为空")
    private Integer type;

    /**
     * 指定包邮
     */
    @Schema(description = "指定包邮")
    @NotNull(message = "指定包邮不能为空")
    private Boolean appoint;

    /**
     * 排序
     */
    @Schema(description = "排序")
    private Integer sort;

    @Schema(description = "配送区域及运费")
    @NotNull(message = "配送区域及运费不能为空")
    private List<ShippingTemplatesRegionReq> shippingTemplatesRegionRequestList;

    @Schema(description = "指定包邮设置")
    //    @NotNull(message = "指定包邮设置不能为空")
    private List<ShippingTemplatesFreeReq> shippingTemplatesFreeRequestList;
}