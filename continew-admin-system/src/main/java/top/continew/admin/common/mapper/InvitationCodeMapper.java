/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import top.continew.starter.data.mybatis.plus.base.BaseMapper;
import top.continew.admin.common.model.entity.InvitationCodeDO;

import java.util.List;

/**
 * 邀请码 Mapper
 *
 * @author joe
 * @since 2024/07/21 13:24
 */
public interface InvitationCodeMapper extends BaseMapper<InvitationCodeDO> {

    @Select("SELECT * FROM t_invitation_code WHERE invitation_code = #{invitationCode}")
    InvitationCodeDO getByInvitationCode(@Param("invitationCode") String invitationCode);

    @Select("SELECT * FROM t_invitation_code WHERE account_name = #{accountName}")
    InvitationCodeDO getByAccountName(@Param("accountName") String accountName);

    @Select("SELECT * FROM t_invitation_code WHERE account_id = #{accountId}")
    InvitationCodeDO getByAccountId(@Param("accountId") Long accountId);

    void updateGroupCountByPath(List<String> invitationCodeList, Integer addCount);

    void updateSingleChildCount(@Param("parentInvitationCode") String parentInvitationCode,
                                @Param("addCount") Integer addCount);

}