/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.req;

import java.io.Serial;
import java.math.BigDecimal;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import top.continew.starter.extension.crud.model.req.BaseReq;

/**
 * 创建或修改交易记录信息
 *
 * @author joe
 * @since 2024/08/05 22:48
 */
@Data
@Schema(description = "创建或修改交易记录信息")
public class RecordReq extends BaseReq {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 发起者地址
     */
    @Schema(description = "发起者地址")
    private String fromAddress;

    /**
     * 接收者地址
     */
    @Schema(description = "接收者地址")
    private String toAddress;

    /**
     * 交易金额
     */
    @Schema(description = "交易金额")
    private BigDecimal amount;

    /**
     * 交易类型
     */
    @Schema(description = "交易类型")
    private String payType;
}