/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.req;

import java.io.Serial;

import jakarta.validation.constraints.*;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import top.continew.starter.extension.crud.model.req.BaseReq;

/**
 * 创建或修改商品优惠券信息
 *
 * @author joe
 * @since 2024/08/20 14:33
 */
@Data
@Schema(description = "创建或修改商品优惠券信息")
public class StoreProductCouponReq extends BaseReq {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 商品id
     */
    @Schema(description = "商品id")
    @NotNull(message = "商品id不能为空")
    private Long productId;

    /**
     * 优惠劵id
     */
    @Schema(description = "优惠劵id")
    @NotNull(message = "优惠劵id不能为空")
    private Long issueCouponId;

    /**
     * 添加时间
     */
    @Schema(description = "添加时间")
    @NotNull(message = "添加时间不能为空")
    private Integer addTime;
}