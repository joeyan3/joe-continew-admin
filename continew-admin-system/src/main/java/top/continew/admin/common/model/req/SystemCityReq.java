/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.req;

import java.io.Serial;

import jakarta.validation.constraints.*;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import org.hibernate.validator.constraints.Length;

import top.continew.starter.extension.crud.model.req.BaseReq;

/**
 * 创建或修改城市信息
 *
 * @author joe
 * @since 2024/08/08 21:46
 */
@Data
@Schema(description = "创建或修改城市信息")
public class SystemCityReq extends BaseReq {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 城市id
     */
    @Schema(description = "城市id")
    @NotNull(message = "城市id不能为空")
    private Long cityId;

    /**
     * 省市级别
     */
    @Schema(description = "省市级别")
    @NotNull(message = "省市级别不能为空")
    private Long level;

    /**
     * 父级id
     */
    @Schema(description = "父级id")
    @NotNull(message = "父级id不能为空")
    private Long parentId;

    /**
     * 区号
     */
    @Schema(description = "区号")
    @NotBlank(message = "区号不能为空")
    @Length(max = 30, message = "区号长度不能超过 {max} 个字符")
    private String areaCode;

    /**
     * 名称
     */
    @Schema(description = "名称")
    @NotBlank(message = "名称不能为空")
    @Length(max = 100, message = "名称长度不能超过 {max} 个字符")
    private String name;

    /**
     * 经度
     */
    @Schema(description = "经度")
    @NotBlank(message = "经度不能为空")
    @Length(max = 50, message = "经度长度不能超过 {max} 个字符")
    private String lng;

    /**
     * 纬度
     */
    @Schema(description = "纬度")
    @NotBlank(message = "纬度不能为空")
    @Length(max = 50, message = "纬度长度不能超过 {max} 个字符")
    private String lat;

    /**
     * 是否展示
     */
    @Schema(description = "是否展示")
    @NotNull(message = "是否展示不能为空")
    private Boolean isShow;

}