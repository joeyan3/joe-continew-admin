/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.query;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.math.BigDecimal;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import top.continew.starter.data.core.annotation.Query;
import top.continew.starter.data.core.enums.QueryType;

/**
 * 优惠券查询条件
 *
 * @author joe
 * @since 2024/08/19 16:14
 */
@Data
@Schema(description = "优惠券查询条件")
public class StoreCouponQuery implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 优惠券名称
     */
    @Schema(description = "优惠券名称")
    @Query(type = QueryType.EQ)
    private String name;

    /**
     * 兑换的优惠券面值
     */
    @Schema(description = "兑换的优惠券面值")
    @Query(type = QueryType.EQ)
    private BigDecimal money;

    /**
     * 是否限量, 默认0 不限量， 1限量
     */
    @Schema(description = "是否限量, 默认0 不限量， 1限量")
    @Query(type = QueryType.EQ)
    private Boolean isLimited;

    /**
     * 使用类型 1 全场通用, 2 商品券, 3 品类券
     */
    @Schema(description = "使用类型 1 全场通用, 2 商品券, 3 品类券")
    @Query(type = QueryType.EQ)
    private Integer useType;

    /**
     * 所属商品id / 分类id
     */
    @Schema(description = "所属商品id / 分类id")
    @Query(type = QueryType.EQ)
    private String primaryKey;

    /**
     * 可领取开始时间
     */
    @Schema(description = "可领取开始时间")
    @Query(type = QueryType.EQ)
    private LocalDateTime receiveStartTime;

    /**
     * 可领取结束时间
     */
    @Schema(description = "可领取结束时间")
    @Query(type = QueryType.EQ)
    private LocalDateTime receiveEndTime;

    /**
     * 是否固定使用时间, 默认0 否， 1是
     */
    @Schema(description = "是否固定使用时间, 默认0 否， 1是")
    @Query(type = QueryType.EQ)
    private Boolean isFixedTime;

    /**
     * 可使用时间范围 开始时间
     */
    @Schema(description = "可使用时间范围 开始时间")
    @Query(type = QueryType.EQ)
    private LocalDateTime useStartTime;

    /**
     * 可使用时间范围 结束时间
     */
    @Schema(description = "可使用时间范围 结束时间")
    @Query(type = QueryType.EQ)
    private LocalDateTime useEndTime;

    /**
     * 天数
     */
    @Schema(description = "天数")
    @Query(type = QueryType.EQ)
    private Integer day;

    /**
     * 优惠券类型 1 手动领取, 2 新人券, 3 赠送券
     */
    @Schema(description = "优惠券类型 1 手动领取, 2 新人券, 3 赠送券")
    @Query(type = QueryType.EQ)
    private Integer type;

    /**
     * 排序
     */
    @Schema(description = "排序")
    @Query(type = QueryType.EQ)
    private Integer sort;

    /**
     * 状态（0：关闭，1：开启）
     */
    @Schema(description = "状态（0：关闭，1：开启）")
    @Query(type = QueryType.EQ)
    private Boolean status;

    /**
     * 是否删除 状态（0：否，1：是）
     */
    @Schema(description = "是否删除 状态（0：否，1：是）")
    @Query(type = QueryType.EQ)
    private Boolean isDel;
}