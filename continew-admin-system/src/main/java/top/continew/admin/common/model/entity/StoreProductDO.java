/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.entity;

import java.io.Serial;
import java.math.BigDecimal;

import lombok.Data;

import com.baomidou.mybatisplus.annotation.TableName;

import top.continew.starter.extension.crud.model.entity.BaseDO;

/**
 * 商品实体
 *
 * @author joe
 * @since 2024/08/17 17:00
 */
@Data
@TableName("t_store_product")
public class StoreProductDO extends BaseDO {

    @Serial
    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 商户Id(0为总后台管理员创建,不为0的时候是商户后台创建)
     */
    private Integer merId;

    /**
     * 商品图片
     */
    private String image;

    /**
     * 轮播图
     */
    private String sliderImage;

    /**
     * 商品名称
     */
    private String storeName;

    /**
     * 商品简介
     */
    private String storeInfo;

    /**
     * 关键字
     */
    private String keyword;

    /**
     * 商品条码（一维码）
     */
    private String barCode;

    /**
     * 分类id
     */
    private String cateId;

    /**
     * 商品价格
     */
    private BigDecimal price;

    /**
     * 会员价格
     */
    private BigDecimal vipPrice;

    /**
     * 市场价
     */
    private BigDecimal otPrice;

    /**
     * 邮费
     */
    private BigDecimal postage;

    /**
     * 单位名
     */
    private String unitName;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 销量
     */
    private Integer sales;

    /**
     * 库存
     */
    private Integer stock;

    /**
     * 状态（0：未上架，1：上架）
     */
    private Boolean isShow;

    /**
     * 是否热卖
     */
    private Boolean isHot;

    /**
     * 是否优惠
     */
    private Boolean isBenefit;

    /**
     * 是否精品
     */
    private Boolean isBest;

    /**
     * 是否新品
     */
    private Boolean isNew;

    /**
     * 添加时间
     */
    private Integer addTime;

    /**
     * 是否包邮
     */
    private Boolean isPostage;

    /**
     * 是否删除
     */
    private Boolean isDel;

    /**
     * 商户是否代理 0不可代理1可代理
     */
    private Boolean merUse;

    /**
     * 获得积分
     */
    private Integer giveIntegral;

    /**
     * 成本价
     */
    private BigDecimal cost;

    /**
     * 秒杀状态 0 未开启 1已开启
     */
    private Boolean isSeckill;

    /**
     * 砍价状态 0未开启 1开启
     */
    private Boolean isBargain;

    /**
     * 是否优品推荐
     */
    private Boolean isGood;

    /**
     * 是否单独分佣
     */
    private Boolean isSub;

    /**
     * 虚拟销量
     */
    private Integer ficti;

    /**
     * 浏览量
     */
    private Integer browse;

    /**
     * 商品二维码地址(用户小程序海报)
     */
    private String codePath;

    /**
     * 淘宝京东1688类型
     */
    private String soureLink;

    /**
     * 主图视频链接
     */
    private String videoLink;

    /**
     * 运费模板ID
     */
    private Long tempId;

    /**
     * 规格 0单 1多
     */
    private Boolean specType;

    /**
     * 活动显示排序 0=默认, 1=秒杀，2=砍价，3=拼团
     */
    private String activity;

    /**
     * 展示图
     */
    private String flatPattern;

    /**
     * 是否回收站
     */
    private Boolean isRecycle;
}