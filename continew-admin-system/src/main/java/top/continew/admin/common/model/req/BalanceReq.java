/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.req;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import top.continew.starter.extension.crud.model.req.BaseReq;

import java.io.Serial;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 创建或修改余额信息
 *
 * @author joe
 * @since 2024/07/21 22:04
 */
@Data
@Schema(description = "创建或修改余额信息")
public class BalanceReq extends BaseReq {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 金额
     */
    @Schema(description = "金额")
    @NotNull(message = "金额不能为空")
    private BigDecimal amount;

    /**
     * 更新时间
     */
    @Schema(description = "更新时间")
    @NotNull(message = "更新时间不能为空")
    private LocalDateTime updateTime;
}