/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.entity;

import java.io.Serial;
import java.math.BigDecimal;

import lombok.Data;

import com.baomidou.mybatisplus.annotation.TableName;

import top.continew.starter.extension.crud.model.entity.BaseDO;

/**
 * 交易记录实体
 *
 * @author joe
 * @since 2024/08/05 22:48
 */
@Data
@TableName("t_record")
public class RecordDO extends BaseDO {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 发起者地址
     */
    private String fromAddress;

    /**
     * 接收者地址
     */
    private String toAddress;

    /**
     * 交易金额
     */
    private BigDecimal amount;

    /**
     * 交易手续费
     */
    private BigDecimal tradeFee;

    /**
     * 交易状态
     */
    private String status;

    /**
     * 前置数据
     */
    private String inputData;

    /**
     * 汇兑时转入币种
     */
    private String toCurrency;

    /**
     * 币种
     */
    private String currency;

    /**
     * 随机数
     */
    private String nonceStr;

    /**
     * 是否系统数据
     */
    private String isSystem;

    /**
     * 是否回调
     */
    private String isCallback;

    /**
     * 是否删除
     */
    private String isDelete;

    /**
     * 交易类型
     */
    private String payType;

    /**
     * 本系统流水号
     */
    private String orderNo;

    /**
     * 备注
     */
    private String remark;

    /**
     * 汇兑金额
     */
    private BigDecimal toAmount;
}