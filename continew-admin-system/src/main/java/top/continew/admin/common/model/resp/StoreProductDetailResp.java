/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import java.io.Serial;
import java.math.BigDecimal;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import top.continew.starter.extension.crud.model.resp.BaseDetailResp;

/**
 * 商品详情信息
 *
 * @author joe
 * @since 2024/08/17 17:00
 */
@Data
@ExcelIgnoreUnannotated
@Schema(description = "商品详情信息")
public class StoreProductDetailResp extends BaseDetailResp {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 商户Id(0为总后台管理员创建,不为0的时候是商户后台创建)
     */
    @Schema(description = "商户Id(0为总后台管理员创建,不为0的时候是商户后台创建)")
    @ExcelProperty(value = "商户Id(0为总后台管理员创建,不为0的时候是商户后台创建)")
    private Integer merId;

    /**
     * 商品图片
     */
    @Schema(description = "商品图片")
    @ExcelProperty(value = "商品图片")
    private String image;

    /**
     * 轮播图
     */
    @Schema(description = "轮播图")
    @ExcelProperty(value = "轮播图")
    private String sliderImage;

    /**
     * 商品名称
     */
    @Schema(description = "商品名称")
    @ExcelProperty(value = "商品名称")
    private String storeName;

    /**
     * 商品简介
     */
    @Schema(description = "商品简介")
    @ExcelProperty(value = "商品简介")
    private String storeInfo;

    /**
     * 关键字
     */
    @Schema(description = "关键字")
    @ExcelProperty(value = "关键字")
    private String keyword;

    /**
     * 商品条码（一维码）
     */
    @Schema(description = "商品条码（一维码）")
    @ExcelProperty(value = "商品条码（一维码）")
    private String barCode;

    /**
     * 分类id
     */
    @Schema(description = "分类id")
    @ExcelProperty(value = "分类id")
    private String cateId;

    /**
     * 商品价格
     */
    @Schema(description = "商品价格")
    @ExcelProperty(value = "商品价格")
    private BigDecimal price;

    /**
     * 会员价格
     */
    @Schema(description = "会员价格")
    @ExcelProperty(value = "会员价格")
    private BigDecimal vipPrice;

    /**
     * 市场价
     */
    @Schema(description = "市场价")
    @ExcelProperty(value = "市场价")
    private BigDecimal otPrice;

    /**
     * 邮费
     */
    @Schema(description = "邮费")
    @ExcelProperty(value = "邮费")
    private BigDecimal postage;

    /**
     * 单位名
     */
    @Schema(description = "单位名")
    @ExcelProperty(value = "单位名")
    private String unitName;

    /**
     * 排序
     */
    @Schema(description = "排序")
    @ExcelProperty(value = "排序")
    private Integer sort;

    /**
     * 销量
     */
    @Schema(description = "销量")
    @ExcelProperty(value = "销量")
    private Integer sales;

    /**
     * 库存
     */
    @Schema(description = "库存")
    @ExcelProperty(value = "库存")
    private Integer stock;

    /**
     * 状态（0：未上架，1：上架）
     */
    @Schema(description = "状态（0：未上架，1：上架）")
    @ExcelProperty(value = "状态（0：未上架，1：上架）")
    private Boolean isShow;

    /**
     * 是否热卖
     */
    @Schema(description = "是否热卖")
    @ExcelProperty(value = "是否热卖")
    private Boolean isHot;

    /**
     * 是否优惠
     */
    @Schema(description = "是否优惠")
    @ExcelProperty(value = "是否优惠")
    private Boolean isBenefit;

    /**
     * 是否精品
     */
    @Schema(description = "是否精品")
    @ExcelProperty(value = "是否精品")
    private Boolean isBest;

    /**
     * 是否新品
     */
    @Schema(description = "是否新品")
    @ExcelProperty(value = "是否新品")
    private Boolean isNew;

    /**
     * 添加时间
     */
    @Schema(description = "添加时间")
    @ExcelProperty(value = "添加时间")
    private Integer addTime;

    /**
     * 是否包邮
     */
    @Schema(description = "是否包邮")
    @ExcelProperty(value = "是否包邮")
    private Integer isPostage;

    /**
     * 是否删除
     */
    @Schema(description = "是否删除")
    @ExcelProperty(value = "是否删除")
    private Integer isDel;

    /**
     * 商户是否代理 0不可代理1可代理
     */
    @Schema(description = "商户是否代理 0不可代理1可代理")
    @ExcelProperty(value = "商户是否代理 0不可代理1可代理")
    private Integer merUse;

    /**
     * 获得积分
     */
    @Schema(description = "获得积分")
    @ExcelProperty(value = "获得积分")
    private Integer giveIntegral;

    /**
     * 成本价
     */
    @Schema(description = "成本价")
    @ExcelProperty(value = "成本价")
    private BigDecimal cost;

    /**
     * 秒杀状态 0 未开启 1已开启
     */
    @Schema(description = "秒杀状态 0 未开启 1已开启")
    @ExcelProperty(value = "秒杀状态 0 未开启 1已开启")
    private Integer isSeckill;

    /**
     * 砍价状态 0未开启 1开启
     */
    @Schema(description = "砍价状态 0未开启 1开启")
    @ExcelProperty(value = "砍价状态 0未开启 1开启")
    private Integer isBargain;

    /**
     * 是否优品推荐
     */
    @Schema(description = "是否优品推荐")
    @ExcelProperty(value = "是否优品推荐")
    private Boolean isGood;

    /**
     * 是否单独分佣
     */
    @Schema(description = "是否单独分佣")
    @ExcelProperty(value = "是否单独分佣")
    private Boolean isSub;

    /**
     * 虚拟销量
     */
    @Schema(description = "虚拟销量")
    @ExcelProperty(value = "虚拟销量")
    private Integer ficti;

    /**
     * 浏览量
     */
    @Schema(description = "浏览量")
    @ExcelProperty(value = "浏览量")
    private Integer browse;

    /**
     * 商品二维码地址(用户小程序海报)
     */
    @Schema(description = "商品二维码地址(用户小程序海报)")
    @ExcelProperty(value = "商品二维码地址(用户小程序海报)")
    private String codePath;

    /**
     * 淘宝京东1688类型
     */
    @Schema(description = "淘宝京东1688类型")
    @ExcelProperty(value = "淘宝京东1688类型")
    private String soureLink;

    /**
     * 主图视频链接
     */
    @Schema(description = "主图视频链接")
    @ExcelProperty(value = "主图视频链接")
    private String videoLink;

    /**
     * 运费模板ID
     */
    @Schema(description = "运费模板ID")
    @ExcelProperty(value = "运费模板ID")
    private Long tempId;

    /**
     * 规格 0单 1多
     */
    @Schema(description = "规格 0单 1多")
    @ExcelProperty(value = "规格 0单 1多")
    private Boolean specType;

    /**
     * 活动显示排序0=默认, 1=秒杀，2=砍价，3=拼团
     */
    @Schema(description = "活动显示排序0=默认, 1=秒杀，2=砍价，3=拼团")
    @ExcelProperty(value = "活动显示排序0=默认, 1=秒杀，2=砍价，3=拼团")
    private String activity;

    /**
     * 展示图
     */
    @Schema(description = "展示图")
    @ExcelProperty(value = "展示图")
    private String flatPattern;

    /**
     * 是否回收站
     */
    @Schema(description = "是否回收站")
    @ExcelProperty(value = "是否回收站")
    private Boolean isRecycle;
}