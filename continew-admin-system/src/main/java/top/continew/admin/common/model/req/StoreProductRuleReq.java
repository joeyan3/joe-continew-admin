/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.req;

import java.io.Serial;

import jakarta.validation.constraints.*;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import org.hibernate.validator.constraints.Length;

import top.continew.starter.extension.crud.model.req.BaseReq;

/**
 * 创建或修改商品规则值(规格)信息
 *
 * @author joe
 * @since 2024/09/09 08:38
 */
@Data
@Schema(description = "创建或修改商品规则值(规格)信息")
public class StoreProductRuleReq extends BaseReq {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 规格名称
     */
    @Schema(description = "规格名称")
    @NotBlank(message = "规格名称不能为空")
    @Length(max = 32, message = "规格名称长度不能超过 {max} 个字符")
    private String ruleName;

    /**
     * 规格值
     */
    @Schema(description = "规格值")
    @NotBlank(message = "规格值不能为空")
    private String ruleValue;
}