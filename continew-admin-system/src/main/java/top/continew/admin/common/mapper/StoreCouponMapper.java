/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import top.continew.admin.common.model.resp.StoreCouponResp;
import top.continew.starter.data.mybatis.plus.base.BaseMapper;
import top.continew.admin.common.model.entity.StoreCouponDO;

import java.util.List;

/**
 * 优惠券 Mapper
 *
 * @author joe
 * @since 2024/08/19 16:14
 */
public interface StoreCouponMapper extends BaseMapper<StoreCouponDO> {
    List<StoreCouponResp> getByIds(@Param("ids") List<Long> ids);

    Boolean deduction(@Param("id") Long id, @Param("num") Integer num, @Param("isLimited") Boolean isLimited);

    @Update("UPDATE t_store_coupon SET is_del = 1 WHERE id = #{id} AND is_del = 0")
    void deleteCoupon(@Param("id") Long id);

    @Update("UPDATE t_store_coupon  SET status = #{status} WHERE id = #{id} AND status != #{status}")
    void updateStatus(@Param("id") Long id, @Param("status") Boolean status);

}