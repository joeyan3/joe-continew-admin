/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import top.continew.starter.extension.crud.model.resp.BaseResp;

import java.io.Serial;

/**
 * 邀请码信息
 *
 * @author joe
 * @since 2024/07/21 19:58
 */
@Data
@Schema(description = "邀请码信息")
public class InvitationCodeResp extends BaseResp {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 账户id
     */
    @Schema(description = "账户id")
    private Long accountId;

    /**
     * 账户手机号
     */
    @Schema(description = "账户手机号")
    private String accountName;

    /**
     * 自身邀请码
     */
    @Schema(description = "自身邀请码")
    private String invitationCode;

    /**
     * 上级邀请码
     */
    @Schema(description = "上级邀请码")
    private String parentInvitationCode;

    /**
     * 邀请路径
     */
    @Schema(description = "邀请路径")
    private String invitationPath;
}