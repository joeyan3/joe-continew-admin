/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.mapper;

import top.continew.starter.data.mybatis.plus.base.BaseMapper;
import top.continew.admin.common.model.entity.RecordDO;

/**
 * 交易记录 Mapper
 *
 * @author joe
 * @since 2024/08/05 22:48
 */
public interface RecordMapper extends BaseMapper<RecordDO> {}