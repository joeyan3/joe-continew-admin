/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import top.continew.admin.common.model.resp.StoreProductAttrValueResp;
import top.continew.starter.data.mybatis.plus.base.BaseMapper;
import top.continew.admin.common.model.entity.StoreProductAttrValueDO;

import java.util.List;

/**
 * 商品属性值 Mapper
 *
 * @author joe
 * @since 2024/08/17 22:19
 */
public interface StoreProductAttrValueMapper extends BaseMapper<StoreProductAttrValueDO> {

    List<StoreProductAttrValueDO> getListByProductIdAndAttrId(@Param("productId") Long productId,
                                                              @Param("attrId") Long attrId,
                                                              @Param("type") Integer type);

    /**
     * 根据产品属性查询
     *
     */
    List<StoreProductAttrValueResp> getByEntity(@Param(Constants.WRAPPER) QueryWrapper<StoreProductAttrValueDO> queryWrapper);

    /**
     * 根据商品id删除AttrValue
     * 
     * @param productId 商品id
     * @param type      类型区分是是否添加营销
     */
    @Delete("DELETE FROM t_store_product_attr_value WHERE product_id = #{productId} AND type = #{type}")
    void removeByProductId(@Param("productId") Long productId, @Param("type") Integer type);

    /**
     * 根据id、类型查询
     */
    @Select("SELECT * FROM t_store_product_attr_value WHERE id = #{id} AND product_id = #{productId} AND type = #{type} AND is_del = 0")
    StoreProductAttrValueResp getByIdAndProductIdAndType(@Param("id") Long id,
                                                         @Param("productId") Long productId,
                                                         @Param("type") Integer type);

    /**
     * 根据sku查询
     */
    @Select("SELECT * FROM t_store_product_attr_value WHERE product_id = #{productId} AND suk = #{suk} AND type = #{type} AND is_del = 0")
    StoreProductAttrValueResp getByProductIdAndSkuAndType(@Param("productId") Long productId,
                                                          @Param("suk") String suk,
                                                          @Param("type") Integer type);

    /**
     * 添加(退货)/扣减库存
     */
    void operationStock(@Param("id") Long id,
                        @Param("num") Integer num,
                        @Param("operationType") String operationType,
                        @Param("type") Integer type);

    /**
     * 删除商品规格属性值（软删除）
     */
    @Update("UPDATE t_store_product_attr_value SET is_del = 1 WHERE product_id = #{productId} AND type = #{type}")
    void deleteByProductIdAndType(@Param("productId") Long productId, @Param("type") Integer type);

    /**
     * 获取商品规格列表
     */
    @Select("SELECT * FROM t_store_product_attr_value WHERE product_id = #{productId} AND type = #{type} AND is_del = 0")
    List<StoreProductAttrValueResp> getListByProductIdAndType(@Param("productId") Long productId,
                                                              @Param("type") Integer type);

}