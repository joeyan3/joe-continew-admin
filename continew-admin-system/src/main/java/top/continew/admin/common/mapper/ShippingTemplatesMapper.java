/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import top.continew.starter.data.mybatis.plus.base.BaseMapper;
import top.continew.admin.common.model.entity.ShippingTemplatesDO;

import java.util.List;

/**
 * 运费模板 Mapper
 *
 * @author joe
 * @since 2024/08/08 10:04
 */
public interface ShippingTemplatesMapper extends BaseMapper<ShippingTemplatesDO> {
    /**
     * 根据模板名查询
     *
     * @param name 模板名
     * @return 运费模板
     */
    @Select("SELECT * FROM t_shipping_templates WHERE name = #{name}")
    ShippingTemplatesDO getByName(@Param("name") String name);

    /**
     * 获取所有模板
     *
     * @return 运费模板
     */
    @Select("SELECT * FROM t_shipping_templates")
    List<ShippingTemplatesDO> getAll();

}