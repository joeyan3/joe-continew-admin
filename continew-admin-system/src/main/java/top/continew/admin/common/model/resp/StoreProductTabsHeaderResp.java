/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import java.io.Serial;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;

import top.continew.starter.extension.crud.model.resp.BaseDetailResp;

/**
 * 商品详情响应对象
 *
 * @author joe
 * @since 2024/08/17 17:00
 */
@Data
@ExcelIgnoreUnannotated
@Schema(description = "商品tabs")
public class StoreProductTabsHeaderResp extends BaseDetailResp {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "计数")
    private Integer count;
    @Schema(description = "tab名")
    private String name;
    // 1=出售中商品 2=仓库中商品 4=已经售罄商品 5=警戒库存 6=商品回收站
    @Schema(description = "类型")
    private Integer type;

    public StoreProductTabsHeaderResp(Integer count, String name, Integer type) {
        this.count = count;
        this.name = name;
        this.type = type;
    }
}