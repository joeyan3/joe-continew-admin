/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.req;

import java.io.Serial;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import top.continew.starter.extension.crud.model.req.BaseReq;
import org.hibernate.validator.constraints.Length;

/**
 * 创建或修改商品属性值信息
 *
 * @author joe
 * @since 2024/08/17 22:19
 */
@Data
@Schema(description = "创建或修改商品属性值信息")
public class StoreProductAttrValueReq extends BaseReq {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Schema(description = "主键")
    private Long id;

    /**
     * 商品ID
     */
    @Schema(description = "商品ID")
    private Long productId;

    /**
     * 商品属性索引值 (attr_value|attr_value[|....])
     */
    @Schema(description = "商品属性索引值 (attr_value|attr_value[|....])")
    @Length(max = 128, message = "商品属性索引值 (attr_value|attr_value[|....])长度不能超过 {max} 个字符")
    private String suk;

    /**
     * 属性对应的库存
     */
    @Schema(description = "属性对应的库存")
    private Integer stock;

    /**
     * 销量
     */
    @Schema(description = "销量")
    private Integer sales;

    /**
     * 属性金额
     */
    @Schema(description = "属性金额")
    private BigDecimal price;

    /**
     * 图片
     */
    @Schema(description = "图片")
    @Length(max = 1000, message = "图片长度不能超过 {max} 个字符")
    private String image;

    /**
     * 唯一值
     */
    @Schema(description = "唯一值")
    @Length(max = 8, message = "唯一值长度不能超过 {max} 个字符")
    private String uniqueValue;

    /**
     * 成本价
     */
    @Schema(description = "成本价")
    private BigDecimal cost;

    /**
     * 商品条码
     */
    @Schema(description = "商品条码")
    @Length(max = 50, message = "商品条码长度不能超过 {max} 个字符")
    private String barCode;

    /**
     * 原价
     */
    @Schema(description = "原价")
    private BigDecimal otPrice;

    /**
     * 重量
     */
    @Schema(description = "重量")
    private BigDecimal weight;

    /**
     * 体积
     */
    @Schema(description = "体积")
    private BigDecimal volume;

    /**
     * 一级返佣
     */
    @Schema(description = "一级返佣")
    private BigDecimal brokerage;

    /**
     * 二级返佣
     */
    @Schema(description = "二级返佣")
    private BigDecimal brokerageTwo;

    /**
     * 活动类型 0=商品，1=秒杀，2=砍价，3=拼团
     */
    @Schema(description = "活动类型 0=商品，1=秒杀，2=砍价，3=拼团")
    private Integer type;

    /**
     * 活动限购数量
     */
    @Schema(description = "活动限购数量")
    private Integer quota;

    /**
     * 活动限购数量显示
     */
    @Schema(description = "活动限购数量显示")
    private Integer quotaShow;

    /**
     * attr_values 创建更新时的属性对应
     */
    @Schema(description = "attr_values 创建更新时的属性对应")
    @Length(max = 65535, message = "attr_values 创建更新时的属性对应长度不能超过 {max} 个字符")
    private String attrValue;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @Schema(description = "更新时间")
    private LocalDateTime updateTime;

    @Schema(description = "是否选中-秒杀用")
    private Boolean checked;

    @Schema(description = "砍价商品最低价")
    private BigDecimal minPrice;
}