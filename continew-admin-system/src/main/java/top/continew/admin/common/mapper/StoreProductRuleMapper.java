/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.mapper;

import org.apache.ibatis.annotations.Select;
import top.continew.starter.data.mybatis.plus.base.BaseMapper;
import top.continew.admin.common.model.entity.StoreProductRuleDO;

import java.util.List;

/**
 * 商品规则值(规格) Mapper
 *
 * @author joe
 * @since 2024/09/09 08:38
 */
public interface StoreProductRuleMapper extends BaseMapper<StoreProductRuleDO> {
    /**
     * 获取所有商品规则值(规格)
     *
     * @return 商品规则值(规格)
     */
    @Select("SELECT * FROM t_store_product_rule")
    List<StoreProductRuleDO> getAll();
}