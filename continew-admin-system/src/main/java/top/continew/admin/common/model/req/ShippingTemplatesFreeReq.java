/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.req;

import java.io.Serial;
import java.math.BigDecimal;

import jakarta.validation.constraints.*;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import org.hibernate.validator.constraints.Length;
import top.continew.starter.extension.crud.model.req.BaseReq;

/**
 * 创建或修改运费模板包邮信息
 *
 * @author joe
 * @since 2024/08/08 10:40
 */
@Data
@Schema(description = "创建或修改运费模板包邮信息")
public class ShippingTemplatesFreeReq extends BaseReq {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 描述
     */
    @Schema(description = "描述")
    @Length(max = 65535, message = "描述长度不能超过 {max} 个字符")
    private String title;

    /**
     * 模板ID
     */
    @Schema(description = "模板ID")
    @NotNull(message = "模板ID不能为空")
    private Long tempId;

    /**
     * 城市ID
     */
    @Schema(description = "城市ID")
    @NotNull(message = "城市ID不能为空")
    private String cityId;

    /**
     * 包邮件数
     */
    @Schema(description = "包邮件数")
    @NotNull(message = "包邮件数不能为空")
    private BigDecimal number;

    /**
     * 包邮金额
     */
    @Schema(description = "包邮金额")
    @NotNull(message = "包邮金额不能为空")
    private BigDecimal price;

    /**
     * 计费方式
     */
    @Schema(description = "计费方式")
    @NotBlank(message = "计费方式不能为空")
    private Integer type;
}