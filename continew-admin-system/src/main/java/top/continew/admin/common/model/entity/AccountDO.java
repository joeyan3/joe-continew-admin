/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import top.continew.admin.common.config.mybatis.BCryptEncryptor;
import top.continew.starter.extension.crud.model.entity.BaseDO;
import top.continew.starter.security.crypto.annotation.FieldEncrypt;

import java.io.Serial;

/**
 * 商户列表实体
 *
 * @author joe
 * @since 2024/07/22 23:08
 */
@Data
@TableName("t_account")
public class AccountDO extends BaseDO {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 账户名
     */
    private String accountName;

    /**
     * 姓名
     */
    private String name;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 头像
     */
    private String headImg;

    /**
     * 密码
     */
    @FieldEncrypt(encryptor = BCryptEncryptor.class)
    private String password;

    /**
     * 支付密码
     */
    @FieldEncrypt(encryptor = BCryptEncryptor.class)
    private String payPassword;

    /**
     * 冻结
     */
    private Boolean isFreeze;

    /**
     * 是否会员
     */
    private Boolean isActive;

    /**
     * 超级用户
     */
    private Boolean isSystem;

    /**
     * 是代理商
     */
    private Boolean isAgent;

    /**
     * 是否注销
     */
    private Boolean isDel;

    /**
     * 是否商户
     */
    private Boolean isShop;

    /**
     * 店铺认证
     */
    private Integer shopStatus;

    /**
     * 店铺码
     */
    private String shopCode;
}