/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.req;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import top.continew.starter.extension.crud.model.req.BaseReq;
import top.continew.starter.extension.crud.util.ValidateGroup;

import java.io.Serial;

/**
 * 创建或修改商户列表信息
 *
 * @author joe
 * @since 2024/07/17 12:14
 */
@Data
@Schema(description = "创建或修改商户列表信息")
public class AccountAddReq extends BaseReq {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 账户名
     */
    @Schema(description = "账户名")
    private String accountName;

    /**
     * 姓名
     */
    @Schema(description = "姓名")
    @Length(max = 65535, message = "姓名长度不能超过 {max} 个字符")
    private String name;

    /**
     * 昵称
     */
    @Schema(description = "昵称")
    @Length(max = 65535, message = "昵称长度不能超过 {max} 个字符")
    private String nickName;

    /**
     * 冻结
     */
    @Schema(description = "冻结")
    private Boolean isFreeze;

    /**
     * 是否会员
     */
    @Schema(description = "是否会员")
    private Boolean isActive;

    /**
     * 超级用户
     */
    @Schema(description = "超级用户")
    private Boolean isSystem;

    /**
     * 是代理商
     */
    @Schema(description = "是代理商")
    private Boolean isAgent;

    /**
     * 是否商户
     */
    @Schema(description = "是否商户")
    private Boolean isShop;

    /**
     * 密码（加密）
     */
    @Schema(description = "密码（加密）", example = "E7c72TH+LDxKTwavjM99W1MdI9Lljh79aPKiv3XB9MXcplhm7qJ1BJCj28yaflbdVbfc366klMtjLIWQGqb0qw==")
    @NotBlank(message = "密码不能为空", groups = ValidateGroup.Crud.Add.class)
    private String password;

    /**
     * 邀请码
     */
    @Schema(description = "邀请码")
    private String invitationCode;

    /**
     * 二维码
     */
    //    @Schema(description = "二维码")
    //    private String code;
}