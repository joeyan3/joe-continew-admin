/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import java.io.Serial;
import java.math.BigDecimal;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import top.continew.starter.extension.crud.model.resp.BaseDetailResp;

/**
 * 运费模板包邮详情信息
 *
 * @author joe
 * @since 2024/08/08 10:40
 */
@Data
@ExcelIgnoreUnannotated
@Schema(description = "运费模板包邮详情信息")
public class ShippingTemplatesFreeDetailResp extends BaseDetailResp {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 模板ID
     */
    @Schema(description = "模板ID")
    @ExcelProperty(value = "模板ID")
    private Long tempId;

    /**
     * 城市ID
     */
    @Schema(description = "城市ID")
    @ExcelProperty(value = "城市ID")
    private Long cityId;

    /**
     * 描述
     */
    @Schema(description = "描述")
    @ExcelProperty(value = "描述")
    private String title;

    /**
     * 包邮件数
     */
    @Schema(description = "包邮件数")
    @ExcelProperty(value = "包邮件数")
    private BigDecimal number;

    /**
     * 包邮金额
     */
    @Schema(description = "包邮金额")
    @ExcelProperty(value = "包邮金额")
    private BigDecimal price;

    /**
     * 计费方式
     */
    @Schema(description = "计费方式")
    @ExcelProperty(value = "计费方式")
    private String type;

    /**
     * 分组唯一值
     */
    @Schema(description = "分组唯一值")
    @ExcelProperty(value = "分组唯一值")
    private String uniqid;

    /**
     * 是否无效
     */
    @Schema(description = "是否无效")
    @ExcelProperty(value = "是否无效")
    private Boolean status;
}