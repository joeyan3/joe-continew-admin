/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.req;

import java.io.Serial;

import jakarta.validation.constraints.*;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import top.continew.starter.extension.crud.model.req.BaseReq;

/**
 * 创建或修改分类信息
 *
 * @author joe
 * @since 2024/08/14 13:03
 */
@Data
@Schema(description = "创建或修改分类信息")
public class CategoryReq extends BaseReq {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 父级ID
     */
    @Schema(description = "父级ID")
    private Long pid;

    /**
     * 分类名称
     */
    @Schema(description = "分类名称")
    @NotBlank(message = "分类名称不能为空")
    private String name;

    /**
     * 类型，1 产品分类，2 附件分类，3 文章分类， 4 设置分类， 5 菜单分类，6 配置分类， 7 秒杀配置
     */
    @Schema(description = "类型，1 产品分类，2 附件分类，3 文章分类， 4 设置分类， 5 菜单分类，6 配置分类， 7 秒杀配置")
    private Integer type;

    /**
     * 状态, 1正常，0失效
     */
    @Schema(description = "状态, 1正常，0失效")
    @NotNull(message = "状态, 1正常，0失效不能为空")
    private Boolean status;

    /**
     * 排序
     */
    @Schema(description = "排序")
    @NotNull(message = "排序不能为空")
    private Integer sort;
}