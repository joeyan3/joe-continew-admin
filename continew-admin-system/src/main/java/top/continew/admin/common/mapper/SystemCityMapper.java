/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import top.continew.starter.data.mybatis.plus.base.BaseMapper;
import top.continew.admin.common.model.entity.SystemCityDO;

import java.util.List;

/**
 * 城市 Mapper
 *
 * @author joe
 * @since 2024/08/08 21:46
 */
public interface SystemCityMapper extends BaseMapper<SystemCityDO> {
    /**
     * 根据父级id获取数据
     *
     * @param parentId 父级id
     */
    @Select("SELECT * FROM eb_system_city WHERE is_show = 1 AND parent_id = #{parentId}")
    List<SystemCityDO> getByParentId(@Param("parentId") Long parentId);

    /**
     * 根据父级id获取数据
     */
    @Select("SELECT DISTINCT city_id FROM eb_system_city WHERE level = 1 AND is_show = 1")
    List<Long> getCityIdList();

    /**
     * 根据city_id获取城市信息
     */
    @Select("SELECT * FROM eb_system_city WHERE city_id = #{cityId} AND is_show = 1 LIMIT 1")
    SystemCityDO getCityByCityId(@Param("cityId") Long cityId);

    /**
     * 根据城市名称获取城市详细数据
     */
    @Select("SELECT * FROM eb_system_city WHERE name = #{cityName} AND is_show = 1 AND level = 1 LIMIT 1")
    SystemCityDO getCityByCityName(@Param("cityName") String cityName);

    @Select("SELECT id, city_id, parent_id, name FROM eb_system_city WHERE is_show = 1")
    List<SystemCityDO> selectList();

    @Select("SELECT * FROM eb_system_city WHERE parent_id = #{pid} AND is_show = 1")
    List<SystemCityDO> selectListByParentId(@Param("pid") Long pid);
}