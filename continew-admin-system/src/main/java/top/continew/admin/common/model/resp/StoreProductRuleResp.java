/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import java.io.Serial;
import java.time.LocalDateTime;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import top.continew.starter.extension.crud.model.resp.BaseResp;

/**
 * 商品规则值(规格)信息
 *
 * @author joe
 * @since 2024/09/09 08:38
 */
@Data
@Schema(description = "商品规则值(规格)信息")
public class StoreProductRuleResp extends BaseResp {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 规格名称
     */
    @Schema(description = "规格名称")
    private String ruleName;

    /**
     * 规格值
     */
    @Schema(description = "规格值")
    private String ruleValue;

    /**
     * 修改人
     */
    @Schema(description = "修改人")
    private Long updateUser;

    /**
     * 
     */
    @Schema(description = "")
    private LocalDateTime updateTime;
}