/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import top.continew.starter.extension.crud.model.req.BaseReq;

import java.io.Serial;

/**
 * 创建或修改商铺认证信息
 *
 * @author joe
 * @since 2024/07/30 15:48
 */
@Data
@Schema(description = "创建或修改商铺认证信息")
public class PersonShopIdentificationReq extends BaseReq {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 审核状态
     */
    @Schema(description = "审核状态")
    @Length(max = 1, message = "审核状态长度不能超过 {max} 个字符")
    private String status;

    /**
     * 错误信息
     */
    @Schema(description = "错误信息")
    @Length(max = 500, message = "错误信息长度不能超过 {max} 个字符")
    private String errMsg;
}