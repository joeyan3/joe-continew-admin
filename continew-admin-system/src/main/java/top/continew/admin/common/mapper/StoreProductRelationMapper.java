/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import top.continew.admin.common.model.resp.StoreProductRelationResp;
import top.continew.starter.data.mybatis.plus.base.BaseMapper;
import top.continew.admin.common.model.entity.StoreProductRelationDO;

import java.util.List;

/**
 * 商品点赞和收藏 Mapper
 *
 * @author joe
 * @since 2024/08/18 15:12
 */
public interface StoreProductRelationMapper extends BaseMapper<StoreProductRelationDO> {
    /**
     * 取消收藏产品
     *
     * @param productId 商品id
     * @param type      类型区分是是否添加营销
     */
    @Delete("DELETE FROM t_store_product_relation WHERE product_id IN (#{productId}) AND category = #{category} AND uid = #{uid} AND type = #{type}")
    void deleteAll(@Param("productId") Long productId,
                   @Param("category") String category,
                   @Param("uid") Long uid,
                   @Param("type") String type);

    /**
     * 根据产品id和类型获取对应列表
     * 
     * @param productId 产品id
     * @param type      类型
     * @return 对应结果
     */
    @Select("SELECT * FROM t_store_product_relation WHERE product_id = #{productId} AND type = #{type}")
    List<StoreProductRelationResp> getList(@Param("productId") Long productId, @Param("type") String type);

    /**
     * 获取用户当前是否喜欢该商品
     * 
     * @param uid       用户id
     * @param productId 商品id
     * @return 是否喜欢标识
     */
    @Select("SELECT * FROM t_store_product_relation WHERE uid = #{uid} AND product_id = #{productId} AND type = #{type}")
    List<StoreProductRelationResp> getLikeOrCollectByUser(@Param("uid") Long uid,
                                                          @Param("productId") Long productId,
                                                          @Param("type") String type);

    /**
     * 获取用户的收藏数量
     * 
     * @param uid 用户uid
     * @return 收藏数量
     */
    @Select("SELECT COUNT(*) FROM t_store_product_relation WHERE uid = #{uid} AND type = 'collect'")
    Integer getCollectCountByUid(@Param("uid") Long uid);

    /**
     * 根据商品Id取消收藏
     *
     * @param productId 商品id
     */
    @Delete("DELETE FROM t_store_product_relation WHERE product_id = #{productId}")
    void deleteByProId(@Param("productId") Long productId);

    /**
     * 根据商品Id取消收藏
     *
     * @param productId 商品id
     */
    @Delete("DELETE FROM t_store_product_relation WHERE product_id = #{productId} AND uid = #{uid}")
    void deleteByProIdAndUid(@Param("productId") Long productId, @Param("uid") Long uid);

    /**
     * 根据日期获取收藏量
     * 
     * @param date 日期，yyyy-MM-dd格式
     * @return Integer
     */
    @Select("SELECT COUNT(id) FROM t_store_product_relation WHERE DATE_FORMAT(create_time, '%Y-%m-%d') = #{date}")
    Integer getCountByDate(@Param("date") String date);

    /**
     * 根据日期获取收藏量
     * 
     * @param date      日期，yyyy-MM-dd格式
     * @param productId 商品id
     * @return Integer
     */
    @Select("SELECT COUNT(id) FROM t_store_product_relation WHERE product_id = #{productId} AND DATE_FORMAT(create_time, '%Y-%m-%d') = #{date}")
    Integer getCountByDateAndProId(@Param("productId") Long productId, @Param("date") String date);

}