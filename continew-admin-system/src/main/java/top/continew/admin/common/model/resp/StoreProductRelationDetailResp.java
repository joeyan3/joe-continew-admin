/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import java.io.Serial;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import top.continew.starter.extension.crud.model.resp.BaseDetailResp;

/**
 * 商品点赞和收藏详情信息
 *
 * @author joe
 * @since 2024/08/18 15:12
 */
@Data
@ExcelIgnoreUnannotated
@Schema(description = "商品点赞和收藏详情信息")
public class StoreProductRelationDetailResp extends BaseDetailResp {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    @ExcelProperty(value = "用户ID")
    private Long uid;

    /**
     * 商品ID
     */
    @Schema(description = "商品ID")
    @ExcelProperty(value = "商品ID")
    private Long productId;

    /**
     * 类型(收藏(collect）、点赞(like))
     */
    @Schema(description = "类型(收藏(collect）、点赞(like))")
    @ExcelProperty(value = "类型(收藏(collect）、点赞(like))")
    private String type;

    /**
     * 某种类型的商品(普通商品、秒杀商品)
     */
    @Schema(description = "某种类型的商品(普通商品、秒杀商品)")
    @ExcelProperty(value = "某种类型的商品(普通商品、秒杀商品)")
    private String category;
}