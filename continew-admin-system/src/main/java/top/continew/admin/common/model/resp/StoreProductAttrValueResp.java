/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import java.io.Serial;
import java.math.BigDecimal;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import top.continew.starter.extension.crud.model.resp.BaseResp;

/**
 * 商品属性值信息
 *
 * @author joe
 * @since 2024/08/17 22:19
 */
@Data
@Schema(description = "商品属性值信息")
public class StoreProductAttrValueResp extends BaseResp {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "attrId")
    private Long id;

    @Schema(description = "商品ID")
    private Long productId;

    @Schema(description = "商品属性索引值 (attr_description|attr_description[|....])")
    private String suk;

    @Schema(description = "属性对应的库存")
    private Integer stock;

    @Schema(description = "销量")
    private Integer sales;

    @Schema(description = "属性金额")
    private BigDecimal price;

    @Schema(description = "图片")
    private String image;

    @Schema(description = "唯一值")
    private String uniqueValue;

    @Schema(description = "成本价")
    private BigDecimal cost;

    @Schema(description = "商品条码")
    private String barCode;

    @Schema(description = "原价")
    private BigDecimal otPrice;

    @Schema(description = "重量")
    private BigDecimal weight;

    @Schema(description = "体积")
    private BigDecimal volume;

    @Schema(description = "一级返佣")
    private BigDecimal brokerage;

    @Schema(description = "二级返佣")
    private BigDecimal brokerageTwo;

    @Schema(description = "活动类型 0=商品，1=秒杀，2=砍价，3=拼团")
    private Integer type;

    @Schema(description = "活动限购数量")
    private Integer quota;

    @Schema(description = "活动限购数量显示")
    private Integer quotaShow;

    @Schema(description = "产品属性值和属性名对应关系")
    private String attrdescription;

    @Schema(description = "是否删除,0-否，1-是")
    private Boolean isDel;
}