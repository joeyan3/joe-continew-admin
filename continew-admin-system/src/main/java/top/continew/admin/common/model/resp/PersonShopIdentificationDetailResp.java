/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import top.continew.starter.extension.crud.model.resp.BaseDetailResp;

import java.io.Serial;

/**
 * 商铺认证详情信息
 *
 * @author joe
 * @since 2024/07/30 15:48
 */
@Data
@ExcelIgnoreUnannotated
@Schema(description = "商铺认证详情信息")
public class PersonShopIdentificationDetailResp extends BaseDetailResp {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    @ExcelProperty(value = "用户ID")
    private Long accountId;

    /**
     * 企业名称
     */
    @Schema(description = "企业名称")
    @ExcelProperty(value = "企业名称")
    private String companyName;

    /**
     * 企业税号
     */
    @Schema(description = "企业税号")
    @ExcelProperty(value = "企业税号")
    private String companyTax;

    /**
     * 银行卡号
     */
    @Schema(description = "银行卡号")
    @ExcelProperty(value = "银行卡号")
    private String cardNo;

    /**
     * 开户行支行地址
     */
    @Schema(description = "开户行支行地址")
    @ExcelProperty(value = "开户行支行地址")
    private String bankAddress;

    /**
     * 法人姓名
     */
    @Schema(description = "法人姓名")
    @ExcelProperty(value = "法人姓名")
    private String name;

    /**
     * 身份证号码
     */
    @Schema(description = "身份证号码")
    @ExcelProperty(value = "身份证号码")
    private String personId;

    /**
     * 支付宝账号
     */
    @Schema(description = "支付宝账号")
    @ExcelProperty(value = "支付宝账号")
    private String alipayNo;

    /**
     * 店铺名称
     */
    @Schema(description = "店铺名称")
    @ExcelProperty(value = "店铺名称")
    private String shopName;

    /**
     * 主营类目
     */
    @Schema(description = "主营类目")
    @ExcelProperty(value = "主营类目")
    private String businessCategory;

    /**
     * 营业执照副本
     */
    @Schema(description = "营业执照副本")
    @ExcelProperty(value = "营业执照副本")
    private String businessLicense;

    /**
     * 法人身份证正面
     */
    @Schema(description = "法人身份证正面")
    @ExcelProperty(value = "法人身份证正面")
    private String passportFaceImg;

    /**
     * 法人身份证背面
     */
    @Schema(description = "法人身份证背面")
    @ExcelProperty(value = "法人身份证背面")
    private String passportSideImg;

    /**
     * 法人身份证手持
     */
    @Schema(description = "法人身份证手持")
    @ExcelProperty(value = "法人身份证手持")
    private String passportHandImg;

    /**
     * 公户银行卡正面
     */
    @Schema(description = "公户银行卡正面")
    @ExcelProperty(value = "公户银行卡正面")
    private String bankcardFront;

    /**
     * 公户银行卡背面
     */
    @Schema(description = "公户银行卡背面")
    @ExcelProperty(value = "公户银行卡背面")
    private String bankcardEnd;

    /**
     * 审核不通过次数
     */
    @Schema(description = "审核不通过次数")
    @ExcelProperty(value = "审核不通过次数")
    private Integer failNum;

    /**
     * 审核状态
     */
    @Schema(description = "审核状态")
    @ExcelProperty(value = "审核状态")
    private String status;

    /**
     * 错误信息
     */
    @Schema(description = "错误信息")
    @ExcelProperty(value = "错误信息")
    private String errMsg;
}