/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import top.continew.admin.common.model.resp.StoreProductDescriptionResp;
import top.continew.starter.data.mybatis.plus.base.BaseMapper;
import top.continew.admin.common.model.entity.StoreProductDescriptionDO;

/**
 * 商品描述 Mapper
 *
 * @author joe
 * @since 2024/08/18 14:17
 */
public interface StoreProductDescriptionMapper extends BaseMapper<StoreProductDescriptionDO> {
    /**
     * 根据商品id和type删除对应描述
     *
     * @param productId 商品id
     * @param type      类型区分是是否添加营销
     */
    @Delete("DELETE FROM t_store_product_description WHERE product_id = #{productId} AND type = #{type}")
    void deleteByProductId(@Param("productId") Long productId, @Param("type") Integer type);

    /**
     * 获取详情
     * 
     * @param productId 商品id
     * @param type      商品类型
     * @return StoreProductDescription
     */
    @Select("SELECT * FROM t_store_product_description WHERE product_id = #{productId} AND type = #{type} LIMIT 1")
    StoreProductDescriptionResp getByProductIdAndType(@Param("productId") Long productId, @Param("type") Integer type);

}