/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.req;

import java.io.Serial;
import java.math.BigDecimal;

import jakarta.validation.constraints.*;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import org.hibernate.validator.constraints.Length;

import top.continew.starter.extension.crud.model.req.BaseReq;

/**
 * 创建或修改运费模板指定区域费用信息
 *
 * @author joe
 * @since 2024/08/08 12:14
 */
@Data
@Schema(description = "创建或修改运费模板指定区域费用信息")
public class ShippingTemplatesRegionReq extends BaseReq {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 模板ID
     */
    @Schema(description = "模板ID")
    private Long tempId;

    /**
     * 城市ID
     */
    @Schema(description = "城市ID")
    @NotNull(message = "城市ID不能为空")
    private String cityId;

    /**
     * 描述
     */
    @Schema(description = "描述")
    @Length(max = 65535, message = "描述长度不能超过 {max} 个字符")
    private String title;

    /**
     * 首件
     */
    @Schema(description = "首件")
    @NotNull(message = "首件不能为空")
    private BigDecimal first;

    /**
     * 首件运费
     */
    @Schema(description = "首件运费")
    @NotNull(message = "首件运费不能为空")
    private BigDecimal firstPrice;

    /**
     * 续件
     */
    @Schema(description = "续件")
    @NotNull(message = "续件不能为空")
    private BigDecimal renewal;

    /**
     * 续件运费
     */
    @Schema(description = "续件运费")
    @NotNull(message = "续件运费不能为空")
    private BigDecimal renewalPrice;

    /**
     * 计费方式 1按件数 2按重量 3按体积
     */
    @Schema(description = "计费方式 1按件数 2按重量 3按体积")
    @NotBlank(message = "计费方式 1按件数 2按重量 3按体积不能为空")
    private Integer type;

    @Schema(description = "分组唯一值")
    private String uniqid;
}