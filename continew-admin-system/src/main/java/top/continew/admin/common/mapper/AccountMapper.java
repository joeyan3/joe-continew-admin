/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import top.continew.admin.common.model.resp.AccountResp;
import top.continew.starter.data.mybatis.plus.base.BaseMapper;
import top.continew.admin.common.model.entity.AccountDO;
import top.continew.starter.data.mybatis.plus.datapermission.DataPermission;

/**
 * 商户列表 Mapper
 *
 * @author joe
 * @since 2024/07/17 12:14
 */
public interface AccountMapper extends BaseMapper<AccountDO> {

    /**
     * 根据商户名查询
     *
     * @param accountname 商户名
     * @return 商户信息
     */
    @Select("SELECT * FROM t_account WHERE account_name = #{accountname}")
    AccountDO getByAccountName(@Param("accountname") String accountname);

    /**
     * 冻结商户
     *
     * @param accountId 商户ID
     * @return 商户信息
     */
    @Update("UPDATE t_account SET is_del = 1 WHERE id = #{accountId}")
    void softDelAccount(@Param("accountId") Long accountId);

    /**
     * 冻结商户
     *
     * @param accountId 商户ID
     * @return 商户信息
     */
    @Update("UPDATE t_account SET is_freeze = 1 WHERE id = #{accountId}")
    void freezeAccount(@Param("accountId") Long accountId);

    /**
     * 解冻商户
     *
     * @param accountId 商户ID
     * @return 商户信息
     */
    @Update("UPDATE t_account SET is_freeze = 0 WHERE id = #{accountId}")
    void unfreezeAccount(@Param("accountId") Long accountId);

    /**
     * 分页查询列表
     *
     * @param page         分页查询条件
     * @param queryWrapper 查询条件
     * @return 分页信息
     */
    @DataPermission("a")
    IPage<AccountResp> queryAccountPageList(@Param("page") IPage<AccountDO> page,
                                            @Param(Constants.WRAPPER) QueryWrapper<AccountDO> queryWrapper);

    //    /**
    //     * 注册人数
    //     *
    //     * @param dayStartTime 开始时间
    //     * @param dayEndTime 结束时间
    //     * @return 商户信息
    //     */
    //    @Select("SELECT COUNT(*) FROM t_account WHERE create_time BETWEEN #{dayStartTime} AND #{dayEndTime}")
    //    BigInteger getRegisterNum(@Param("dayStartTime") Date dayStartTime,@Param("dayEndTime") Date dayEndTime);

}