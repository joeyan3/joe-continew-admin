/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import top.continew.starter.data.mybatis.plus.base.BaseMapper;
import top.continew.admin.common.model.entity.InvitationGroupCountDO;

/**
 * 邀请组 Mapper
 *
 * @author joe
 * @since 2024/07/21 20:25
 */
public interface InvitationGroupCountMapper extends BaseMapper<InvitationGroupCountDO> {
    /**
     * 更改账号
     *
     * @param invitationCode 原始账号
     * @param accountName    新账号
     */
    @Update("UPDATE t_invitation_group_count SET account_name = #{accountName} WHERE invitation_code = #{invitationCode}")
    void updateAccountName(@Param("accountId") String accountName, @Param("invitationCode") String invitationCode);
}