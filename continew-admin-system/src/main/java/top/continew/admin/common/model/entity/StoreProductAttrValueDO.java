/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.entity;

import java.io.Serial;
import java.math.BigDecimal;

import lombok.Data;

import com.baomidou.mybatisplus.annotation.TableName;

import top.continew.starter.extension.crud.model.entity.BaseDO;

/**
 * 商品属性值实体
 *
 * @author joe
 * @since 2024/08/17 22:19
 */
@Data
@TableName("t_store_product_attr_value")
public class StoreProductAttrValueDO extends BaseDO {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 商品ID
     */
    private Long productId;

    /**
     * 商品属性索引值 (attr_value|attr_value[|....])
     */
    private String suk;

    /**
     * 属性对应的库存
     */
    private Integer stock;

    /**
     * 销量
     */
    private Integer sales;

    /**
     * 属性金额
     */
    private BigDecimal price;

    /**
     * 图片
     */
    private String image;

    /**
     * 唯一值
     */
    private String uniqueValue;

    /**
     * 成本价
     */
    private BigDecimal cost;

    /**
     * 商品条码
     */
    private String barCode;

    /**
     * 原价
     */
    private BigDecimal otPrice;

    /**
     * 重量
     */
    private BigDecimal weight;

    /**
     * 体积
     */
    private BigDecimal volume;

    /**
     * 一级返佣
     */
    private BigDecimal brokerage;

    /**
     * 二级返佣
     */
    private BigDecimal brokerageTwo;

    /**
     * 活动类型 0=商品，1=秒杀，2=砍价，3=拼团
     */
    private Integer type;

    /**
     * 活动限购数量
     */
    private Integer quota;

    /**
     * 活动限购数量显示
     */
    private Integer quotaShow;

    /**
     * attr_values 创建更新时的属性对应
     */
    private String attrValue;

    /**
     * 是否删除,0-否，1-是
     */
    private Boolean isDel;
}