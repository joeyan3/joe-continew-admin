/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import top.continew.admin.common.model.resp.StoreProductAttrResultResp;
import top.continew.starter.data.mybatis.plus.base.BaseMapper;
import top.continew.admin.common.model.entity.StoreProductAttrResultDO;

import java.util.List;

/**
 * 商品属性详情 Mapper
 *
 * @author joe
 * @since 2024/08/17 22:07
 */
public interface StoreProductAttrResultMapper extends BaseMapper<StoreProductAttrResultDO> {
    /**
     * 根据商品属性值集合查询
     *
     */
    List<StoreProductAttrResultResp> getByEntity(@Param(Constants.WRAPPER) QueryWrapper<StoreProductAttrResultDO> queryWrapper);

}