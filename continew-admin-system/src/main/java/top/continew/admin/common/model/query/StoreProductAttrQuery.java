/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.query;

import java.io.Serial;
import java.io.Serializable;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;
import top.continew.starter.data.core.annotation.Query;
import top.continew.starter.data.core.enums.QueryType;

/**
 * 商品属性查询条件
 *
 * @author joe
 * @since 2024/08/17 21:58
 */
@Data
@Schema(description = "商品属性查询条件")
public class StoreProductAttrQuery implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 商品ID
     */
    @Schema(description = "商品ID")
    @Query(type = QueryType.EQ)
    private Long productId;

    /**
     * 属性名
     */
    @Schema(description = "属性名")
    @Query(type = QueryType.EQ)
    private String attrName;

    /**
     * 属性值
     */
    @Schema(description = "属性值")
    @Query(type = QueryType.EQ)
    private String attrValues;

    /**
     * 活动类型 0=商品，1=秒杀，2=砍价，3=拼团
     */
    @Schema(description = "活动类型 0=商品，1=秒杀，2=砍价，3=拼团")
    @Query(type = QueryType.EQ)
    private Integer type;

    /**
     * 是否删除,0-否，1-是
     */
    @Schema(description = "是否删除,0-否，1-是")
    @Query(type = QueryType.EQ)
    private Boolean isDel;
}