/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import top.continew.admin.common.model.resp.StoreProductAttrResp;
import top.continew.starter.data.mybatis.plus.base.BaseMapper;
import top.continew.admin.common.model.entity.StoreProductAttrDO;

import java.util.List;

/**
 * 商品属性 Mapper
 *
 * @author joe
 * @since 2024/08/17 21:58
 */
public interface StoreProductAttrMapper extends BaseMapper<StoreProductAttrDO> {
    /**
     * 根据基本属性查询商品属性详情
     *
     */
    List<StoreProductAttrResp> getByEntity(@Param(Constants.WRAPPER) QueryWrapper<StoreProductAttrDO> queryWrapper);

    /**
     * 根据id删除商品属性
     * 
     * @param productId 待删除商品id
     * @param type      类型区分是是否添加营销
     */
    @Delete("DELETE FROM t_store_product_attr WHERE product_id = #{productId} AND type = #{type}")
    void removeByProductId(@Param("productId") Long productId, @Param("type") Integer type);

    /**
     * 删除商品属性
     * 
     * @param productId 商品id
     * @param type      商品类型
     */
    @Update("UPDATE t_store_product_attr SET is_del = 1 WHERE product_id = #{productId} AND type = #{type}")
    void deleteByProductIdAndType(@Param("productId") Long productId, @Param("type") Integer type);

    /**
     * 获取商品规格列表
     * 
     * @param productId 商品id
     * @param type      商品类型
     */
    @Select("SELECT * FROM t_store_product_attr WHERE product_id = #{productId} AND type = #{type} AND is_del = 0")
    List<StoreProductAttrResp> getListByProductIdAndType(@Param("productId") Long productId,
                                                         @Param("type") Integer type);

}