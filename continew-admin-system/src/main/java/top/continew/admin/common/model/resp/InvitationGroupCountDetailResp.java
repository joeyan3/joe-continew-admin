/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import top.continew.starter.extension.crud.model.resp.BaseDetailResp;

import java.io.Serial;
import java.math.BigDecimal;

/**
 * 邀请组详情信息
 *
 * @author joe
 * @since 2024/07/21 20:25
 */
@Data
@ExcelIgnoreUnannotated
@Schema(description = "邀请组详情信息")
public class InvitationGroupCountDetailResp extends BaseDetailResp {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 邀请码
     */
    @Schema(description = "邀请码")
    @ExcelProperty(value = "邀请码")
    private String invitationCode;

    /**
     * 用户账号
     */
    @Schema(description = "用户账号")
    @ExcelProperty(value = "用户账号")
    private String accountName;

    /**
     * 团队人数
     */
    @Schema(description = "团队人数")
    @ExcelProperty(value = "团队人数")
    private Long groupCount;

    /**
     * 推荐人数
     */
    @Schema(description = "推荐人数")
    @ExcelProperty(value = "推荐人数")
    private Long childCount;

    /**
     * 激活人数
     */
    @Schema(description = "激活人数")
    @ExcelProperty(value = "激活人数")
    private Long activeCount;

    /**
     * 认证人数
     */
    @Schema(description = "认证人数")
    @ExcelProperty(value = "认证人数")
    private Long identCount;

    /**
     * 总业绩
     */
    @Schema(description = "总业绩")
    @ExcelProperty(value = "总业绩")
    private BigDecimal totalMoney;

    /**
     * 新增业绩
     */
    @Schema(description = "新增业绩")
    @ExcelProperty(value = "新增业绩")
    private BigDecimal newMoney;

    /**
     * 权益积分
     */
    @Schema(description = "权益积分")
    @ExcelProperty(value = "权益积分")
    private BigDecimal totalScore;
}