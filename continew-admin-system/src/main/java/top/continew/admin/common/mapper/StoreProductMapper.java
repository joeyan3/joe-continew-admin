/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import top.continew.admin.common.model.resp.StoreProductResp;
import top.continew.starter.data.mybatis.plus.base.BaseMapper;
import top.continew.admin.common.model.entity.StoreProductDO;

import java.util.List;

/**
 * 商品 Mapper
 *
 * @author joe
 * @since 2024/08/17 17:00
 */
public interface StoreProductMapper extends BaseMapper<StoreProductDO> {
    /**
     * 根据商品id集合获取
     */
    @Select("SELECT * FROM t_store_product  WHERE id IN (#{pathIdList}) AND is_del = 0")
    List<StoreProductResp> getListInIds(@Param("pathIdList") List<Long> pathIdList);

    /**
     * 恢复已删除的商品
     * 
     * @param productId 商品id
     */
    @Update("UPDATE t_store_product SET is_recycle = 0 WHERE id = #{productId}")
    Integer reStoreProduct(@Param("productId") Long productId);

    /**
     * 获取购物车商品信息
     */
    @Select("SELECT id, image, store_name FROM t_store_product WHERE id = #{productId}")
    StoreProductDO getCartByProId(@Param("productId") Long productId);

    /**
     * 根据日期获取新增商品数量
     */
    @Select("SELECT COUNT(id) FROM t_store_product WHERE is_del = 0 AND date_format(add_time, '%Y-%m-%d') = #{date}")
    Integer getNewProductByDate(@Param("date") String date);

    /**
     * 获取所有未删除的商品
     */
    @Select("SELECT id FROM t_store_product WHERE is_del = 0")
    List<StoreProductDO> findAllProductByNotDelte();

    /**
     * 模糊搜索商品名称
     */
    @Select("SELECT id FROM t_store_product WHERE store_name LIKE CONCAT('%', #{productName}, '%') AND is_del = 0")
    List<StoreProductDO> likeProductName(@Param("productName") String productName);

    /**
     * 警戒库存数量
     */
    @Select("SELECT COUNT(*) FROM t_store_product WHERE stock <= #{stock} AND is_recycle = 0 AND is_del = 0")
    Integer getVigilanceInventoryNum(@Param("stock") Integer stock);

    /**
     * 销售中（上架）商品数量
     */
    @Select("SELECT COUNT(*) FROM t_store_product WHERE is_show = 1 AND is_recycle = 0 AND is_del = 0")
    Integer getOnSaleNum();

    /**
     * 未销售（仓库）商品数量
     * 
     * @return Integer
     */
    @Select("SELECT COUNT(*) FROM t_store_product WHERE is_show = 0 AND is_recycle = 0 AND is_del = 0")
    Integer getNotSaleNum();

    List<StoreProductDO> selectStoreProducts(@Param("type") Integer type, @Param("storeStock") Integer storeStock);

    List<StoreProductDO> selectStoreProductsByIds(@Param("productIdList") List<Long> productIdList);

    Integer updateStoreProductStatus(@Param("productId") Long productId, @Param("type") String type);

    Integer operationStock(@Param("id") Integer id, @Param("num") Integer num, @Param("type") String type);

    @Select("SELECT COUNT(*) FROM t_store_product WHERE is_show = 1 AND is_recycle = 0 AND is_del = 0")
    Integer getLeaderboardCount();

    @Select("SELECT id, store_name, image, price, ot_price, (sales + ficti) AS sales FROM t_store_product WHERE is_show = 1 AND is_recycle = 0 AND is_del = 0 ORDER BY sales DESC LIMIT 10")
    List<StoreProductDO> getLeaderboardProducts();

    List<StoreProductDO> selectStoreProductsByQuery(@Param("type") Integer type,
                                                    @Param("storeStock") Integer storeStock,
                                                    @Param("keywords") String keywords,
                                                    @Param("storeName") String storeName,
                                                    @Param("cateId") String cateId);

}