/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import java.io.Serial;
import java.math.BigDecimal;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import top.continew.starter.extension.crud.model.resp.BaseDetailResp;

/**
 * 交易记录详情信息
 *
 * @author joe
 * @since 2024/08/05 22:48
 */
@Data
@ExcelIgnoreUnannotated
@Schema(description = "交易记录详情信息")
public class RecordDetailResp extends BaseDetailResp {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 发起者地址
     */
    @Schema(description = "发起者地址")
    @ExcelProperty(value = "发起者地址")
    private String fromAddress;

    /**
     * 接收者地址
     */
    @Schema(description = "接收者地址")
    @ExcelProperty(value = "接收者地址")
    private String toAddress;

    /**
     * 交易金额
     */
    @Schema(description = "交易金额")
    @ExcelProperty(value = "交易金额")
    private BigDecimal amount;

    /**
     * 交易手续费
     */
    @Schema(description = "交易手续费")
    @ExcelProperty(value = "交易手续费")
    private BigDecimal tradeFee;

    /**
     * 交易状态
     */
    @Schema(description = "交易状态")
    @ExcelProperty(value = "交易状态")
    private String status;

    /**
     * 前置数据
     */
    @Schema(description = "前置数据")
    @ExcelProperty(value = "前置数据")
    private String inputData;

    /**
     * 汇兑时转入币种
     */
    @Schema(description = "汇兑时转入币种")
    @ExcelProperty(value = "汇兑时转入币种")
    private String toCurrency;

    /**
     * 币种
     */
    @Schema(description = "币种")
    @ExcelProperty(value = "币种")
    private String currency;

    /**
     * 随机数
     */
    @Schema(description = "随机数")
    @ExcelProperty(value = "随机数")
    private String nonceStr;

    /**
     * 是否系统数据
     */
    @Schema(description = "是否系统数据")
    @ExcelProperty(value = "是否系统数据")
    private String isSystem;

    /**
     * 是否回调
     */
    @Schema(description = "是否回调")
    @ExcelProperty(value = "是否回调")
    private String isCallback;

    /**
     * 是否删除
     */
    @Schema(description = "是否删除")
    @ExcelProperty(value = "是否删除")
    private String isDelete;

    /**
     * 交易类型
     */
    @Schema(description = "交易类型")
    @ExcelProperty(value = "交易类型")
    private String payType;

    /**
     * 本系统流水号
     */
    @Schema(description = "本系统流水号")
    @ExcelProperty(value = "本系统流水号")
    private String orderNo;

    /**
     * 备注
     */
    @Schema(description = "备注")
    @ExcelProperty(value = "备注")
    private String remark;

    /**
     * 汇兑金额
     */
    @Schema(description = "汇兑金额")
    @ExcelProperty(value = "汇兑金额")
    private BigDecimal toAmount;
}