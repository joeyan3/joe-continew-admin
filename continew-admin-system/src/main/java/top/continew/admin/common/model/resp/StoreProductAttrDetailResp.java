/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import java.io.Serial;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import top.continew.starter.extension.crud.model.resp.BaseDetailResp;

/**
 * 商品属性详情信息
 *
 * @author joe
 * @since 2024/08/17 21:58
 */
@Data
@ExcelIgnoreUnannotated
@Schema(description = "商品属性详情信息")
public class StoreProductAttrDetailResp extends BaseDetailResp {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 商品ID
     */
    @Schema(description = "商品ID")
    @ExcelProperty(value = "商品ID")
    private Long productId;

    /**
     * 属性名
     */
    @Schema(description = "属性名")
    @ExcelProperty(value = "属性名")
    private String attrName;

    /**
     * 属性值
     */
    @Schema(description = "属性值")
    @ExcelProperty(value = "属性值")
    private String attrValues;

    /**
     * 活动类型 0=商品，1=秒杀，2=砍价，3=拼团
     */
    @Schema(description = "活动类型 0=商品，1=秒杀，2=砍价，3=拼团")
    @ExcelProperty(value = "活动类型 0=商品，1=秒杀，2=砍价，3=拼团")
    private Boolean type;

    /**
     * 是否删除,0-否，1-是
     */
    @Schema(description = "是否删除,0-否，1-是")
    @ExcelProperty(value = "是否删除,0-否，1-是")
    private Boolean isDel;
}