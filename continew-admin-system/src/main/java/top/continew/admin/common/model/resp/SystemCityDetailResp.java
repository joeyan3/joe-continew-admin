/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import java.io.Serial;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import top.continew.starter.extension.crud.model.resp.BaseDetailResp;

/**
 * 城市详情信息
 *
 * @author joe
 * @since 2024/08/08 21:46
 */
@Data
@ExcelIgnoreUnannotated
@Schema(description = "城市详情信息")
public class SystemCityDetailResp extends BaseDetailResp {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 城市id
     */
    @Schema(description = "城市id")
    @ExcelProperty(value = "城市id")
    private Long cityId;

    /**
     * 省市级别
     */
    @Schema(description = "省市级别")
    @ExcelProperty(value = "省市级别")
    private Long level;

    /**
     * 父级id
     */
    @Schema(description = "父级id")
    @ExcelProperty(value = "父级id")
    private Long parentId;

    /**
     * 区号
     */
    @Schema(description = "区号")
    @ExcelProperty(value = "区号")
    private String areaCode;

    /**
     * 名称
     */
    @Schema(description = "名称")
    @ExcelProperty(value = "名称")
    private String name;

    /**
     * 合并名称
     */
    @Schema(description = "合并名称")
    @ExcelProperty(value = "合并名称")
    private String mergerName;

    /**
     * 经度
     */
    @Schema(description = "经度")
    @ExcelProperty(value = "经度")
    private String lng;

    /**
     * 纬度
     */
    @Schema(description = "纬度")
    @ExcelProperty(value = "纬度")
    private String lat;

    /**
     * 是否展示
     */
    @Schema(description = "是否展示")
    @ExcelProperty(value = "是否展示")
    private Boolean isShow;
}