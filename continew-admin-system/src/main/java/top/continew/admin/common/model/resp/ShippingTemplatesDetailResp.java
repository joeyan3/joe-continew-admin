/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import java.io.Serial;

import lombok.Data;

import io.swagger.v3.oas.annotations.media.Schema;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import top.continew.starter.extension.crud.model.resp.BaseDetailResp;

/**
 * 运费模板详情信息
 *
 * @author joe
 * @since 2024/08/08 10:04
 */
@Data
@ExcelIgnoreUnannotated
@Schema(description = "运费模板详情信息")
public class ShippingTemplatesDetailResp extends BaseDetailResp {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 模板名称
     */
    @Schema(description = "模板名称")
    @ExcelProperty(value = "模板名称")
    private String name;

    /**
     * 计费方式
     */
    @Schema(description = "计费方式")
    @ExcelProperty(value = "计费方式")
    private String type;

    /**
     * 指定包邮
     */
    @Schema(description = "指定包邮")
    @ExcelProperty(value = "指定包邮")
    private Boolean appoint;

    /**
     * 排序
     */
    @Schema(description = "排序")
    @ExcelProperty(value = "排序")
    private Integer sort;
}