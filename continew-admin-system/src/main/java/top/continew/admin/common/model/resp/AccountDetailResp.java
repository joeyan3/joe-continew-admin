/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.common.model.resp;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import top.continew.starter.extension.crud.model.resp.BaseDetailResp;

import java.io.Serial;
import java.math.BigDecimal;

/**
 * 商户列表详情信息
 *
 * @author joe
 * @since 2024/07/17 12:14
 */
@Data
@ExcelIgnoreUnannotated
@Schema(description = "商户列表详情信息")
public class AccountDetailResp extends BaseDetailResp {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 账户名
     */
    @Schema(description = "账户名")
    @ExcelProperty(value = "账户名")
    private String accountName;

    /**
     * 姓名
     */
    @Schema(description = "姓名")
    @ExcelProperty(value = "姓名")
    private String name;

    /**
     * 昵称
     */
    @Schema(description = "昵称")
    @ExcelProperty(value = "昵称")
    private String nickName;

    /**
     * 头像
     */
    @Schema(description = "头像")
    @ExcelProperty(value = "头像")
    private String headImg;

    /**
     * 冻结
     */
    @Schema(description = "冻结")
    @ExcelProperty(value = "冻结")
    private Boolean isFreeze;

    /**
     * 是否会员
     */
    @Schema(description = "是否会员")
    @ExcelProperty(value = "是否会员")
    private Boolean isActive;

    /**
     * 是否商户
     */
    @Schema(description = "是否商户")
    @ExcelProperty(value = "是否商户")
    private Boolean isShop;

    /**
     * 店铺认证
     */
    @Schema(description = "店铺认证")
    @ExcelProperty(value = "店铺认证")
    private Integer shopStatus;

    /**
     * 店铺码
     */
    @Schema(description = "店铺码")
    @ExcelProperty(value = "店铺码")
    private String shopCode;

    /**
     * 邀请码
     */
    @Schema(description = "邀请码")
    @ExcelProperty(value = "邀请码")
    private String invitationCode;

    /**
     * 邀请人手机号
     */
    @Schema(description = "邀请人手机号")
    @ExcelProperty(value = "邀请人手机号")
    private String parentAccountName;

    /**
     * 余额
     */
    @Schema(description = "余额")
    @ExcelProperty(value = "余额")
    private BigDecimal shopAmount;

    /**
     * 积分
     */
    @Schema(description = "积分")
    @ExcelProperty(value = "积分")
    private BigDecimal scoreAmount;
}