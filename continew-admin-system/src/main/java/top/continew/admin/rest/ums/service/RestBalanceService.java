/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.rest.ums.service;

import top.continew.admin.common.model.entity.BalanceDO;
import top.continew.admin.common.model.query.BalanceQuery;
import top.continew.admin.common.model.req.BalanceReq;
import top.continew.admin.common.model.resp.BalanceDetailResp;
import top.continew.admin.common.model.resp.BalanceResp;
import top.continew.starter.extension.crud.service.BaseService;

/**
 * 余额业务接口
 *
 * @author joe
 * @since 2024/07/21 22:04
 */
public interface RestBalanceService extends BaseService<BalanceResp, BalanceDetailResp, BalanceQuery, BalanceReq> {
    /**
     * 根据accountid获取
     *
     * @param accountId 用户id
     * @param currency  币种
     * @return ID
     */
    BalanceResp getBalanceByAccountId(Long accountId, String currency);

    /**
     * 新增
     *
     * @param balanceDO 用户信息
     * @return ID
     */
    Long add(BalanceDO balanceDO);

    /**
     * 根据accountId创建Balance
     *
     * @param accountId 用户id
     * @return ID
     */
    void createBalanceByAccountId(Long accountId);
}