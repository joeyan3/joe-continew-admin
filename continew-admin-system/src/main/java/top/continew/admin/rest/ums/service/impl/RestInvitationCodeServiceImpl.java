/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.rest.ums.service.impl;

import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import top.continew.admin.rest.ums.service.RestInvitationCodeService;
import top.continew.admin.common.mapper.InvitationCodeMapper;
import top.continew.admin.common.model.entity.InvitationCodeDO;
import top.continew.admin.common.model.query.InvitationCodeQuery;
import top.continew.admin.common.model.req.InvitationCodeReq;
import top.continew.admin.common.model.resp.InvitationCodeDetailResp;
import top.continew.admin.common.model.resp.InvitationCodeResp;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;

/**
 * 邀请码业务实现
 *
 * @author joe
 * @since 2024/07/21 13:24
 */
@Service
@RequiredArgsConstructor
public class RestInvitationCodeServiceImpl extends BaseServiceImpl<InvitationCodeMapper, InvitationCodeDO, InvitationCodeResp, InvitationCodeDetailResp, InvitationCodeQuery, InvitationCodeReq> implements RestInvitationCodeService {
    private final InvitationCodeMapper baseMapper;

    @Override
    public String updateAccountName(Long accountId, String accountName) {
        InvitationCodeDO invitationCodeDO = baseMapper.getByAccountId(accountId);
        if (null != invitationCodeDO) {
            invitationCodeDO.setAccountName(accountName);
            baseMapper.updateById(invitationCodeDO);
            return invitationCodeDO.getInvitationCode();
        }
        return "";
    }

    private String processInvitationCode(String invitationCode) {
        if (StrUtil.isNotBlank(invitationCode)) {
            invitationCode = invitationCode.replace("0", "O").replace("1", "i").trim().toUpperCase();
        } else {
            invitationCode = "000000";
        }
        return invitationCode;
    }

    @Override
    public String validateInvitationCode(String invitationCode) {
        // 校验邀请码
        String processedInvitationCode = processInvitationCode(invitationCode);

        // 判断输入的邀请码是否存在
        InvitationCodeDO exitInvitationCodeDO = baseMapper.getByInvitationCode(processedInvitationCode);
        if (null == exitInvitationCodeDO) {
            return "";
        } else {
            return processedInvitationCode;
        }

    }

}