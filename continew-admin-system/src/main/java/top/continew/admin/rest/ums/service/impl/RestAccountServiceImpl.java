/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.rest.ums.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.continew.admin.common.constant.RegexConstants;
import top.continew.admin.common.mapper.AccountMapper;
import top.continew.admin.common.util.SmsUtils;
import top.continew.admin.rest.ums.service.*;
import top.continew.admin.system.service.OptionService;
import top.continew.admin.common.model.entity.AccountDO;
import top.continew.admin.common.model.query.AccountQuery;
import top.continew.admin.common.model.req.AccountAddReq;
import top.continew.admin.common.model.req.AccountPasswordResetReq;
import top.continew.admin.common.model.req.AccountReq;
import top.continew.admin.common.model.resp.AccountDetailResp;
import top.continew.admin.common.model.resp.AccountResp;
import top.continew.starter.core.util.validate.CheckUtils;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;

import java.lang.reflect.InvocationTargetException;

/**
 * 商户列表业务实现
 *
 * @author joe
 * @since 2024/07/17 12:14
 */
@Service
@RequiredArgsConstructor
public class RestAccountServiceImpl extends BaseServiceImpl<AccountMapper, AccountDO, AccountResp, AccountDetailResp, AccountQuery, AccountReq> implements RestAccountService {
    private final PasswordEncoder passwordEncoder;
    private final OptionService optionService;
    private final RestInvitationCodeService invitationCodeService;
    private final RestInvitationGroupCountService invitationGroupCountService;
    private final RestBalanceService balanceService;
    private final RestRecordService recordService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long registerNewAccount(AccountAddReq accountAddReq) throws InvocationTargetException, IllegalAccessException {
        // 根据账户名获取已有的账户
        AccountDO accountDO = baseMapper.getByAccountName(accountAddReq.getAccountName());

        // 如果账户已存在，抛出异常
        CheckUtils.throwIfNotNull(accountDO, "商户已存在!");

        AccountDO newAccountDO = new AccountDO();
        BeanUtil.copyProperties(accountAddReq, newAccountDO);

        // 将新账户插入数据库
        baseMapper.insert(newAccountDO);
        //创建资金账户
        balanceService.createBalanceByAccountId(newAccountDO.getId());

        recordService.createTrade(newAccountDO.getId().toString());

        // 返回新账户的ID
        return newAccountDO.getId();
    }

    @Override
    public void resetPassword(AccountPasswordResetReq req, Long id) {
        AccountDO account = super.getById(id);
        account.setPassword(req.getNewPassword());
        baseMapper.updateById(account);
    }

    @Override
    public void updatePassword(String oldPassword, String newPassword, Long id) {
        CheckUtils.throwIfEqual(newPassword, oldPassword, "新密码不能与当前密码相同");
        AccountDO account = super.getById(id);
        String password = account.getPassword();
        //        if (StrUtil.isNotBlank(password)) {
        //            CheckUtils.throwIf(!passwordEncoder.matches(oldPassword, password), "当前密码错误");
        //        }
        account.setPassword(newPassword);
        baseMapper.updateById(account);
    }

    @Override
    public void updatePayPassword(String oldPayPassword, String newPayPassword, Long id) {
        CheckUtils.throwIfEqual(newPayPassword, oldPayPassword, "新支付密码不能与当前密码相同");
        AccountDO account = super.getById(id);
        String payPassword = account.getPayPassword();
        if (StrUtil.isNotBlank(payPassword)) {
            CheckUtils.throwIf(!passwordEncoder.matches(oldPayPassword, payPassword), "当前密码错误");
        }
        account.setPayPassword(newPayPassword);
        baseMapper.updateById(account);
    }

    @Override
    public AccountDO getByAccountName(String accountName) {
        return baseMapper.getByAccountName(accountName);
    }

    @Override
    public boolean sendCode(String accountName, String key) {
        // 根据账户名获取已有的账户
        AccountDO accountDO = baseMapper.getByAccountName(accountName);

        // 如果账户不存在，抛出异常
        CheckUtils.throwIfNull(accountDO, "商户不存在!");
        CheckUtils.throwIf(accountDO.getIsFreeze(), "商户已冻结！");
        //        todo: 添加测试环境兼容，以及确定是否要添加对账户名是邮箱时的处理
        if (!ReUtil.isMatch(RegexConstants.PHONE_REGEXP, accountName)) {
            // 发送短信验证码，Redis存储手机号和验证码
            return SmsUtils.sendSms(accountName, key);
        }
        return false;

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean softDelAccount(Long accountId) {
        // 根据账户名获取已有的账户
        AccountDO accountDO = baseMapper.selectById(accountId);
        if (!accountDO.getIsDel()) {
            baseMapper.softDelAccount(accountId);
            String newAccountName = generateUniqueAccountName(accountDO.getAccountName());

            //修改账号为新的账号
            String invitationCode = invitationCodeService.updateAccountName(accountId, newAccountName);
            if (StrUtil.isNotBlank(invitationCode)) {
                // 更新邀请组计数表中的账户名
                invitationGroupCountService.updateAccountName(newAccountName, invitationCode);
            }
            return true;
        }

        return false;

    }

    private String generateUniqueAccountName(String baseName) {
        String accountName = baseName + 'x';
        while (baseMapper.getByAccountName(accountName) != null) {
            accountName += 'x';
        }
        return accountName;
    }

}