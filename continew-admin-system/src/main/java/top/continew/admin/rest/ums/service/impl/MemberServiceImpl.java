/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.rest.ums.service.impl;

import cn.hutool.core.util.ObjectUtil;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import top.continew.admin.common.model.dto.LoginMember;
import top.continew.admin.common.util.helper.LoginMemberHelper;
import top.continew.admin.rest.ums.service.MemberService;
import top.continew.admin.rest.ums.service.RestAccountService;
import top.continew.admin.common.model.entity.AccountDO;
import top.continew.starter.core.util.validate.CheckUtils;

/**
 * 登录业务实现
 *
 * @author Charles7c
 * @since 2022/12/21 21:49
 */
@Service
@RequiredArgsConstructor
public class MemberServiceImpl implements MemberService {
    private final RestAccountService accountService;
    private final PasswordEncoder passwordEncoder;

    @Override
    public String accountLogin(String username, String password, HttpServletRequest request) {
        AccountDO user = accountService.getByAccountName(username);
        boolean isError = ObjectUtil.isNull(user) || !passwordEncoder.matches(password, user.getPassword());
        //        this.checkUserLocked(username, request, isError);
        CheckUtils.throwIf(isError, "用户名或密码错误");
        return this.login(user);
    }

    /**
     * 登录并缓存用户信息
     *
     * @return 令牌
     */
    private String login(AccountDO user) {
        // 权限设置，如果需要的话
        Long userId = user.getId();
        String userName = user.getAccountName();
        LoginMember loginUser = new LoginMember();
        loginUser.setId(userId);
        loginUser.setUsername(userName);

        return LoginMemberHelper.login(loginUser);
    }
}
