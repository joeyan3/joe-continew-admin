/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.rest.ums.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import top.continew.admin.common.enums.CurrenyTypeEnum;
import top.continew.admin.common.mapper.BalanceMapper;
import top.continew.admin.common.model.entity.BalanceDO;
import top.continew.admin.common.model.query.BalanceQuery;
import top.continew.admin.common.model.req.BalanceReq;
import top.continew.admin.common.model.resp.BalanceDetailResp;
import top.continew.admin.common.model.resp.BalanceResp;
import top.continew.admin.rest.ums.service.RestBalanceService;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;

/**
 * 余额业务实现
 *
 * @author joe
 * @since 2024/07/21 22:04
 */
@Service
@RequiredArgsConstructor
public class RestBalanceServiceImpl extends BaseServiceImpl<BalanceMapper, BalanceDO, BalanceResp, BalanceDetailResp, BalanceQuery, BalanceReq> implements RestBalanceService {
    @Override
    public BalanceResp getBalanceByAccountId(Long accountId, String currency) {
        return baseMapper.getBalanceByAccountId(accountId, currency);
    }

    @Override
    public Long add(BalanceDO balanceDO) {
        baseMapper.insert(balanceDO);
        return balanceDO.getId();
    }

    @Override
    public void createBalanceByAccountId(Long accountId) {
        // 为账户初始化每种货币类型的余额
        for (String currencyType : CurrenyTypeEnum.CURRENY_TYPES) {
            BalanceResp balanceDO = getBalanceByAccountId(accountId, currencyType);
            // 如果该货币类型的余额不存在，创建新的余额记录
            if (null == balanceDO) {
                BalanceDO newBalanceDO = new BalanceDO();
                newBalanceDO.setAccountId(accountId);
                newBalanceDO.setCurrency(currencyType);
                add(newBalanceDO);
            }
        }
    }

}