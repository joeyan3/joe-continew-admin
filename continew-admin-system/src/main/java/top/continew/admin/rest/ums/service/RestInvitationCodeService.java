/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.rest.ums.service;

import top.continew.starter.extension.crud.service.BaseService;
import top.continew.admin.common.model.query.InvitationCodeQuery;
import top.continew.admin.common.model.req.InvitationCodeReq;
import top.continew.admin.common.model.resp.InvitationCodeDetailResp;
import top.continew.admin.common.model.resp.InvitationCodeResp;

/**
 * 邀请码业务接口
 *
 * @author joe
 * @since 2024/07/21 13:24
 */
public interface RestInvitationCodeService extends BaseService<InvitationCodeResp, InvitationCodeDetailResp, InvitationCodeQuery, InvitationCodeReq> {
    String updateAccountName(Long accountId, String accountName);

    String validateInvitationCode(String invitationCode);

}