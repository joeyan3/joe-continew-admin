/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.rest.ums.service;

import top.continew.admin.common.model.entity.RecordDO;
import top.continew.starter.extension.crud.service.BaseService;
import top.continew.admin.common.model.query.RecordQuery;
import top.continew.admin.common.model.req.RecordReq;
import top.continew.admin.common.model.resp.RecordDetailResp;
import top.continew.admin.common.model.resp.RecordResp;

/**
 * 交易记录业务接口
 *
 * @author joe
 * @since 2024/08/05 22:48
 */
public interface RestRecordService extends BaseService<RecordResp, RecordDetailResp, RecordQuery, RecordReq> {
    RecordDO createTrade(String fromAddress);

}