/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.rest.ums.service.impl;

import cn.hutool.core.map.MapUtil;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import top.continew.admin.common.enums.CurrenyTypeEnum;
import top.continew.admin.common.enums.PayStatusEnum;
import top.continew.admin.common.enums.PayTypeEnum;
import top.continew.admin.common.enums.SystemTypeEnum;
import top.continew.admin.common.util.SequenceUtil;
import top.continew.admin.rest.ums.service.RestRecordService;
import top.continew.admin.system.service.OptionService;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;
import top.continew.admin.common.mapper.RecordMapper;
import top.continew.admin.common.model.entity.RecordDO;
import top.continew.admin.common.model.query.RecordQuery;
import top.continew.admin.common.model.req.RecordReq;
import top.continew.admin.common.model.resp.RecordDetailResp;
import top.continew.admin.common.model.resp.RecordResp;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 交易记录业务实现
 *
 * @author joe
 * @since 2024/08/05 22:48
 */
@Service
@RequiredArgsConstructor
public class RestRecordServiceImpl extends BaseServiceImpl<RecordMapper, RecordDO, RecordResp, RecordDetailResp, RecordQuery, RecordReq> implements RestRecordService {
    private final OptionService optionService;

    @Override
    public RecordDO createTrade(String toAddress) {
        Map<String, String> map = optionService.getByCategory("APPCONFIG");
        Long registerGiftScore = MapUtil.getLong(map, "APPCONFIG_REGISTER_GIFT_SCORE");
        BigDecimal regGiftScore = BigDecimal.valueOf(0);
        if (null != registerGiftScore) {
            regGiftScore = BigDecimal.valueOf(registerGiftScore);
        }

        RecordDO recordDO = new RecordDO();
        recordDO.setAmount(regGiftScore);
        recordDO.setCurrency(CurrenyTypeEnum.SCORE.getValue());
        recordDO.setFromAddress("0x");
        recordDO.setToAddress(toAddress);
        recordDO.setOrderNo(SequenceUtil.generateTransOrder());
        recordDO.setStatus(PayStatusEnum.SUCCESS.getValue());
        recordDO.setToCurrency("");
        recordDO.setPayType(PayTypeEnum.REGISTER_BONUS.getValue());
        recordDO.setIsSystem(SystemTypeEnum.USER.getValue());
        baseMapper.insert(recordDO);
        return recordDO;
    }
}