/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service;

import org.springframework.transaction.annotation.Transactional;
import top.continew.admin.common.model.entity.StoreProductDO;
import top.continew.admin.common.model.req.StoreProductStockReq;
import top.continew.admin.common.model.resp.StoreProductInfoResp;
import top.continew.admin.common.model.resp.StoreProductTabsHeaderResp;
import top.continew.starter.extension.crud.model.query.PageQuery;
import top.continew.starter.extension.crud.model.resp.PageResp;
import top.continew.starter.extension.crud.service.BaseService;
import top.continew.admin.common.model.query.StoreProductQuery;
import top.continew.admin.common.model.req.StoreProductReq;
import top.continew.admin.common.model.resp.StoreProductDetailResp;
import top.continew.admin.common.model.resp.StoreProductResp;

import java.util.List;

/**
 * 商品业务接口
 *
 * @author joe
 * @since 2024/08/17 17:00
 */
public interface StoreProductService extends BaseService<StoreProductResp, StoreProductDetailResp, StoreProductQuery, StoreProductReq> {
    List<StoreProductResp> getListInIds(List<Long> productIds);

    Boolean save(StoreProductReq request);

    Boolean update(StoreProductReq storeProductRequest);

    StoreProductResp getByProductId(Long id);

    StoreProductInfoResp getInfo(Long id);

    List<StoreProductTabsHeaderResp> getTabsHeader();

    List<Long> getSecondaryCategoryByProductId(String productIdStr);

    Boolean deleteProduct(Long productId, String type);

    Boolean reStoreProduct(Long productId);

    Boolean doProductStock(StoreProductStockReq storeProductStockRequest);

    Boolean operationStock(Integer id, Integer num, String type);

    @Transactional(rollbackFor = Exception.class)
    Boolean offShelf(Long id);

    @Transactional(rollbackFor = Exception.class)
    Boolean putOnShelf(Long id);

    StoreProductDO getCartByProId(Long productId);

    Integer getNewProductByDate(String date);

    List<StoreProductDO> findAllProductByNotDelte();

    List<StoreProductDO> likeProductName(String productName);

    Integer getVigilanceInventoryNum();

    Integer getOnSaleNum();

    Integer getNotSaleNum();

    List<StoreProductDO> getLeaderboard();

    PageResp<StoreProductResp> getAdminList(StoreProductQuery request, PageQuery pageQuery);
}