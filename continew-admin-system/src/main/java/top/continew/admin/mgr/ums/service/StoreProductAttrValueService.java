/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service;

import top.continew.admin.common.model.entity.StoreProductAttrValueDO;
import top.continew.starter.extension.crud.service.BaseService;
import top.continew.admin.common.model.query.StoreProductAttrValueQuery;
import top.continew.admin.common.model.req.StoreProductAttrValueReq;
import top.continew.admin.common.model.resp.StoreProductAttrValueDetailResp;
import top.continew.admin.common.model.resp.StoreProductAttrValueResp;

import java.util.List;

/**
 * 商品属性值业务接口
 *
 * @author joe
 * @since 2024/08/17 22:19
 */
public interface StoreProductAttrValueService extends BaseService<StoreProductAttrValueResp, StoreProductAttrValueDetailResp, StoreProductAttrValueQuery, StoreProductAttrValueReq> {
    List<StoreProductAttrValueDO> getListByProductIdAndAttrId(Long productId, Long attrId, Integer type);

    List<StoreProductAttrValueResp> getByEntity(StoreProductAttrValueQuery query);

    void removeByProductId(Long productId, Integer type);

    StoreProductAttrValueResp getByIdAndProductIdAndType(Long id, Long productId, Integer type);

    StoreProductAttrValueResp getByProductIdAndSkuAndType(Long productId, String suk, Integer type);

    void operationStock(Long id, Integer num, String operationType, Integer type);

    void deleteByProductIdAndType(Long productId, Integer type);

    List<StoreProductAttrValueResp> getListByProductIdAndType(Long productId, Integer type);

    void saveBatch(List<StoreProductAttrValueDO> attrValueList);

    void update(StoreProductAttrValueDO storeProductAttrValueDO);

    void updateBatchById(List<StoreProductAttrValueDO> attrList);
}