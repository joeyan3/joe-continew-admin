/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import cn.hutool.core.collection.CollUtil;
import lombok.RequiredArgsConstructor;

import net.dreamlu.mica.core.utils.BeanUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

import top.continew.admin.common.util.CommonUtil;
import top.continew.admin.mgr.ums.service.ShippingTemplatesFreeService;
import top.continew.admin.mgr.ums.service.SystemCityService;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;
import top.continew.admin.common.mapper.ShippingTemplatesFreeMapper;
import top.continew.admin.common.model.entity.ShippingTemplatesFreeDO;
import top.continew.admin.common.model.query.ShippingTemplatesFreeQuery;
import top.continew.admin.common.model.req.ShippingTemplatesFreeReq;
import top.continew.admin.common.model.resp.ShippingTemplatesFreeDetailResp;
import top.continew.admin.common.model.resp.ShippingTemplatesFreeResp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 运费模板包邮业务实现
 *
 * @author joe
 * @since 2024/08/08 10:40
 */
@Service
@RequiredArgsConstructor
public class ShippingTemplatesFreeServiceImpl extends BaseServiceImpl<ShippingTemplatesFreeMapper, ShippingTemplatesFreeDO, ShippingTemplatesFreeResp, ShippingTemplatesFreeDetailResp, ShippingTemplatesFreeQuery, ShippingTemplatesFreeReq> implements ShippingTemplatesFreeService {

    private List<Long> cityIdList;
    private SystemCityService systemCityService;

    @Override
    public void saveAll(List<ShippingTemplatesFreeReq> shippingTemplatesFreeRequestList, Integer type, Long tempId) {
        ArrayList<ShippingTemplatesFreeDO> shippingTemplatesFreeList = new ArrayList<>();

        List<ShippingTemplatesFreeResp> existShippingTemplatesFreeList = baseMapper.getBytempId(tempId);
        if (CollUtil.isNotEmpty(existShippingTemplatesFreeList)) {
            //把目前模板下的所有数据标记为无效
            baseMapper.updateStatus(tempId);
        }

        if (CollUtil.isNotEmpty(shippingTemplatesFreeRequestList)) {
            for (ShippingTemplatesFreeReq shippingTemplatesFreeRequest : shippingTemplatesFreeRequestList) {
                String uniqueKey = DigestUtils.md5Hex(shippingTemplatesFreeRequest.toString());

                if ("all".equals(shippingTemplatesFreeRequest.getCityId()) || "0".equals(shippingTemplatesFreeRequest
                    .getCityId())) {
                    cityIdList = systemCityService.getCityIdList();
                } else {
                    cityIdList = CommonUtil.stringToArrayLong(shippingTemplatesFreeRequest.getCityId());
                }
                for (Long cityId : cityIdList) {
                    ShippingTemplatesFreeDO shippingTemplatesFree = new ShippingTemplatesFreeDO();
                    BeanUtil.copyProperties(shippingTemplatesFreeRequest, shippingTemplatesFree);

                    shippingTemplatesFree.setCityId(cityId);
                    shippingTemplatesFree.setUniqid(uniqueKey);
                    shippingTemplatesFree.setTempId(tempId);
                    shippingTemplatesFree.setType(type);
                    shippingTemplatesFree.setStatus(true);

                    shippingTemplatesFreeList.add(shippingTemplatesFree);
                }
            }
            //批量保存模板数据
            baseMapper.insertBatch(shippingTemplatesFreeList);
        }

        if (CollUtil.isNotEmpty(existShippingTemplatesFreeList)) {
            //删除模板下的无效数据
            baseMapper.delete(tempId);
        }
    }

    /**
     * 删除模板下的无效数据
     * 
     * @param tempId Long 运费模板id
     */
    @Override
    public Boolean delete(Long tempId) {
        baseMapper.delete(tempId);
        return true;
    }

    /**
     * 根据多个id查询
     *
     * @param idList
     */
    @Override
    public List<ShippingTemplatesFreeResp> getListByIds(Collection<? extends Serializable> idList) {
        return baseMapper.getListByIds(idList);
    }

    /**
     * 根据模板编号、城市ID查询
     */
    @Override
    public ShippingTemplatesFreeResp getByTempIdAndCityId(Long tempId, Long cityId) {
        return baseMapper.getByTempIdAndCityId(tempId, cityId);
    }

    /**
     * 分组查询
     */
    @Override
    public List<ShippingTemplatesFreeResp> getListGroup(Long tempId) {
        return baseMapper.getListGroup(tempId);
    }

}