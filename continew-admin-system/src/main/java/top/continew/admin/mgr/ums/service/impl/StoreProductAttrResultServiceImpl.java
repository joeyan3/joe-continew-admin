/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import top.continew.admin.mgr.ums.service.StoreProductAttrResultService;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;
import top.continew.admin.common.mapper.StoreProductAttrResultMapper;
import top.continew.admin.common.model.entity.StoreProductAttrResultDO;
import top.continew.admin.common.model.query.StoreProductAttrResultQuery;
import top.continew.admin.common.model.req.StoreProductAttrResultReq;
import top.continew.admin.common.model.resp.StoreProductAttrResultDetailResp;
import top.continew.admin.common.model.resp.StoreProductAttrResultResp;

import java.util.List;

/**
 * 商品属性详情业务实现
 *
 * @author joe
 * @since 2024/08/17 22:07
 */
@Service
@RequiredArgsConstructor
public class StoreProductAttrResultServiceImpl extends BaseServiceImpl<StoreProductAttrResultMapper, StoreProductAttrResultDO, StoreProductAttrResultResp, StoreProductAttrResultDetailResp, StoreProductAttrResultQuery, StoreProductAttrResultReq> implements StoreProductAttrResultService {
    /**
     * 根据商品属性值集合查询
     *
     * @param query 查询参数
     * @return 查询结果
     */
    @Override
    public List<StoreProductAttrResultResp> getByEntity(StoreProductAttrResultQuery query) {
        QueryWrapper<StoreProductAttrResultDO> queryWrapper = this.buildQueryWrapper(query);
        return baseMapper.getByEntity(queryWrapper);
    }

}