/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import lombok.RequiredArgsConstructor;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import top.continew.admin.common.constant.StoreConstants;
import top.continew.admin.common.model.vo.CategoryTreeVo;
import top.continew.admin.common.util.CommonUtil;
import top.continew.starter.core.util.validate.CheckUtils;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;
import top.continew.admin.common.mapper.CategoryMapper;
import top.continew.admin.common.model.entity.CategoryDO;
import top.continew.admin.common.model.query.CategoryQuery;
import top.continew.admin.common.model.req.CategoryReq;
import top.continew.admin.common.model.resp.CategoryDetailResp;
import top.continew.admin.common.model.resp.CategoryResp;
import top.continew.admin.mgr.ums.service.CategoryService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 分类业务实现
 *
 * @author joe
 * @since 2024/08/14 13:03
 */
@Service
@RequiredArgsConstructor
public class CategoryServiceImpl extends BaseServiceImpl<CategoryMapper, CategoryDO, CategoryResp, CategoryDetailResp, CategoryQuery, CategoryReq> implements CategoryService {

    /**
     * 通过id集合获取列表 id => name
     *
     * @param cateIdList List<Integer> id集合
     * @return HashMap<Integer, String>
     * @author Mr.Zhang
     * @since 2020-04-16
     */
    @Override
    public HashMap<Long, String> getListInId(List<Long> cateIdList) {
        HashMap<Long, String> map = new HashMap<>();
        List<CategoryDO> list = baseMapper.getByIds(cateIdList);
        for (CategoryDO category : list) {
            map.put(category.getId(), category.getName());
        }

        return map;
    }

    /**
     * 通过id集合获取列表
     *
     * @param cateIdList List<Integer> id集合
     */
    @Override
    public List<CategoryDO> getByIds(List<Long> cateIdList) {
        return baseMapper.getByIds(cateIdList);
    }

    /**
     * 查询id和url是否存在
     *
     * @return Boolean
     */
    public Boolean checkAuth(List<Long> pathIdList, String uri) {
        List<CategoryDO> categoryList = baseMapper.selectByIdsAndUrl(pathIdList, uri);
        return !categoryList.isEmpty();
    }

    /**
     * 修改
     *
     * @param request CategoryRequest
     * @param id      Integer
     * @return bool
     * @author Mr.Zhang
     * @since 2020-04-16
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateCategory(CategoryReq request, Long id) {

        //修改分类信息
        CategoryDO category = new CategoryDO();
        BeanUtils.copyProperties(request, category);
        category.setId(id);
        category.setPath(getPathByPId(category.getPid()));

        updateById(category);

        //如状态为关闭，那么所以子集的状态都关闭
        if (!request.getStatus()) {
            baseMapper.updateStatusByPid(id, false);
        } else {
            //如是开启，则父类的状态为开启
            updatePidStatusById(id);
        }

    }

    private String getPathByPId(Long pid) {
        CategoryDO category = getById(pid, false);
        if (null != category) {
            return category.getPath() + pid + "/";
        }
        return null;
    }

    /**
     * 开启父级状态
     *
     * @param id Integer
     * @author Mr.Zhang
     * @since 2020-04-16
     */
    private void updatePidStatusById(Long id) {
        CategoryDO category = getById(id);
        List<Long> categoryIdList = CommonUtil.stringToArrayLongByRegex(category.getPath(), "/");
        categoryIdList.removeIf(i -> i.equals(0));
        ArrayList<CategoryDO> categoryArrayList = new ArrayList<>();
        if (categoryIdList.isEmpty()) {
            return;
        }
        for (Long categoryId : categoryIdList) {
            CategoryDO categoryDo = new CategoryDO();
            categoryDo.setId(Long.valueOf(categoryId));
            categoryDo.setStatus(true);
            categoryArrayList.add(categoryDo);
        }
        updateBatchById(categoryArrayList);
    }

    public List<CategoryTreeVo> getTree(Integer type, Integer status, String name, List<Integer> categoryIdList) {
        // 查询所有符合条件的分类
        List<CategoryDO> allTree = baseMapper.selectCategoryTree(type, status, name, categoryIdList);
        if (allTree == null || allTree.isEmpty()) {
            return new ArrayList<>();
        }

        // 处理名称搜索的情况，补充父级分类
        if (StrUtil.isNotBlank(name)) {
            List<Long> categoryIds = allTree.stream().map(CategoryDO::getId).collect(Collectors.toList());

            List<Long> pidList = allTree.stream()
                .filter(c -> c.getPid() > 0 && !categoryIds.contains(c.getPid()))
                .map(CategoryDO::getPid)
                .distinct()
                .collect(Collectors.toList());

            for (Long pid : pidList) {
                CategoryDO parentCategory = baseMapper.selectById(pid);
                if (parentCategory != null && !categoryIds.contains(parentCategory.getId())) {
                    allTree.add(parentCategory);
                }
            }
        }

        // 构建树形结构
        List<CategoryTreeVo> treeList = allTree.stream().map(category -> {
            CategoryTreeVo categoryTreeVo = new CategoryTreeVo();
            BeanUtils.copyProperties(category, categoryTreeVo);
            return categoryTreeVo;
        }).collect(Collectors.toList());

        // 将CategoryTreeVo对象以ID为键存储到Map中
        Map<Long, CategoryTreeVo> map = treeList.stream()
            .collect(Collectors.toMap(CategoryTreeVo::getId, categoryTreeVo -> categoryTreeVo));

        // 构建树结构
        List<CategoryTreeVo> list = new ArrayList<>();
        for (CategoryTreeVo tree : treeList) {
            CategoryTreeVo parent = map.get(tree.getPid());
            if (parent != null) {
                parent.getChildren().add(tree);
            } else {
                list.add(tree);
            }
        }

        // 返回树形结构
        return list;
    }

    private String getPathByPId(Integer pid) {
        CategoryDO category = getById(pid);
        if (null != category) {
            return category.getPath() + pid + "/";
        }
        return null;
    }

    /**
     * 带结构的无线级分类
     */
    @Override
    public List<CategoryTreeVo> getListTree(Integer type, Integer status, String name) {
        return getTree(type, status, name, null);
    }

    /**
     * 带权限的属性结构
     */
    @Override
    public List<CategoryTreeVo> getListTree(Integer type, Integer status, List<Integer> categoryIdList) {
        System.out.println("菜单列表:getListTree: type:" + type + "| status:" + status + "| categoryIdList:" + JSON
            .toJSONString(categoryIdList));
        return getTree(type, status, null, categoryIdList);
    }

    /**
     * 删除分类表
     *
     * @param id Integer
     * @return bool
     * @author Mr.Zhang
     * @since 2020-04-16
     */
    @Override
    public int delete(Long id) {
        //查看是否有子类, 物理删除
        CheckUtils.throwIf(baseMapper.getChildCountByPid(id) > 0, "当前分类下有子类，请先删除子类！");

        return baseMapper.deleteById(id);
    }

    /**
     * 检测url是否存在
     *
     * @param uri String url
     * @return boolean
     */
    @Override
    public boolean checkUrl(String uri) {
        return baseMapper.checkUrl(uri) > 0;
    }

    @Override
    public boolean updateStatus(Long id) {
        CategoryDO category = getById(id);
        category.setStatus(!category.getStatus());
        return updateById(category);
    }

    /**
     * 新增分类
     *
     * @param categoryRequest
     */
    @Override
    public Boolean create(CategoryReq categoryRequest) {
        //检测标题是否存在
        CheckUtils.throwIf(baseMapper.checkName(categoryRequest.getName(), categoryRequest.getType()) > 0, "此分类已存在！");

        CategoryDO category = new CategoryDO();
        BeanUtils.copyProperties(categoryRequest, category);
        category.setPath(getPathByPId(category.getPid()));
        //        todo 添加systemAttachmentService
        //        category.setExtra(systemAttachmentService.clearPrefix(category.getExtra()));
        return save(category);
    }

    /**
     * 获取文章分类列表
     *
     * @return List<Category>
     */
    @Override
    public List<CategoryDO> findArticleCategoryList() {
        return baseMapper.findArticleCategoryList(StoreConstants.CATEGORY_TYPE_ARTICLE);
    }

}