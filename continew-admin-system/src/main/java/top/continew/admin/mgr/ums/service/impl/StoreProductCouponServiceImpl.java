/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import top.continew.admin.mgr.ums.service.StoreProductCouponService;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;
import top.continew.admin.common.mapper.StoreProductCouponMapper;
import top.continew.admin.common.model.entity.StoreProductCouponDO;
import top.continew.admin.common.model.query.StoreProductCouponQuery;
import top.continew.admin.common.model.req.StoreProductCouponReq;
import top.continew.admin.common.model.resp.StoreProductCouponDetailResp;
import top.continew.admin.common.model.resp.StoreProductCouponResp;

import java.util.List;

/**
 * 商品优惠券业务实现
 *
 * @author joe
 * @since 2024/08/20 14:33
 */
@Service
@RequiredArgsConstructor
public class StoreProductCouponServiceImpl extends BaseServiceImpl<StoreProductCouponMapper, StoreProductCouponDO, StoreProductCouponResp, StoreProductCouponDetailResp, StoreProductCouponQuery, StoreProductCouponReq> implements StoreProductCouponService {
    /**
     *
     * @param productId 产品id
     */
    @Override
    public void deleteByProductId(Long productId) {
        baseMapper.deleteByProductId(productId);
    }

    /**
     * 根据商品id获取已关联优惠券信息
     * 
     * @param productId 商品id
     * @return 已关联优惠券
     */
    @Override
    public List<StoreProductCouponResp> getListByProductId(Long productId) {
        return baseMapper.getListByProductId(productId);
    }

    /**
     * 批量保存
     */
    @Override
    public void saveBatch(List<StoreProductCouponDO> storeProductCouponList) {
        baseMapper.insertBatch(storeProductCouponList);
    }
}