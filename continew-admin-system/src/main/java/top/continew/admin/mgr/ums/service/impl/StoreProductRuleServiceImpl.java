/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import top.continew.admin.mgr.ums.service.StoreProductRuleService;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;
import top.continew.admin.common.mapper.StoreProductRuleMapper;
import top.continew.admin.common.model.entity.StoreProductRuleDO;
import top.continew.admin.common.model.query.StoreProductRuleQuery;
import top.continew.admin.common.model.req.StoreProductRuleReq;
import top.continew.admin.common.model.resp.StoreProductRuleDetailResp;
import top.continew.admin.common.model.resp.StoreProductRuleResp;

import java.util.List;

/**
 * 商品规则值(规格)业务实现
 *
 * @author joe
 * @since 2024/09/09 08:38
 */
@Service
@RequiredArgsConstructor
public class StoreProductRuleServiceImpl extends BaseServiceImpl<StoreProductRuleMapper, StoreProductRuleDO, StoreProductRuleResp, StoreProductRuleDetailResp, StoreProductRuleQuery, StoreProductRuleReq> implements StoreProductRuleService {
    /**
     * 获取商品规则值(规格)
     *
     * @return StoreProductRuleDO
     */
    @Override
    public List<StoreProductRuleDO> getAll() {
        List<StoreProductRuleDO> templates = baseMapper.getAll();
        return templates;
    }
}