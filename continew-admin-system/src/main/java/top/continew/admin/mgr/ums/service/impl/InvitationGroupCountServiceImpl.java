/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import top.continew.admin.mgr.ums.service.InvitationGroupCountService;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;
import top.continew.admin.common.mapper.InvitationGroupCountMapper;
import top.continew.admin.common.model.entity.InvitationGroupCountDO;
import top.continew.admin.common.model.query.InvitationGroupCountQuery;
import top.continew.admin.common.model.req.InvitationGroupCountReq;
import top.continew.admin.common.model.resp.InvitationGroupCountDetailResp;
import top.continew.admin.common.model.resp.InvitationGroupCountResp;

/**
 * 邀请组业务实现
 *
 * @author joe
 * @since 2024/07/21 20:25
 */
@Service
@RequiredArgsConstructor
public class InvitationGroupCountServiceImpl extends BaseServiceImpl<InvitationGroupCountMapper, InvitationGroupCountDO, InvitationGroupCountResp, InvitationGroupCountDetailResp, InvitationGroupCountQuery, InvitationGroupCountReq> implements InvitationGroupCountService {

    @Override
    public void initAndSveInvitationGroupCount(String invitationCode, String accountName) {
        InvitationGroupCountDO invitationGroupCountDO = new InvitationGroupCountDO();
        invitationGroupCountDO.setInvitationCode(invitationCode);
        invitationGroupCountDO.setAccountName(accountName);
        baseMapper.insert(invitationGroupCountDO);
    }
}