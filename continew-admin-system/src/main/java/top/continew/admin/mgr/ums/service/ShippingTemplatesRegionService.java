/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service;

import top.continew.starter.extension.crud.service.BaseService;
import top.continew.admin.common.model.query.ShippingTemplatesRegionQuery;
import top.continew.admin.common.model.req.ShippingTemplatesRegionReq;
import top.continew.admin.common.model.resp.ShippingTemplatesRegionDetailResp;
import top.continew.admin.common.model.resp.ShippingTemplatesRegionResp;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * 运费模板指定区域费用业务接口
 *
 * @author joe
 * @since 2024/08/08 12:14
 */
public interface ShippingTemplatesRegionService extends BaseService<ShippingTemplatesRegionResp, ShippingTemplatesRegionDetailResp, ShippingTemplatesRegionQuery, ShippingTemplatesRegionReq> {
    void saveAll(List<ShippingTemplatesRegionReq> shippingTemplatesRegionRequestList, Integer type, Long id);

    Boolean delete(Long tempId);

    List<ShippingTemplatesRegionResp> getListByIds(Collection<? extends Serializable> idList);

    ShippingTemplatesRegionResp getByTempIdAndCityId(Long tempId, Long cityId);

    List<ShippingTemplatesRegionResp> getListGroup(Long tempId);
}