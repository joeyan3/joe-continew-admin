/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import lombok.RequiredArgsConstructor;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import top.continew.admin.common.constant.StoreConstants;
import top.continew.admin.common.model.entity.*;
import top.continew.admin.common.model.query.StoreProductAttrQuery;
import top.continew.admin.common.model.query.StoreProductAttrResultQuery;
import top.continew.admin.common.model.query.StoreProductAttrValueQuery;
import top.continew.admin.common.model.req.StoreProductAttrReq;
import top.continew.admin.common.model.req.StoreProductAttrValueReq;
import top.continew.admin.common.model.req.StoreProductStockReq;
import top.continew.admin.common.model.resp.*;
import top.continew.admin.common.util.CommonUtil;
import top.continew.admin.common.util.DateUtils;
import top.continew.admin.mgr.ums.service.*;
import top.continew.admin.system.service.OptionService;
import top.continew.starter.core.util.validate.CheckUtils;
import top.continew.starter.extension.crud.model.query.PageQuery;
import top.continew.starter.extension.crud.model.resp.PageResp;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;
import top.continew.admin.common.mapper.StoreProductMapper;
import top.continew.admin.common.model.query.StoreProductQuery;
import top.continew.admin.common.model.req.StoreProductReq;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.transaction.support.TransactionTemplate;

/**
 * 商品业务实现
 *
 * @author joe
 * @since 2024/08/17 17:00
 */
@Service
@RequiredArgsConstructor
public class StoreProductServiceImpl extends BaseServiceImpl<StoreProductMapper, StoreProductDO, StoreProductResp, StoreProductDetailResp, StoreProductQuery, StoreProductReq> implements StoreProductService {
    private final TransactionTemplate transactionTemplate;
    private final StoreProductAttrValueService storeProductAttrValueService;
    private final StoreProductAttrResultService storeProductAttrResultService;
    private final StoreProductDescriptionService storeProductDescriptionService;
    private final StoreProductAttrService attrService;
    private final StoreProductCouponService storeProductCouponService;
    private final OptionService optionService;
    private final CategoryService categoryService;
    private final StoreProductRelationService storeProductRelationService;
    private final StoreCouponService storeCouponService;

    /**
     * 根据商品id集合获取
     *
     * @param productIds id集合
     * @return
     */
    @Override
    public List<StoreProductResp> getListInIds(List<Long> productIds) {
        return baseMapper.getListInIds(productIds);
    }

    /**
     * 新增产品
     *
     * @param request 新增产品request对象
     * @return 新增结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean save(StoreProductReq request) {
        // 验证规格类型是否合法
        validateSpecType(request);

        // 初始化商品信息
        StoreProductDO storeProduct = initializeStoreProduct(request);

        // 处理商品图片信息
        processImages(storeProduct);

        // 设置商品价格和库存
        setProductPricesAndStock(storeProduct, request.getAttrValue());

        // 设置商品的默认值
        setDefaultValues(storeProduct, request);

        // 映射商品属性信息
        List<StoreProductAttrDO> attrList = mapAttributes(request.getAttr());

        // 映射商品属性值信息
        List<StoreProductAttrValueDO> attrValueList = mapAttrValues(request.getAttrValue());

        // 创建商品描述信息
        StoreProductDescriptionDO spd = createProductDescription(request.getContent());

        // 保存商品主表信息
        save(storeProduct);

        // 保存商品属性信息
        saveProductAttributes(attrList, storeProduct.getId());

        // 保存商品属性值信息
        saveProductAttrValues(attrValueList, storeProduct.getId());

        // 保存商品描述信息
        saveProductDescription(spd, storeProduct.getId());

        // 保存商品关联的优惠券信息
        saveProductCoupons(request.getCouponIds(), storeProduct.getId());

        return Boolean.TRUE; // 返回保存结果
    }

    private void validateSpecType(StoreProductReq request) {
        // 验证商品规格类型
        // 如果商品不是多规格商品，验证属性值的数量不能大于1
        if (!request.getSpecType()) {
            CheckUtils.throwIf((request.getAttrValue().size() > 1), "单规格商品属性值不能大于1");
        }
    }

    private StoreProductDO initializeStoreProduct(StoreProductReq request) {
        // 初始化商品实体对象
        StoreProductDO storeProduct = new StoreProductDO();
        // 复制请求对象的属性到商品对象
        BeanUtils.copyProperties(request, storeProduct);
        // 清除商品ID，防止覆盖已有记录
        storeProduct.setId(null);
        // 设置添加时间
        storeProduct.setAddTime(DateUtils.getNowTime());
        // 默认商品不显示
        storeProduct.setIsShow(false);
        // 设置商品活动信息
        storeProduct.setActivity(getProductActivityStr(request.getActivity()));
        return storeProduct;
    }

    private void processImages(StoreProductDO storeProduct) {
        // 处理商品图片
        // 这里是占位符，需根据具体业务实现图片处理逻辑
        storeProduct.setImage(storeProduct.getImage());
        storeProduct.setSliderImage(storeProduct.getSliderImage());
        if (StrUtil.isNotEmpty(storeProduct.getFlatPattern())) {
            storeProduct.setFlatPattern(storeProduct.getFlatPattern());
        }
    }

    private void setProductPricesAndStock(StoreProductDO storeProduct,
                                          List<StoreProductAttrValueReq> attrValueAddRequestList) {
        // 设置商品价格和库存
        // 从属性值中找到价格最小的项
        StoreProductAttrValueReq minAttrValue = attrValueAddRequestList.stream()
            .min(Comparator.comparing(StoreProductAttrValueReq::getPrice))
            .orElseThrow(() -> new IllegalArgumentException("未找到属性值"));

        // 设置商品价格和其他相关价格
        storeProduct.setPrice(minAttrValue.getPrice());
        storeProduct.setOtPrice(minAttrValue.getOtPrice());
        storeProduct.setCost(minAttrValue.getCost());
        // 计算并设置商品库存为所有属性值库存的总和
        storeProduct.setStock(attrValueAddRequestList.stream().mapToInt(StoreProductAttrValueReq::getStock).sum());
    }

    private void setDefaultValues(StoreProductDO storeProduct, StoreProductReq request) {
        // 设置商品的默认值
        storeProduct.setSort(ObjectUtil.defaultIfNull(request.getSort(), 0));
        storeProduct.setIsHot(ObjectUtil.defaultIfNull(request.getIsHot(), false));
        storeProduct.setIsBenefit(ObjectUtil.defaultIfNull(request.getIsBenefit(), false));
        storeProduct.setIsBest(ObjectUtil.defaultIfNull(request.getIsBest(), false));
        storeProduct.setIsNew(ObjectUtil.defaultIfNull(request.getIsNew(), false));
        storeProduct.setIsGood(ObjectUtil.defaultIfNull(request.getIsGood(), false));
        storeProduct.setGiveIntegral(ObjectUtil.defaultIfNull(request.getGiveIntegral(), 0));
        storeProduct.setFicti(ObjectUtil.defaultIfNull(request.getFicti(), 0));
    }

    private List<StoreProductAttrDO> mapAttributes(List<StoreProductAttrReq> attrReqList) {
        // 将请求中的商品属性映射为实体对象列表
        return attrReqList.stream().map(attrReq -> {
            StoreProductAttrDO attr = new StoreProductAttrDO();
            // 复制请求中的属性数据到实体对象
            BeanUtils.copyProperties(attrReq, attr);
            // 设置属性类型为普通商品类型
            attr.setType(StoreConstants.PRODUCT_TYPE_NORMAL);
            return attr;
        }).collect(Collectors.toList());
    }

    private List<StoreProductAttrValueDO> mapAttrValues(List<StoreProductAttrValueReq> attrValueReqList) {
        // 将请求中的商品属性值映射为实体对象列表
        return attrValueReqList.stream().map(attrValueReq -> {
            StoreProductAttrValueDO attrValue = new StoreProductAttrValueDO();
            // 复制请求中的属性值数据到实体对象
            BeanUtils.copyProperties(attrValueReq, attrValue);
            // 清除ID，防止覆盖已有记录
            attrValue.setId(null);
            // 生成并设置SKU
//            attrValue.setSuk(getSku(attrValueReq.getAttrValue()));
            // 设置配额和显示配额为0
            attrValue.setQuota(0);
            attrValue.setQuotaShow(0);
            // 设置属性值类型为普通商品类型
            attrValue.setType(StoreConstants.PRODUCT_TYPE_NORMAL);
            // 设置属性值的图片
            attrValue.setImage(attrValueReq.getImage());
            return attrValue;
        }).collect(Collectors.toList());
    }

    private StoreProductDescriptionDO createProductDescription(String content) {
        // 创建商品描述对象
        StoreProductDescriptionDO spd = new StoreProductDescriptionDO();
        // 设置描述内容，如果内容为空则设置为空字符串
        spd.setDescription(StrUtil.isNotEmpty(content) ? content : "");
        // 设置描述类型为普通商品类型
        spd.setType(StoreConstants.PRODUCT_TYPE_NORMAL);
        return spd;
    }

    private void saveProductAttributes(List<StoreProductAttrDO> attrList, Long productId) {
        // 保存商品属性，并设置关联的商品ID
        attrList.forEach(attr -> attr.setProductId(productId));
        attrService.saveBatch(attrList);
    }

    private void saveProductAttrValues(List<StoreProductAttrValueDO> attrValueList, Long productId) {
        // 保存商品属性值，并设置关联的商品ID
        attrValueList.forEach(value -> value.setProductId(productId));
        storeProductAttrValueService.saveBatch(attrValueList);
    }

    private void saveProductDescription(StoreProductDescriptionDO spd, Long productId) {
        // 保存商品描述，并设置关联的商品ID
        spd.setProductId(productId);
        // 删除旧的描述记录
        storeProductDescriptionService.deleteByProductId(productId, StoreConstants.PRODUCT_TYPE_NORMAL);
        // 保存新的描述
        storeProductDescriptionService.saveSingle(spd);
    }

    private void saveProductCoupons(List<Long> couponIds, Long productId) {
        // 保存商品关联的优惠券信息
        if (CollUtil.isNotEmpty(couponIds)) {
            List<StoreProductCouponDO> couponList = couponIds.stream().map(couponId -> {
                StoreProductCouponDO spc = new StoreProductCouponDO();
                // 设置商品ID
                spc.setId(productId);
                // 设置优惠券ID
                spc.setIssueCouponId(couponId);
                // 设置添加时间
                spc.setAddTime(DateUtils.getNowTime());
                return spc;
            }).collect(Collectors.toList());
            // 批量保存优惠券
            storeProductCouponService.saveBatch(couponList);
        }
    }

    /**
     * 商品sku
     *
     * @param attrValue json字符串
     * @return sku
     */
    private String getSku(String attrValue) {
        LinkedHashMap linkedHashMap = JSONObject.parseObject(attrValue, LinkedHashMap.class, Feature.OrderedField);
        Iterator<Map.Entry<String, String>> iterator = linkedHashMap.entrySet().iterator();
        List<String> strings = CollUtil.newArrayList();
        while (iterator.hasNext()) {
            Map.Entry<String, String> next = iterator.next();
            strings.add(next.getValue());
        }
        //        List<String> strings = jsonObject.values().stream().map(o -> (String) o).collect(Collectors.toList());
        return String.join(",", strings);
    }

    /**
     * 商品活动字符串
     *
     * @param activityList 活动数组
     * @return 商品活动字符串
     */
    private String getProductActivityStr(List<String> activityList) {
        if (CollUtil.isEmpty(activityList)) {
            return "0, 1, 2, 3";
        }
        List<Integer> activities = new ArrayList<>();
        activityList.forEach(e -> {
            switch (e) {
                case StoreConstants.PRODUCT_TYPE_NORMAL_STR:
                    activities.add(StoreConstants.PRODUCT_TYPE_NORMAL);
                    break;
                case StoreConstants.PRODUCT_TYPE_SECKILL_STR:
                    activities.add(StoreConstants.PRODUCT_TYPE_SECKILL);
                    break;
                case StoreConstants.PRODUCT_TYPE_BARGAIN_STR:
                    activities.add(StoreConstants.PRODUCT_TYPE_BARGAIN);
                    break;
                case StoreConstants.PRODUCT_TYPE_PINGTUAN_STR:
                    activities.add(StoreConstants.PRODUCT_TYPE_PINGTUAN);
                    break;
            }
        });
        return activities.stream().map(Object::toString).collect(Collectors.joining(","));
    }

    /**
     * 商品活动字符列表
     *
     * @param activityStr 商品活动字符串
     * @return 商品活动字符列表
     */
    private List<String> getProductActivityList(String activityStr) {
        List<String> activityList = CollUtil.newArrayList();
        if ("0, 1, 2, 3".equals(activityStr)) {
            activityList.add(StoreConstants.PRODUCT_TYPE_NORMAL_STR);
            activityList.add(StoreConstants.PRODUCT_TYPE_SECKILL_STR);
            activityList.add(StoreConstants.PRODUCT_TYPE_BARGAIN_STR);
            activityList.add(StoreConstants.PRODUCT_TYPE_PINGTUAN_STR);
            return activityList;
        }
        String[] split = activityStr.split(",");
        for (String s : split) {
            Integer integer = Integer.valueOf(s);
            if (integer.equals(StoreConstants.PRODUCT_TYPE_NORMAL)) {
                activityList.add(StoreConstants.PRODUCT_TYPE_NORMAL_STR);
            }
            if (integer.equals(StoreConstants.PRODUCT_TYPE_SECKILL)) {
                activityList.add(StoreConstants.PRODUCT_TYPE_SECKILL_STR);
            }
            if (integer.equals(StoreConstants.PRODUCT_TYPE_BARGAIN)) {
                activityList.add(StoreConstants.PRODUCT_TYPE_BARGAIN_STR);
            }
            if (integer.equals(StoreConstants.PRODUCT_TYPE_PINGTUAN)) {
                activityList.add(StoreConstants.PRODUCT_TYPE_PINGTUAN_STR);
            }
        }
        return activityList;
    }

    /**
     * 更新商品信息
     *
     * @param storeProductRequest 商品参数
     * @return 更新结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean update(StoreProductReq storeProductRequest) {
        validateProductRequest(storeProductRequest); // 校验商品请求

        StoreProductDO existingProduct = getById(storeProductRequest.getId());
        validateExistingProduct(existingProduct); // 校验现有商品信息

        StoreProductDO storeProduct = buildStoreProduct(storeProductRequest); // 构建商品信息对象

        List<StoreProductAttrDO> attrToAddList = new ArrayList<>();
        List<StoreProductAttrDO> attrToUpdateList = new ArrayList<>();
        prepareProductAttributes(storeProductRequest, storeProduct, attrToAddList, attrToUpdateList); // 准备商品属性

        List<StoreProductAttrValueDO> attrValueToAddList = new ArrayList<>();
        List<StoreProductAttrValueDO> attrValueToUpdateList = new ArrayList<>();
        prepareProductAttrValues(storeProductRequest, storeProduct, attrValueToAddList, attrValueToUpdateList); // 准备商品属性值

        StoreProductDescriptionDO description = buildProductDescription(storeProductRequest, storeProduct); // 构建商品描述

        baseMapper.updateById(storeProduct); // 更新商品基本信息
        // 更新商品属性和属性值
        updateProductAttributes(storeProduct, attrToAddList, attrValueToAddList);

        // 更新商品描述
        updateProductDescription(storeProduct, description);

        // 更新商品优惠券
        updateProductCoupons(storeProductRequest.getCouponIds(), storeProduct.getId());

        return Boolean.TRUE;
    }

    /**
     * 校验商品请求参数
     *
     * @param storeProductRequest 商品请求参数
     */
    private void validateProductRequest(StoreProductReq storeProductRequest) {
        CheckUtils.throwIf(ObjectUtil.isNull(storeProductRequest.getId()), "商品ID不能为空");

        if (!storeProductRequest.getSpecType()) {
            CheckUtils.throwIf((storeProductRequest.getAttrValue().size() > 1), "单规格商品属性值不能大于1");
        }
    }

    /**
     * 校验现有商品信息
     *
     * @param existingProduct 现有商品对象
     */
    private void validateExistingProduct(StoreProductDO existingProduct) {
        CheckUtils.throwIfNull(existingProduct, "商品不存在");
        CheckUtils.throwIf((existingProduct.getIsRecycle() || existingProduct.getIsDel()), "商品不存在");
        CheckUtils.throwIf(existingProduct.getIsShow(), "请先下架商品，再进行修改");
    }

    /**
     * 构建商品信息对象
     *
     * @param storeProductRequest 商品请求参数
     * @return 构建后的商品对象
     */
    private StoreProductDO buildStoreProduct(StoreProductReq storeProductRequest) {
        StoreProductDO storeProduct = new StoreProductDO();
        BeanUtils.copyProperties(storeProductRequest, storeProduct);
        storeProduct.setActivity(getProductActivityStr(storeProductRequest.getActivity())); // 设置活动信息
        storeProduct.setImage(storeProduct.getImage()); // 设置主图
        storeProduct.setSliderImage(storeProduct.getSliderImage()); // 设置轮播图

        // 计算价格与库存
        StoreProductAttrValueReq minAttrValue = storeProductRequest.getAttrValue()
            .stream()
            .min(Comparator.comparing(StoreProductAttrValueReq::getPrice))
            .orElseThrow(() -> new IllegalArgumentException("商品属性值不能为空"));
        storeProduct.setPrice(minAttrValue.getPrice());
        storeProduct.setOtPrice(minAttrValue.getOtPrice());
        storeProduct.setCost(minAttrValue.getCost());
        storeProduct.setStock(storeProductRequest.getAttrValue()
            .stream()
            .mapToInt(StoreProductAttrValueReq::getStock)
            .sum());

        return storeProduct;
    }

    /**
     * 准备商品属性
     *
     * @param storeProductRequest 商品请求参数
     * @param storeProduct        商品对象
     * @param attrToAddList       需要新增的商品属性列表
     * @param attrToUpdateList    需要更新的商品属性列表
     */
    private void prepareProductAttributes(StoreProductReq storeProductRequest,
                                          StoreProductDO storeProduct,
                                          List<StoreProductAttrDO> attrToAddList,
                                          List<StoreProductAttrDO> attrToUpdateList) {
        storeProductRequest.getAttr().forEach(attrReq -> {
            StoreProductAttrDO attr = new StoreProductAttrDO();
            BeanUtils.copyProperties(attrReq, attr);
            if (ObjectUtil.isNull(attr.getId())) {
                attr.setProductId(storeProduct.getId());
                attr.setType(StoreConstants.PRODUCT_TYPE_NORMAL);
                attrToAddList.add(attr);
            } else {
                attr.setIsDel(false);
                attrToUpdateList.add(attr);
            }
        });
    }

    /**
     * 准备商品属性值
     *
     * @param storeProductRequest   商品请求参数
     * @param storeProduct          商品对象
     * @param attrValueToAddList    需要新增的商品属性值列表
     * @param attrValueToUpdateList 需要更新的商品属性值列表
     */
    private void prepareProductAttrValues(StoreProductReq storeProductRequest,
                                          StoreProductDO storeProduct,
                                          List<StoreProductAttrValueDO> attrValueToAddList,
                                          List<StoreProductAttrValueDO> attrValueToUpdateList) {
        storeProductRequest.getAttrValue().forEach(attrValueReq -> {
            StoreProductAttrValueDO attrValue = new StoreProductAttrValueDO();
            BeanUtils.copyProperties(attrValueReq, attrValue);
            attrValue.setSuk(getSku(attrValueReq.getAttrValue()));
            attrValue.setImage(attrValueReq.getImage());
            if (ObjectUtil.isNull(attrValue.getId()) || attrValue.getId().equals(0)) {
                attrValue.setId(null);
                attrValue.setProductId(storeProduct.getId());
                attrValue.setQuota(0);
                attrValue.setQuotaShow(0);
                attrValue.setType(StoreConstants.PRODUCT_TYPE_NORMAL);
                attrValueToAddList.add(attrValue);
            } else {
                attrValue.setIsDel(false);
                attrValueToUpdateList.add(attrValue);
            }
        });
    }

    /**
     * 构建商品描述对象
     *
     * @param storeProductRequest 商品请求参数
     * @param storeProduct        商品对象
     * @return 构建后的商品描述对象
     */
    private StoreProductDescriptionDO buildProductDescription(StoreProductReq storeProductRequest,
                                                              StoreProductDO storeProduct) {
        StoreProductDescriptionDO description = new StoreProductDescriptionDO();
        description.setDescription(StrUtil.isNotEmpty(storeProductRequest.getContent())
            ? storeProductRequest.getContent()
            : "");
        description.setType(StoreConstants.PRODUCT_TYPE_NORMAL);
        description.setProductId(storeProduct.getId());
        return description;
    }

    /**
     * 更新商品属性和属性值
     *
     * @param storeProduct       商品对象
     * @param attrToAddList      需要新增的商品属性列表
     * @param attrValueToAddList 需要新增的商品属性值列表
     */
    private void updateProductAttributes(StoreProductDO storeProduct,
                                         List<StoreProductAttrDO> attrToAddList,
                                         List<StoreProductAttrValueDO> attrValueToAddList) {
        attrService.deleteByProductIdAndType(storeProduct.getId(), StoreConstants.PRODUCT_TYPE_NORMAL);
        storeProductAttrValueService.deleteByProductIdAndType(storeProduct.getId(), StoreConstants.PRODUCT_TYPE_NORMAL);

        if (CollUtil.isNotEmpty(attrToAddList)) {
            attrService.saveBatch(attrToAddList);
        }
        if (CollUtil.isNotEmpty(attrValueToAddList)) {
            storeProductAttrValueService.saveBatch(attrValueToAddList);
        }
    }

    /**
     * 更新商品描述
     *
     * @param storeProduct 商品对象
     * @param description  商品描述对象
     */
    private void updateProductDescription(StoreProductDO storeProduct, StoreProductDescriptionDO description) {
        storeProductDescriptionService.deleteByProductId(storeProduct.getId(), StoreConstants.PRODUCT_TYPE_NORMAL);
        storeProductDescriptionService.saveSingle(description);
    }

    /**
     * 更新商品优惠券
     *
     * @param couponIds 优惠券ID列表
     * @param productId 商品ID
     */
    private void updateProductCoupons(List<Long> couponIds, Long productId) {

        if (CollUtil.isNotEmpty(couponIds)) {
            storeProductCouponService.deleteByProductId(productId);

            List<StoreProductCouponDO> couponList = couponIds.stream().map(couponId -> {
                StoreProductCouponDO coupon = new StoreProductCouponDO();
                coupon.setProductId(productId);
                coupon.setIssueCouponId(couponId);
                coupon.setAddTime(DateUtils.getNowTime());
                return coupon;
            }).collect(Collectors.toList());
            storeProductCouponService.saveBatch(couponList);
        }
    }

    /**
     * 获取商品详情
     *
     * @param id 商品ID
     * @return 商品详情响应对象
     */
    @Override
    public StoreProductResp getByProductId(Long id) {
        // 获取商品基础信息
        StoreProductDO storeProduct = baseMapper.selectById(id);
        CheckUtils.throwIfNull(storeProduct, "未找到对应商品信息");

        // 构建商品响应对象
        StoreProductResp storeProductResponse = new StoreProductResp();
        BeanUtils.copyProperties(storeProduct, storeProductResponse);

        // 设置商品属性信息
        setProductAttributes(storeProduct, storeProductResponse);

        // 设置商品描述信息
        setProductDescription(storeProduct, storeProductResponse);

        // 设置商品优惠券信息
        setProductCoupons(storeProduct, storeProductResponse);

        return storeProductResponse;
    }

    /**
     * 设置商品属性信息
     *
     * @param storeProduct         商品对象
     * @param storeProductResponse 商品响应对象
     */
    private void setProductAttributes(StoreProductDO storeProduct, StoreProductResp storeProductResponse) {
        StoreProductAttrQuery spaPram = new StoreProductAttrQuery();
        spaPram.setProductId(storeProduct.getId());
        spaPram.setType(StoreConstants.PRODUCT_TYPE_NORMAL);

        // 获取商品属性列表
        storeProductResponse.setAttr(attrService.getByEntity(spaPram));

        StoreProductAttrValueQuery spavPram = new StoreProductAttrValueQuery();
        spavPram.setProductId(storeProduct.getId());
        spavPram.setType(StoreConstants.PRODUCT_TYPE_NORMAL);

        // 获取商品属性值列表
        List<StoreProductAttrValueResp> storeProductAttrValues = storeProductAttrValueService.getByEntity(spavPram);
        List<HashMap<String, Object>> attrValues = new ArrayList<>();

        if (storeProduct.getSpecType()) {
            setMultiSpecAttributes(storeProduct, storeProductAttrValues, attrValues);
        }

        // H5端用于生成skuList
        //        storeProductResponse.setAttrValues(attrValues);
        //        List<StoreProductAttrValueResp> sPAVResponses = storeProductAttrValues.stream()
        //                .map(spav -> {
        //                    StoreProductAttrValueResp atr = new StoreProductAttrValueResp();
        //                    BeanUtils.copyProperties(spav, atr);
        //                    return atr;
        //                }).collect(Collectors.toList());
        //        storeProductResponse.setAttrValue(sPAVResponses);
    }

    /**
     * 设置多规格商品的属性信息
     *
     * @param storeProduct           商品对象
     * @param storeProductAttrValues 商品属性值列表
     * @param attrValues             最终生成的多规格属性信息列表
     */
    private void setMultiSpecAttributes(StoreProductDO storeProduct,
                                        List<StoreProductAttrValueResp> storeProductAttrValues,
                                        List<HashMap<String, Object>> attrValues) {
        StoreProductAttrResultQuery sparPram = new StoreProductAttrResultQuery();
        sparPram.setProductId(storeProduct.getId());
        sparPram.setType(StoreConstants.PRODUCT_TYPE_NORMAL);

        // 获取商品属性结果列表
        List<StoreProductAttrResultResp> attrResults = storeProductAttrResultService.getByEntity(sparPram);
        CheckUtils.throwIf(attrResults == null || attrResults.isEmpty(), "未找到对应属性值");

        StoreProductAttrResultResp attrResult = attrResults.get(0);
        List<StoreProductAttrValueReq> storeProductAttrValueRequests = com.alibaba.fastjson.JSONObject
            .parseArray(attrResult.getResult(), StoreProductAttrValueReq.class);

        if (storeProductAttrValueRequests != null) {
            for (int i = 0; i < storeProductAttrValueRequests.size(); i++) {
                HashMap<String, Object> attrValue = new HashMap<>();
                String currentSku = storeProductAttrValues.get(i).getSuk();

                // 查找与当前SKU匹配的属性值
                StoreProductAttrValueResp currentAttrValue = storeProductAttrValues.stream()
                    .filter(e -> e.getSuk().equals(currentSku))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("未找到对应的SKU属性值"));

                // 填充属性信息
                fillAttrValue(attrValue, currentAttrValue, currentSku);

                attrValues.add(attrValue);
            }
        }
    }

    /**
     * 填充属性信息
     *
     * @param attrValue        属性信息Map
     * @param currentAttrValue 当前SKU的属性值
     * @param currentSku       当前SKU
     */
    private void fillAttrValue(HashMap<String, Object> attrValue,
                               StoreProductAttrValueResp currentAttrValue,
                               String currentSku) {
        attrValue.put("id", currentAttrValue.getId());
        attrValue.put("image", currentAttrValue.getImage());
        attrValue.put("cost", currentAttrValue.getCost());
        attrValue.put("price", currentAttrValue.getPrice());
        attrValue.put("otPrice", currentAttrValue.getOtPrice());
        attrValue.put("stock", currentAttrValue.getStock());
        attrValue.put("barCode", currentAttrValue.getBarCode());
        attrValue.put("weight", currentAttrValue.getWeight());
        attrValue.put("volume", currentAttrValue.getVolume());
        attrValue.put("suk", currentSku);
        attrValue.put("brokerage", currentAttrValue.getBrokerage());
        attrValue.put("brokerageTwo", currentAttrValue.getBrokerageTwo());

        // 分割SKU字符串以填充每个值
        String[] skus = currentSku.split(",");
        for (int k = 0; k < skus.length; k++) {
            attrValue.put("value" + k, skus[k]);
        }
    }

    /**
     * 设置商品描述信息
     *
     * @param storeProduct         商品对象
     * @param storeProductResponse 商品响应对象
     */
    private void setProductDescription(StoreProductDO storeProduct, StoreProductResp storeProductResponse) {
        StoreProductDescriptionResp sd = storeProductDescriptionService.getByProductIdAndType(storeProduct
            .getId(), StoreConstants.PRODUCT_TYPE_NORMAL);

        if (sd != null) {
            storeProductResponse.setContent(sd.getDescription() != null ? sd.getDescription() : "");
        }
    }

    /**
     * 设置商品优惠券信息
     *
     * @param storeProduct         商品对象
     * @param storeProductResponse 商品响应对象
     */
    private void setProductCoupons(StoreProductDO storeProduct, StoreProductResp storeProductResponse) {
        List<StoreProductCouponResp> storeProductCoupons = storeProductCouponService.getListByProductId(storeProduct
            .getId());

        if (null != storeProductCoupons && !storeProductCoupons.isEmpty()) {
            List<Long> couponIds = storeProductCoupons.stream()
                .map(StoreProductCouponResp::getIssueCouponId)
                .collect(Collectors.toList());
            List<StoreCouponResp> shipCoupons = storeCouponService.getByIds(couponIds);
            storeProductResponse.setCoupons(shipCoupons);
            storeProductResponse.setCouponIds(couponIds);
        }
    }

    /**
     * 商品详情（管理端）
     *
     * @param id 商品id
     * @return StoreProductInfoResponse
     */
    @Override
    public StoreProductInfoResp getInfo(Long id) {
        // 获取商品信息
        StoreProductDO storeProduct = baseMapper.selectById(id);
        // 检查商品是否存在
        CheckUtils.throwIfNull(storeProduct, "未找到对应商品信息");

        // 创建返回对象并复制商品基础信息
        StoreProductInfoResp storeProductResponse = new StoreProductInfoResp();
        BeanUtils.copyProperties(storeProduct, storeProductResponse);

        // 设置商品参与的活动信息
        List<String> activityList = getProductActivityList(storeProduct.getActivity());
        storeProductResponse.setActivity(activityList);

        // 获取商品的属性信息
        List<StoreProductAttrResp> attrList = attrService.getListByProductIdAndType(storeProduct
            .getId(), StoreConstants.PRODUCT_TYPE_NORMAL);
        storeProductResponse.setAttr(attrList);

        // 获取商品的属性值信息
        List<StoreProductAttrValueResp> attrValueList = storeProductAttrValueService
            .getListByProductIdAndType(storeProduct.getId(), StoreConstants.PRODUCT_TYPE_NORMAL);
        List<AttrValueResp> valueResponseList = attrValueList.stream().map(e -> {
            AttrValueResp valueResponse = new AttrValueResp();
            BeanUtils.copyProperties(e, valueResponse);
            return valueResponse;
        }).collect(Collectors.toList());
        storeProductResponse.setAttrValue(valueResponseList);

        // 获取商品描述信息
        StoreProductDescriptionResp sd = storeProductDescriptionService.getByProductIdAndType(storeProduct
            .getId(), StoreConstants.PRODUCT_TYPE_NORMAL);
        if (ObjectUtil.isNotNull(sd)) {
            // 设置商品描述，如果描述为空则设置为空字符串
            storeProductResponse.setContent(ObjectUtil.defaultIfNull(sd.getDescription(), ""));
        }

        // 获取已关联的优惠券信息
        List<StoreProductCouponResp> storeProductCoupons = storeProductCouponService.getListByProductId(storeProduct
            .getId());
        if (CollUtil.isNotEmpty(storeProductCoupons)) {
            // 提取优惠券ID列表并设置到响应对象中
            List<Long> ids = storeProductCoupons.stream()
                .map(StoreProductCouponResp::getIssueCouponId)
                .collect(Collectors.toList());
            storeProductResponse.setCouponIds(ids);
        }

        return storeProductResponse;
    }

    /**
     * 根据商品tabs获取对应类型的产品数量
     *
     * @return List
     */
    @Override
    public List<StoreProductTabsHeaderResp> getTabsHeader() {
        List<StoreProductTabsHeaderResp> headers = new ArrayList<>();
        StoreProductTabsHeaderResp header1 = new StoreProductTabsHeaderResp(0, "出售中商品", 1);
        StoreProductTabsHeaderResp header2 = new StoreProductTabsHeaderResp(0, "仓库中商品", 2);
        StoreProductTabsHeaderResp header3 = new StoreProductTabsHeaderResp(0, "已经售馨商品", 3);
        StoreProductTabsHeaderResp header4 = new StoreProductTabsHeaderResp(0, "警戒库存", 4);
        StoreProductTabsHeaderResp header5 = new StoreProductTabsHeaderResp(0, "商品回收站", 5);
        headers.add(header1);
        headers.add(header2);
        headers.add(header3);
        headers.add(header4);
        headers.add(header5);
        for (StoreProductTabsHeaderResp h : headers) {

            Map<String, String> map = optionService.getByCategory("STORE");
            Integer storeStock = MapUtil.getInt(map, "STORE_STOCK");

            List<StoreProductDO> storeProducts = baseMapper.selectStoreProducts(h.getType(), storeStock);

            h.setCount(storeProducts.size());
        }

        return headers;
    }

    /**
     * 根据商品id取出二级分类
     *
     * @param productIdStr String 商品分类
     * @return List<Integer>
     */
    @Override
    public List<Long> getSecondaryCategoryByProductId(String productIdStr) {
        // 如果输入字符串为空或仅包含空白字符，返回空列表
        if (StringUtils.isBlank(productIdStr)) {
            return Collections.emptyList();
        }

        // 将产品ID字符串转换为Long类型的列表
        List<Long> productIdList = CommonUtil.stringToArrayLong(productIdStr);
        if (productIdList.isEmpty()) {
            return Collections.emptyList();
        }

        // 根据产品ID列表查询产品信息
        List<StoreProductDO> productList = baseMapper.selectStoreProductsByIds(productIdList);
        if (productList.isEmpty()) {
            return Collections.emptyList();
        }

        // 收集所有产品对应的分类ID，并去重
        Set<Long> categoryIdSet = productList.stream()
            .map(StoreProductDO::getCateId)
            .filter(StringUtils::isNotBlank)
            .flatMap(cateIdStr -> CommonUtil.stringToArrayLong(cateIdStr).stream())
            .collect(Collectors.toSet());

        if (categoryIdSet.isEmpty()) {
            return Collections.emptyList();
        }

        // 初始化结果集合，包含所有原始的分类ID
        Set<Long> resultSet = new HashSet<>(categoryIdSet);

        // 根据分类ID列表查询分类信息
        List<CategoryDO> categoryList = categoryService.getByIds(new ArrayList<>(categoryIdSet));
        if (categoryList.isEmpty()) {
            return new ArrayList<>(resultSet);
        }

        // 提取所有分类的二级分类ID，并添加到结果集合中
        categoryList.forEach(category -> {
            String path = category.getPath();
            if (StringUtils.isNotBlank(path)) {
                List<Long> parentIdList = CommonUtil.stringToArrayLongByRegex(path, "/");
                if (parentIdList.size() > 2) {
                    Long secondaryCategoryId = parentIdList.get(2);
                    if (secondaryCategoryId > 0) {
                        resultSet.add(secondaryCategoryId);
                    }
                }
            }
        });

        return new ArrayList<>(resultSet);
    }

    /**
     * @param productId 商品id
     * @param type      类型：recycle——回收站 delete——彻底删除
     * @return Boolean
     */
    @Override
    public Boolean deleteProduct(Long productId, String type) {
        StoreProductDO product = baseMapper.selectById(productId);
        CheckUtils.throwIfNull(product, "商品不存在");
        CheckUtils.throwIf((StrUtil.isNotBlank(type) && "recycle".equals(type) && product.getIsDel()), "商品已存在回收站");

        return baseMapper.updateStoreProductStatus(productId, type) > 0;
    }

    /**
     * 恢复已删除的商品
     *
     * @param productId 商品id
     * @return 恢复结果
     */
    @Override
    public Boolean reStoreProduct(Long productId) {
        return baseMapper.reStoreProduct(productId) > 0;
    }

    /**
     * 扣减库存任务操作
     *
     * @param storeProductStockRequest 扣减库存参数
     * @return 执行结果
     */
    @Override
    public Boolean doProductStock(StoreProductStockReq storeProductStockRequest) {
        // 获取商品本身信息
        StoreProductDO existProduct = baseMapper.selectById(storeProductStockRequest.getProductId());
        if (existProduct == null) {
            // 如果未找到商品
            return true;
        }

        // 获取商品属性信息
        List<StoreProductAttrValueDO> existAttr = storeProductAttrValueService
            .getListByProductIdAndAttrId(storeProductStockRequest.getProductId(), storeProductStockRequest
                .getAttrId(), storeProductStockRequest.getType());

        if (existAttr == null || existAttr.isEmpty()) {
            return true;
        }

        // 确定操作类型（增加或减少库存）
        boolean isPlus = "add".equals(storeProductStockRequest.getOperationType());
        int delta = storeProductStockRequest.getNum();
        int adjustedStock = isPlus ? existProduct.getStock() + delta : existProduct.getStock() - delta;
        int adjustedSales = existProduct.getSales() - delta;

        // 更新商品库存和销量
        existProduct.setStock(adjustedStock);
        existProduct.setSales(adjustedSales);
        updateById(existProduct);

        // 更新商品属性的库存和销量
        existAttr.forEach(attrValue -> {
            int updatedAttrStock = isPlus ? attrValue.getStock() + delta : attrValue.getStock() - delta;
            int updatedAttrSales = attrValue.getSales() - delta;
            attrValue.setStock(updatedAttrStock);
            attrValue.setSales(updatedAttrSales);
        });

        // 批量更新属性数据
        storeProductAttrValueService.updateBatchById(existAttr);

        return true;

    }

    /**
     * 添加/扣减库存
     *
     * @param id   商品id
     * @param num  数量
     * @param type 类型：add—添加，sub—扣减
     */
    @Override
    public Boolean operationStock(Integer id, Integer num, String type) {
        int updateCount = baseMapper.operationStock(id, num, type);
        CheckUtils.throwIf((updateCount <= 0), "更新普通商品库存失败,商品id = " + id);

        return updateCount > 0;
    }

    /**
     * 下架
     *
     * @param id 商品id
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean offShelf(Long id) {
        StoreProductDO storeProduct = baseMapper.selectById(id);
        CheckUtils.throwIfNull(storeProduct, "商品不存在");

        if (!storeProduct.getIsShow()) {
            return true;
        }

        storeProduct.setIsShow(false);
        baseMapper.updateById(storeProduct);
        //        todo：更新购物车
        //        storeCartService.productStatusNotEnable(id);
        // 商品下架时，清除用户收藏
        storeProductRelationService.deleteByProId(storeProduct.getId());
        return Boolean.TRUE;
    }

    /**
     * 上架
     *
     * @param id 商品id
     * @return Boolean
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean putOnShelf(Long id) {
        StoreProductDO storeProduct = baseMapper.selectById(id);
        CheckUtils.throwIfNull(storeProduct, "商品不存在");
        if (storeProduct.getIsShow()) {
            return true;
        }

        // 获取商品skuid
        StoreProductAttrValueQuery tempSku = new StoreProductAttrValueQuery();
        tempSku.setProductId(id);
        tempSku.setType(StoreConstants.PRODUCT_TYPE_NORMAL);
        List<StoreProductAttrValueResp> skuList = storeProductAttrValueService.getByEntity(tempSku);
        List<Long> skuIdList = skuList.stream().map(StoreProductAttrValueResp::getId).collect(Collectors.toList());

        storeProduct.setIsShow(true);
        baseMapper.updateById(storeProduct);
        //        todo: 增加购物车
        //        storeCartService.productStatusNoEnable(skuIdList);
        return Boolean.TRUE;
    }

    /**
     * 获取购物车商品信息
     *
     * @param productId 商品编号
     */
    @Override
    public StoreProductDO getCartByProId(Long productId) {
        return baseMapper.getCartByProId(productId);
    }

    /**
     * 根据日期获取新增商品数量
     *
     * @param date 日期，yyyy-MM-dd格式
     * @return Integer
     */
    @Override
    public Integer getNewProductByDate(String date) {
        return baseMapper.getNewProductByDate(date);
    }

    /**
     * 获取所有未删除的商品
     *
     * @return List<StoreProductDO>
     */
    @Override
    public List<StoreProductDO> findAllProductByNotDelte() {
        return baseMapper.findAllProductByNotDelte();
    }

    /**
     * 模糊搜索商品名称
     *
     * @param productName 商品名称
     * @return List
     */
    @Override
    public List<StoreProductDO> likeProductName(String productName) {
        return baseMapper.likeProductName(productName);
    }

    /**
     * 警戒库存数量
     *
     * @return Integer
     */
    @Override
    public Integer getVigilanceInventoryNum() {
        Map<String, String> map = optionService.getByCategory("STORE");
        Integer storeStock = MapUtil.getInt(map, "STORE_STOCK");
        return baseMapper.getVigilanceInventoryNum(storeStock);
    }

    /**
     * 销售中（上架）商品数量
     *
     * @return Integer
     */
    @Override
    public Integer getOnSaleNum() {
        return baseMapper.getOnSaleNum();
    }

    /**
     * 未销售（仓库）商品数量
     *
     * @return Integer
     */
    @Override
    public Integer getNotSaleNum() {
        return baseMapper.getNotSaleNum();
    }

    /**
     * 获取商品排行榜
     * 1. 3个商品以内不返回数据
     * 2. TOP10
     *
     * @return List
     */
    @Override
    public List<StoreProductDO> getLeaderboard() {
        Integer count = baseMapper.getLeaderboardCount();
        if (count < 4) {
            return CollUtil.newArrayList();
        }
        return baseMapper.getLeaderboardProducts();
    }

    /**
     * 获取产品列表Admin
     *
     * @param query 筛选参数
     * @return PageInfo
     */
    @Override
    public PageResp<StoreProductResp> getAdminList(StoreProductQuery query, PageQuery pageQuery) {
        // 获取配置中的库存警戒线
        Integer storeStock = MapUtil.getInt(optionService.getByCategory("STORE"), "STORE_STOCK");

        // 查询产品列表
        List<StoreProductDO> storeProducts = baseMapper.selectStoreProductsByQuery(query.getType(), storeStock, query
            .getKeywords(), query.getStoreName(), query.getCateId());

        // 构建响应列表
        List<StoreProductResp> storeProductResponses = storeProducts.stream().map(product -> {
            StoreProductResp storeProductResponse = new StoreProductResp();
            BeanUtils.copyProperties(product, storeProductResponse);

            StoreProductAttrQuery storeProductAttrPram = new StoreProductAttrQuery();
            storeProductAttrPram.setProductId(product.getId());
            storeProductAttrPram.setType(StoreConstants.PRODUCT_TYPE_NORMAL);
            // 设置产品属性
            List<StoreProductAttrResp> attrs = attrService.getByEntity(storeProductAttrPram);
            if (!attrs.isEmpty()) {
                storeProductResponse.setAttr(attrs);
            }

            StoreProductAttrValueQuery storeProductAttrValuePram = new StoreProductAttrValueQuery();
            storeProductAttrValuePram.setProductId(product.getId());
            storeProductAttrValuePram.setType(StoreConstants.PRODUCT_TYPE_NORMAL);
            // 设置属性值
            List<StoreProductAttrValueResp> storeProductAttrValueResponse = storeProductAttrValueService
                .getByEntity(storeProductAttrValuePram)
                .stream()
                .map(attrValue -> {
                    StoreProductAttrValueResp response = new StoreProductAttrValueResp();
                    BeanUtils.copyProperties(attrValue, response);
                    return response;
                })
                .collect(Collectors.toList());
            storeProductResponse.setAttrValue(storeProductAttrValueResponse);

            // 处理富文本
            StoreProductDescriptionResp description = storeProductDescriptionService.getByProductIdAndType(product
                .getId(), StoreConstants.PRODUCT_TYPE_NORMAL);
            storeProductResponse.setContent(description != null ? description.getDescription() : "");

            // 处理分类中文名称
            List<CategoryDO> categories = categoryService.getByIds(CommonUtil.stringToArrayLong(product.getCateId()));
            storeProductResponse.setCateValues(categories.isEmpty()
                ? ""
                : categories.stream().map(CategoryDO::getName).collect(Collectors.joining(",")));

            // 处理收藏数
            int collectCount = storeProductRelationService.getList(product.getId(), "collect").size();
            storeProductResponse.setCollectCount(collectCount);

            return storeProductResponse;
        }).collect(Collectors.toList());

        // 构建分页响应对象
        PageResp<StoreProductResp> pageResp = PageResp.build(pageQuery.getPage(), pageQuery
            .getSize(), storeProductResponses);

        // 填充额外信息
        pageResp.getList().forEach(this::fill);

        return pageResp;
    }

}