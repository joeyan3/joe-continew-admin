/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import top.continew.admin.mgr.ums.service.StoreProductRelationService;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;
import top.continew.admin.common.mapper.StoreProductRelationMapper;
import top.continew.admin.common.model.entity.StoreProductRelationDO;
import top.continew.admin.common.model.query.StoreProductRelationQuery;
import top.continew.admin.common.model.req.StoreProductRelationReq;
import top.continew.admin.common.model.resp.StoreProductRelationDetailResp;
import top.continew.admin.common.model.resp.StoreProductRelationResp;

import java.util.List;

/**
 * 商品点赞和收藏业务实现
 *
 * @author joe
 * @since 2024/08/18 15:12
 */
@Service
@RequiredArgsConstructor
public class StoreProductRelationServiceImpl extends BaseServiceImpl<StoreProductRelationMapper, StoreProductRelationDO, StoreProductRelationResp, StoreProductRelationDetailResp, StoreProductRelationQuery, StoreProductRelationReq> implements StoreProductRelationService {
    //    /**
    //     * 添加收藏产品
    //     * @param request UserCollectAllRequest 新增参数
    //     * @return boolean
    //     */

    //    需要把uid带过来
    //    @Override
    //    public Boolean all(UserCollectAllRequest request) {
    //        Integer[] arr = request.getProductId();
    //        if(arr.length < 1){
    //            throw new CrmebException("请选择产品");
    //        }
    //
    //        List<Integer> list = CrmebUtil.arrayUnique(arr);
    //
    //        Integer uid = userService.getUserIdException();
    //        deleteAll(request, uid, "collect");  //先删除所有已存在的
    //
    //        ArrayList<StoreProductRelation> storeProductRelationList = new ArrayList<>();
    //        for (Integer productId: list) {
    //            StoreProductRelation storeProductRelation = new StoreProductRelation();
    //            storeProductRelation.setUid(uid);
    //            storeProductRelation.setType("collect");
    //            storeProductRelation.setProductId(productId);
    //            storeProductRelation.setCategory(request.getCategory());
    //            storeProductRelationList.add(storeProductRelation);
    //        }
    //        return saveBatch(storeProductRelationList);
    //    }

    //    /**
    //     * 取消收藏产品
    //     */
    //    跟用户关联 需要uid
    //    @Override
    //    public Boolean delete(String requestJson) {
    //        JSONObject jsonObject = JSONObject.parseObject(requestJson);
    //        if (StrUtil.isBlank(jsonObject.getString("ids"))) {
    //            throw new CrmebException("收藏id不能为空");
    //        }
    //        List<Integer> idList = CrmebUtil.stringToArray(jsonObject.getString("ids"));
    //        if (CollUtil.isEmpty(idList)) {
    //            throw new CrmebException("收藏id不能为空");
    //        }
    //        Integer userId = userService.getUserIdException();
    //        LambdaQueryWrapper<StoreProductRelation> lqw = Wrappers.lambdaQuery();
    //        lqw.in(StoreProductRelation::getId, idList);
    //        lqw.eq(StoreProductRelation::getUid, userId);
    //        int delete = dao.delete(lqw);
    //        return delete > 0;
    //    }

    //    /**
    //     * 取消收藏产品
    //     * @param request UserCollectAllRequest 参数
    //     * @param type 类型
    //     */
    //      跟用户关联
    //    private void deleteAll(UserCollectAllRequest request, Integer uid, String type) {
    //        LambdaQueryWrapper<StoreProductRelation> lambdaQueryWrapper = new LambdaQueryWrapper<>();
    //        lambdaQueryWrapper.in(StoreProductRelation::getProductId, Arrays.asList(request.getProductId()))
    //                .eq(StoreProductRelation::getCategory, request.getCategory())
    //                .eq(StoreProductRelation::getUid, uid)
    //                .eq(StoreProductRelation::getType, type);
    //        dao.delete(lambdaQueryWrapper);
    //    }

    /**
     * 根据产品id和类型获取对应列表
     * 
     * @param productId 产品id
     * @param type      类型
     * @return 对应结果
     */
    @Override
    public List<StoreProductRelationResp> getList(Long productId, String type) {
        return baseMapper.getList(productId, type);
    }

    /**
     * 获取用户当前是否喜欢该商品
     * 
     * @param userId    用户id
     * @param productId 商品id
     * @return 是否喜欢标识
     */
    @Override
    public List<StoreProductRelationResp> getLikeOrCollectByUser(Long userId, Long productId, String type) {
        return baseMapper.getLikeOrCollectByUser(userId, productId, type);
    }

    /**
     * 获取用户的收藏数量
     * 
     * @param uid 用户uid
     * @return 收藏数量
     */
    @Override
    public Integer getCollectCountByUid(Long uid) {
        return baseMapper.getCollectCountByUid(uid);
    }

    /**
     * 根据商品Id取消收藏
     * 
     * @param productId 商品Id
     */
    @Override
    public void deleteByProId(Long productId) {
        baseMapper.deleteByProId(productId);
    }

    /**
     * 根据商品Id取消收藏
     * 
     * @param productId 商品Id
     * @return Boolean
     */
    @Override
    public void deleteByProIdAndUid(Long productId, Long uid) {
        baseMapper.deleteByProIdAndUid(productId, uid);
    }

    /**
     * 根据日期获取收藏量
     * 
     * @param date 日期，yyyy-MM-dd格式
     * @return Integer
     */
    @Override
    public Integer getCountByDate(String date) {
        return baseMapper.getCountByDate(date);
    }

    /**
     * 根据日期获取收藏量
     * 
     * @param date      日期，yyyy-MM-dd格式
     * @param productId 商品id
     * @return Integer
     */
    @Override
    public Integer getCountByDateAndProId(Long productId, String date) {
        return baseMapper.getCountByDateAndProId(productId, date);
    }

    //    /**
    //     * 添加收藏
    //     * @param request 收藏参数
    //     */
    //    @Override
    //    public Boolean add(UserCollectRequest request) {
    //        StoreProductRelation storeProductRelation = new StoreProductRelation();
    //        BeanUtils.copyProperties(request, storeProductRelation);
    //        storeProductRelation.setUid(userService.getUserIdException());
    //        return save(storeProductRelation);
    //    }

    //    /**
    //     * 获取用户收藏列表
    //     * @param pageParamRequest 分页参数
    //     * @return List<UserRelationResponse>
    //     */
    //    @Override
    //    public List<UserRelationResponse> getUserList(PageParamRequest pageParamRequest) {
    //        PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());
    //        Integer userId = userService.getUserIdException();
    //        return dao.getUserList(userId);
    //    }
}