/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import top.continew.admin.common.util.IdGeneratorUtil;
import top.continew.admin.common.util.ShareCodeUtil;
import top.continew.admin.mgr.ums.service.InvitationCodeService;
import top.continew.starter.cache.redisson.util.RedisUtils;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;
import top.continew.admin.common.mapper.InvitationCodeMapper;
import top.continew.admin.common.model.entity.InvitationCodeDO;
import top.continew.admin.common.model.query.InvitationCodeQuery;
import top.continew.admin.common.model.req.InvitationCodeReq;
import top.continew.admin.common.model.resp.InvitationCodeDetailResp;
import top.continew.admin.common.model.resp.InvitationCodeResp;

import java.util.Arrays;
import java.util.List;

/**
 * 邀请码业务实现
 *
 * @author joe
 * @since 2024/07/21 13:24
 */
@Service
@RequiredArgsConstructor
public class InvitationCodeServiceImpl extends BaseServiceImpl<InvitationCodeMapper, InvitationCodeDO, InvitationCodeResp, InvitationCodeDetailResp, InvitationCodeQuery, InvitationCodeReq> implements InvitationCodeService {
    private final InvitationCodeMapper baseMapper;

    private String processInvitationCode(String invitationCode) {
        if (StrUtil.isNotBlank(invitationCode)) {
            invitationCode = invitationCode.replace("0", "O").replace("1", "i").trim().toUpperCase();
        } else {
            invitationCode = "000000";
        }
        return invitationCode;
    }

    private InvitationCodeDO validateInvitationCode(String invitationCode) {
        if (!"000000".equals(invitationCode)) {
            return baseMapper.getByInvitationCode(invitationCode);
        }
        return null;
    }

    private String createAndSaveInvitationCode(String invitationCode, Long accountId, String accountName) {

        // 获取邀请人的上层
        InvitationCodeDO parentInvitation = baseMapper.getByInvitationCode(invitationCode);

        // 新增记录
        InvitationCodeDO invitationCodeDO = createInvitationCodeDO(invitationCode, accountId, accountName, parentInvitation);

        baseMapper.insert(invitationCodeDO);

        // 更新邀请路径和相关信息
        updateInvitationPathAndCounts(invitationCodeDO, parentInvitation, accountId);

        // 根据id生成邀请码
        generateAndSaveInvitationCode(invitationCodeDO);

        // 最后再保存一次，确保所有信息已更新
        saveOrUpdate(invitationCodeDO);

        return invitationCodeDO.getInvitationCode();

    }

    private void updateInvitationPathAndCounts(InvitationCodeDO InvitationCodeDO,
                                               InvitationCodeDO parentInvitation,
                                               Long accountId) {
        if (parentInvitation != null) {
            RedisUtils.set("invitee:" + parentInvitation.getAccountId(), accountId);
            RedisUtils.set("inviter:" + accountId, parentInvitation.getInvitationPath().split("/"));
            //            RedisUtils.set("inviter:" + accountId, new String[]{InvitationCodeDO.getParentInvitationCode()});

            updateGroupCountByPath(InvitationCodeDO.getInvitationPath(), 1);
            updateSingleChildCount(InvitationCodeDO.getParentInvitationCode(), 1);
        }
    }

    private InvitationCodeDO createInvitationCodeDO(String invitationCode,
                                                    Long accountId,
                                                    String accountName,
                                                    InvitationCodeDO parentInvitation) {
        InvitationCodeDO InvitationCodeDO = new InvitationCodeDO();
        long id = IdGeneratorUtil.generateSmallId();
        InvitationCodeDO.setId(id);
        InvitationCodeDO.setAccountId(accountId);
        InvitationCodeDO.setParentInvitationCode(invitationCode);
        InvitationCodeDO.setAccountName(accountName);

        if (parentInvitation != null) {
            InvitationCodeDO.setInvitationPath(parentInvitation.getInvitationPath() + "/" + invitationCode);
        } else {
            InvitationCodeDO.setInvitationPath("000000");
        }
        return InvitationCodeDO;
    }

    @Override
    public String processAndCreateInvitationCodeForAccount(String invitationCode, Long accountId, String accountName) {
        // 校验邀请码
        String processedInvitationCode = processInvitationCode(invitationCode);
        InvitationCodeDO invitationCodeDO = validateInvitationCode(processedInvitationCode);
        if (null == invitationCodeDO) {
            processedInvitationCode = "000000";
        }
        // 判断输入的邀请码是否存在
        InvitationCodeDO exitInvitationCodeDO = baseMapper.getByAccountName(accountName);
        if (null == exitInvitationCodeDO) {
            return createAndSaveInvitationCode(processedInvitationCode, accountId, accountName);
        } else {
            return "";
        }

    }

    @Override
    public void updateGroupCountByPath(String invitationPath, Integer addCount) {
        List<String> invitationCodeList = Arrays.asList(invitationPath.split("/"));
        baseMapper.updateGroupCountByPath(invitationCodeList, addCount);
    }

    @Override
    public void updateSingleChildCount(String parentInvitationCode, Integer addCount) {
        baseMapper.updateSingleChildCount(parentInvitationCode, addCount);
    }

    private void generateAndSaveInvitationCode(InvitationCodeDO invitationCodeDO) {
        if (null != invitationCodeDO.getId()) {
            String invitationCode = ShareCodeUtil.toSerialCode(invitationCodeDO.getId());
            invitationCodeDO.setInvitationCode(invitationCode);

        }
    }

}