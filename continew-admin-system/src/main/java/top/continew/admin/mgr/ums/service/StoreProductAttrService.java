/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service;

import top.continew.admin.common.model.entity.StoreProductAttrDO;
import top.continew.starter.extension.crud.service.BaseService;
import top.continew.admin.common.model.query.StoreProductAttrQuery;
import top.continew.admin.common.model.req.StoreProductAttrReq;
import top.continew.admin.common.model.resp.StoreProductAttrDetailResp;
import top.continew.admin.common.model.resp.StoreProductAttrResp;

import java.util.List;

/**
 * 商品属性业务接口
 *
 * @author joe
 * @since 2024/08/17 21:58
 */
public interface StoreProductAttrService extends BaseService<StoreProductAttrResp, StoreProductAttrDetailResp, StoreProductAttrQuery, StoreProductAttrReq> {
    List<StoreProductAttrResp> getByEntity(StoreProductAttrQuery query);

    void removeByProductId(Long productId, Integer type);

    void deleteByProductIdAndType(Long productId, Integer type);

    List<StoreProductAttrResp> getListByProductIdAndType(Long productId, Integer type);

    void saveBatch(List<StoreProductAttrDO> attrList);
}