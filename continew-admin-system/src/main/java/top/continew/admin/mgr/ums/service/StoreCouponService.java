/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service;

import top.continew.admin.common.model.entity.StoreCouponDO;
import top.continew.starter.extension.crud.service.BaseService;
import top.continew.admin.common.model.query.StoreCouponQuery;
import top.continew.admin.common.model.req.StoreCouponReq;
import top.continew.admin.common.model.resp.StoreCouponDetailResp;
import top.continew.admin.common.model.resp.StoreCouponResp;

import java.util.List;

/**
 * 优惠券业务接口
 *
 * @author joe
 * @since 2024/08/19 16:14
 */
public interface StoreCouponService extends BaseService<StoreCouponResp, StoreCouponDetailResp, StoreCouponQuery, StoreCouponReq> {
    Boolean create(StoreCouponReq request);

    StoreCouponDO getInfoException(Long id);

    StoreCouponResp info(Long id);

    List<StoreCouponResp> getByIds(List<Long> ids);

    void deduction(Long id, Integer num, Boolean isLimited);

    void deleteCoupon(Long id);
}