/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service;

import top.continew.admin.common.model.entity.AccountDO;
import top.continew.admin.common.model.query.AccountQuery;
import top.continew.admin.common.model.req.AccountAddReq;
import top.continew.admin.common.model.req.AccountPasswordResetReq;
import top.continew.admin.common.model.req.AccountReq;
import top.continew.admin.common.model.resp.AccountDetailResp;
import top.continew.admin.common.model.resp.AccountResp;
import top.continew.starter.data.mybatis.plus.service.IService;
import top.continew.starter.extension.crud.model.query.PageQuery;
import top.continew.starter.extension.crud.model.resp.PageResp;
import top.continew.starter.extension.crud.service.BaseService;

import java.lang.reflect.InvocationTargetException;

/**
 * 商户列表业务接口
 *
 * @author joe
 * @since 2024/07/17 12:14
 */
public interface AccountService extends BaseService<AccountResp, AccountDetailResp, AccountQuery, AccountReq>, IService<AccountDO> {
    /**
     * 注册新商户
     *
     * @param accountAddReq 商户信息
     * @return ID
     */
    Long registerNewAccount(AccountAddReq accountAddReq) throws InvocationTargetException, IllegalAccessException;

    /**
     * 查看分页
     *
     * @param query     商户查询
     * @param pageQuery 分页查询条件
     * @return ID
     */
    PageResp<AccountResp> page(AccountQuery query, PageQuery pageQuery);

    /**
     * 重置密码
     *
     * @param req 重置信息
     * @param id  ID
     */
    void resetPassword(AccountPasswordResetReq req, Long id);

    /**
     * 修改密码
     *
     * @param oldPassword 当前密码
     * @param newPassword 新密码
     * @param id          ID
     */
    void updatePassword(String oldPassword, String newPassword, Long id);

    /**
     * 修改支付密码
     *
     * @param oldPayPassword 当前支付密码
     * @param newPayPassword 新支付密码
     * @param id             ID
     */
    void updatePayPassword(String oldPayPassword, String newPayPassword, Long id);

    /**
     * 根据用户名查询
     *
     * @param accountName 用户名
     * @return 用户信息
     */
    AccountDO getByAccountName(String accountName);

    /**
     * 冻结商户
     *
     * @param id ID
     * @return 用户信息
     */
    void freezeAccount(Long id);

    /**
     * 解冻商户
     *
     * @param id ID
     * @return 用户信息
     */
    void unfreezeAccount(Long id);

}