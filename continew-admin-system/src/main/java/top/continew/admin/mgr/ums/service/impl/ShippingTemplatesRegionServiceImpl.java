/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import cn.hutool.core.collection.CollUtil;
import lombok.RequiredArgsConstructor;

import net.dreamlu.mica.core.utils.BeanUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

import top.continew.admin.common.util.CommonUtil;
import top.continew.admin.mgr.ums.service.ShippingTemplatesRegionService;
import top.continew.admin.mgr.ums.service.SystemCityService;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;
import top.continew.admin.common.mapper.ShippingTemplatesRegionMapper;
import top.continew.admin.common.model.entity.ShippingTemplatesRegionDO;
import top.continew.admin.common.model.query.ShippingTemplatesRegionQuery;
import top.continew.admin.common.model.req.ShippingTemplatesRegionReq;
import top.continew.admin.common.model.resp.ShippingTemplatesRegionDetailResp;
import top.continew.admin.common.model.resp.ShippingTemplatesRegionResp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 运费模板指定区域费用业务实现
 *
 * @author joe
 * @since 2024/08/08 12:14
 */
@Service
@RequiredArgsConstructor
public class ShippingTemplatesRegionServiceImpl extends BaseServiceImpl<ShippingTemplatesRegionMapper, ShippingTemplatesRegionDO, ShippingTemplatesRegionResp, ShippingTemplatesRegionDetailResp, ShippingTemplatesRegionQuery, ShippingTemplatesRegionReq> implements ShippingTemplatesRegionService {

    private List<Long> cityIdList;
    private final SystemCityService systemCityService;

    @Override
    public void saveAll(List<ShippingTemplatesRegionReq> shippingTemplatesRegionRequestList,
                        Integer type,
                        Long tempId) {
        ArrayList<ShippingTemplatesRegionDO> shippingTemplatesRegionList = new ArrayList<>();
        List<ShippingTemplatesRegionResp> existShippingTemplatesRegionList = baseMapper.getBytempId(tempId);
        if (CollUtil.isNotEmpty(existShippingTemplatesRegionList)) {
            //把目前模板下的所有数据标记为无效
            baseMapper.updateStatus(tempId);
        }

        if (CollUtil.isNotEmpty(shippingTemplatesRegionRequestList)) {

            for (ShippingTemplatesRegionReq shippingTemplatesRegionRequest : shippingTemplatesRegionRequestList) {
                String uniqueKey = DigestUtils.md5Hex(shippingTemplatesRegionRequest.toString());

                if ("all".equals(shippingTemplatesRegionRequest.getCityId()) || "0"
                    .equals(shippingTemplatesRegionRequest.getCityId())) {
                    cityIdList = systemCityService.getCityIdList();
                } else {
                    cityIdList = CommonUtil.stringToArrayLong(shippingTemplatesRegionRequest.getCityId());
                }
                for (Long cityId : cityIdList) {
                    ShippingTemplatesRegionDO shippingTemplatesRegion = new ShippingTemplatesRegionDO();
                    BeanUtil.copyProperties(shippingTemplatesRegionRequest, shippingTemplatesRegion);
                    shippingTemplatesRegion.setCityId(cityId);
                    shippingTemplatesRegion.setUniqid(uniqueKey);
                    shippingTemplatesRegion.setTempId(tempId);
                    shippingTemplatesRegion.setType(type);
                    shippingTemplatesRegion.setStatus(true);

                    shippingTemplatesRegionList.add(shippingTemplatesRegion);
                }
            }
            //批量保存模板数据
            baseMapper.insertBatch(shippingTemplatesRegionList);
        }
        if (CollUtil.isNotEmpty(existShippingTemplatesRegionList)) {
            baseMapper.delete(tempId);
        }
    }

    /**
     * 删除模板下的无效数据
     * 
     * @param tempId Long 运费模板id
     */
    @Override
    public Boolean delete(Long tempId) {
        baseMapper.delete(tempId);
        return true;
    }

    /**
     * 根据多个id查询
     *
     * @param idList
     */
    @Override
    public List<ShippingTemplatesRegionResp> getListByIds(Collection<? extends Serializable> idList) {
        return baseMapper.getListByIds(idList);
    }

    /**
     * 根据模板编号、城市ID查询
     */
    @Override
    public ShippingTemplatesRegionResp getByTempIdAndCityId(Long tempId, Long cityId) {
        return baseMapper.getByTempIdAndCityId(tempId, cityId);
    }

    /**
     * 分组查询
     */
    @Override
    public List<ShippingTemplatesRegionResp> getListGroup(Long tempId) {
        return baseMapper.getListGroup(tempId);
    }

}