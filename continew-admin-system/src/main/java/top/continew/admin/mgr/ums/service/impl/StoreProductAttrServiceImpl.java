/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import top.continew.admin.mgr.ums.service.StoreProductAttrService;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;
import top.continew.admin.common.mapper.StoreProductAttrMapper;
import top.continew.admin.common.model.entity.StoreProductAttrDO;
import top.continew.admin.common.model.query.StoreProductAttrQuery;
import top.continew.admin.common.model.req.StoreProductAttrReq;
import top.continew.admin.common.model.resp.StoreProductAttrDetailResp;
import top.continew.admin.common.model.resp.StoreProductAttrResp;

import java.util.List;

/**
 * 商品属性业务实现
 *
 * @author joe
 * @since 2024/08/17 21:58
 */
@Service
@RequiredArgsConstructor
public class StoreProductAttrServiceImpl extends BaseServiceImpl<StoreProductAttrMapper, StoreProductAttrDO, StoreProductAttrResp, StoreProductAttrDetailResp, StoreProductAttrQuery, StoreProductAttrReq> implements StoreProductAttrService {
    /**
     * 根据产品属性查询
     *
     * @param query 查询参数
     * @return 查询结果
     */
    @Override
    public List<StoreProductAttrResp> getByEntity(StoreProductAttrQuery query) {
        QueryWrapper<StoreProductAttrDO> queryWrapper = this.buildQueryWrapper(query);
        return baseMapper.getByEntity(queryWrapper);
    }

    /**
     * 根据id删除商品
     * 
     * @param productId 待删除商品id
     * @param type      类型区分是是否添加营销
     */
    @Override
    public void removeByProductId(Long productId, Integer type) {
        baseMapper.removeByProductId(productId, type);
    }

    /**
     * 删除商品规格
     * 
     * @param productId 商品id
     * @param type      商品类型
     */
    @Override
    public void deleteByProductIdAndType(Long productId, Integer type) {
        baseMapper.deleteByProductIdAndType(productId, type);
    }

    /**
     * 获取商品规格列表
     * 
     * @param productId 商品id
     * @param type      商品类型
     * @return List
     */
    @Override
    public List<StoreProductAttrResp> getListByProductIdAndType(Long productId, Integer type) {
        return baseMapper.getListByProductIdAndType(productId, type);
    }

    /**
     * 批量保存
     */
    @Override
    public void saveBatch(List<StoreProductAttrDO> attrList) {
        baseMapper.insertBatch(attrList);
    }
}