/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import top.continew.admin.mgr.ums.service.PersonShopIdentificationService;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;
import top.continew.admin.common.mapper.PersonShopIdentificationMapper;
import top.continew.admin.common.model.entity.PersonShopIdentificationDO;
import top.continew.admin.common.model.query.PersonShopIdentificationQuery;
import top.continew.admin.common.model.req.PersonShopIdentificationReq;
import top.continew.admin.common.model.resp.PersonShopIdentificationDetailResp;
import top.continew.admin.common.model.resp.PersonShopIdentificationResp;

/**
 * 商铺认证业务实现
 *
 * @author joe
 * @since 2024/07/30 15:48
 */
@Service
@RequiredArgsConstructor
public class PersonShopIdentificationServiceImpl extends BaseServiceImpl<PersonShopIdentificationMapper, PersonShopIdentificationDO, PersonShopIdentificationResp, PersonShopIdentificationDetailResp, PersonShopIdentificationQuery, PersonShopIdentificationReq> implements PersonShopIdentificationService {}