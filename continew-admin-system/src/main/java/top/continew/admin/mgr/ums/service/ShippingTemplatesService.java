/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service;

import org.springframework.transaction.annotation.Transactional;
import top.continew.admin.common.model.entity.ShippingTemplatesDO;
import top.continew.starter.extension.crud.service.BaseService;
import top.continew.admin.common.model.query.ShippingTemplatesQuery;
import top.continew.admin.common.model.req.ShippingTemplatesReq;
import top.continew.admin.common.model.resp.ShippingTemplatesDetailResp;
import top.continew.admin.common.model.resp.ShippingTemplatesResp;

import java.util.List;

/**
 * 运费模板业务接口
 *
 * @author joe
 * @since 2024/08/08 10:04
 */
public interface ShippingTemplatesService extends BaseService<ShippingTemplatesResp, ShippingTemplatesDetailResp, ShippingTemplatesQuery, ShippingTemplatesReq> {
    Boolean createNewTemplate(ShippingTemplatesReq request);

    Boolean updateTemplate(Long id, ShippingTemplatesReq request);

    ShippingTemplatesDO getInfo(Long id);

    List<ShippingTemplatesDO> getAll();

    @Transactional(rollbackFor = Exception.class)
    Boolean remove(Long id);
}