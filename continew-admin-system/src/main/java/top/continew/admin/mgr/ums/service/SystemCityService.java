/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service;

import top.continew.admin.common.model.entity.SystemCityDO;
import top.continew.admin.common.model.vo.SystemCityTreeVO;
import top.continew.starter.extension.crud.service.BaseService;
import top.continew.admin.common.model.query.SystemCityQuery;
import top.continew.admin.common.model.req.SystemCityReq;
import top.continew.admin.common.model.resp.SystemCityDetailResp;
import top.continew.admin.common.model.resp.SystemCityResp;

import java.util.List;

/**
 * 城市业务接口
 *
 * @author joe
 * @since 2024/08/08 21:46
 */
public interface SystemCityService extends BaseService<SystemCityResp, SystemCityDetailResp, SystemCityQuery, SystemCityReq> {
    Object getList(SystemCityReq request);

    Boolean updateStatus(Long id, Boolean status);

    Boolean update(Long id, SystemCityReq request);

    List<SystemCityTreeVO> getListTree();

    List<SystemCityTreeVO> getListTreeTop();

    List<SystemCityTreeVO> getAllDescendants(Long topLevelCityId);

    List<SystemCityTreeVO> getListTreeChild(Long parentId);

    List<Long> getCityIdList();

    SystemCityDO getCityByCityId(Long cityId);

    SystemCityDO getCityByCityName(String cityName);
}