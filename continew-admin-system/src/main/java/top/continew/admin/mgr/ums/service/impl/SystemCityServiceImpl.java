/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import lombok.RequiredArgsConstructor;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import top.continew.admin.common.constant.StoreConstants;
import top.continew.admin.common.model.vo.SystemCityTreeVO;
import top.continew.admin.mgr.ums.service.SystemCityAsyncService;
import top.continew.admin.mgr.ums.service.SystemCityService;
import top.continew.starter.cache.redisson.util.RedisUtils;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;
import top.continew.admin.common.mapper.SystemCityMapper;
import top.continew.admin.common.model.entity.SystemCityDO;
import top.continew.admin.common.model.query.SystemCityQuery;
import top.continew.admin.common.model.req.SystemCityReq;
import top.continew.admin.common.model.resp.SystemCityDetailResp;
import top.continew.admin.common.model.resp.SystemCityResp;

import java.time.Duration;
import java.util.List;

/**
 * 城市业务实现
 *
 * @author joe
 * @since 2024/08/08 21:46
 */
@Service
@RequiredArgsConstructor
public class SystemCityServiceImpl extends BaseServiceImpl<SystemCityMapper, SystemCityDO, SystemCityResp, SystemCityDetailResp, SystemCityQuery, SystemCityReq> implements SystemCityService {

    private final SystemCityAsyncService systemCityAsyncService;

    /**
     * 列表
     *
     * @param request 请求参数
     * @return List<SystemCity>
     */
    @Override
    public Object getList(SystemCityReq request) {
        Object list = RedisUtils.get(StoreConstants.CITY_LIST);
        if (ObjectUtil.isNull(list)) {
            //城市数据，异步同步到redis，第一次拿不到数据，去数据库读取
            list = baseMapper.getByParentId(request.getParentId());
        }
        return list;
    }

    /**
     * 修改状态
     *
     * @param id     城市id
     * @param status 状态
     */
    @Override
    public Boolean updateStatus(Long id, Boolean status) {
        SystemCityDO systemCity = baseMapper.selectById(id);
        systemCity.setId(id);
        systemCity.setIsShow(status);
        boolean result = updateById(systemCity);
        if (result) {
            asyncRedis(systemCity.getParentId());
        }
        return result;
    }

    /**
     * 修改城市
     *
     * @param id      城市id
     * @param request 修改参数
     */
    @Override
    public Boolean update(Long id, SystemCityReq request) {
        SystemCityDO systemCity = new SystemCityDO();
        BeanUtils.copyProperties(request, systemCity);
        systemCity.setId(id);
        boolean result = updateById(systemCity);
        if (result) {
            asyncRedis(request.getParentId());
        }
        return result;
    }

    /**
     * 获取tree结构的列表
     *
     * @return Object
     */
    @Override
    public List<SystemCityTreeVO> getListTree() {
        List<SystemCityTreeVO> cityList = RedisUtils.get(StoreConstants.CITY_LIST_TREE);
        if (CollUtil.isEmpty(cityList)) {
            systemCityAsyncService.setListTree();
        }
        return RedisUtils.get(StoreConstants.CITY_LIST_TREE);
    }

    /**
     * 获取tree结构的列表
     *
     * @return Object
     */
    @Override
    public List<SystemCityTreeVO> getListTreeTop() {
        List<SystemCityTreeVO> cityList = RedisUtils.get(StoreConstants.CITY_LIST_TREE_TOP);
        if (CollUtil.isEmpty(cityList)) {
            systemCityAsyncService.setListTree();
            systemCityAsyncService.getTopLevelNodes();
        }
        return RedisUtils.get(StoreConstants.CITY_LIST_TREE_TOP);
    }

    /**
     * 获取顶层节点下所有的子节点
     *
     * @return Object
     */
    @Override
    public List<SystemCityTreeVO> getAllDescendants(Long topLevelCityId) {
        List<SystemCityTreeVO> cityList = RedisUtils.get(StoreConstants.CITY_LIST_TREE_TOP + topLevelCityId);

        if (CollUtil.isEmpty(cityList)) {
            systemCityAsyncService.getAllDescendants(topLevelCityId);

        }
        return RedisUtils.get(StoreConstants.CITY_LIST_TREE_TOP + topLevelCityId);

    }

    /**
     * 获取tree结构的列表
     *
     * @return Object
     */
    @Override
    public List<SystemCityTreeVO> getListTreeChild(Long parentId) {
        List<SystemCityTreeVO> cityList = RedisUtils.get(StoreConstants.CITY_LIST_TREE_CHILDREN + parentId);
        if (CollUtil.isEmpty(cityList)) {
            systemCityAsyncService.getChildNodes(parentId);
        }
        return RedisUtils.get(StoreConstants.CITY_LIST_TREE_CHILDREN + parentId);
    }

    /**
     * 获取所有城市cityId
     *
     * @return List<Integer>
     */
    @Override
    public List<Long> getCityIdList() {
        Object data = RedisUtils.get(StoreConstants.CITY_LIST_LEVEL_1);
        List<Long> collect;
        if (data == null || "".equals(data) || !data.getClass().isArray()) {
            collect = baseMapper.getCityIdList();
            RedisUtils.set(StoreConstants.CITY_LIST_LEVEL_1, collect, Duration.ofMinutes(Integer.parseInt("10")));
        } else {
            collect = (List<Long>)data;
        }

        return collect;
    }

    /**
     * 根据city_id获取城市信息
     */
    @Override
    public SystemCityDO getCityByCityId(Long cityId) {
        return baseMapper.getCityByCityId(cityId);
    }

    /**
     * 数据整体刷入redis
     */
    public void asyncRedis(Long pid) {
        systemCityAsyncService.async(pid);
    }

    /**
     * 根据城市名称获取城市详细数据
     *
     * @param cityName 城市名称
     * @return 城市数据
     */
    @Override
    public SystemCityDO getCityByCityName(String cityName) {
        return baseMapper.getCityByCityName(cityName);
    }

}