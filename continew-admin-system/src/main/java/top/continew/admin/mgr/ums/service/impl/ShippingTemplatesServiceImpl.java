/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import cn.hutool.core.collection.CollUtil;
import lombok.RequiredArgsConstructor;

import net.dreamlu.mica.core.utils.BeanUtil;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import top.continew.admin.common.model.req.ShippingTemplatesFreeReq;
import top.continew.admin.common.model.req.ShippingTemplatesRegionReq;
import top.continew.admin.mgr.ums.service.ShippingTemplatesService;
import top.continew.starter.core.util.validate.CheckUtils;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;
import top.continew.admin.common.mapper.ShippingTemplatesMapper;
import top.continew.admin.common.model.entity.ShippingTemplatesDO;
import top.continew.admin.common.model.query.ShippingTemplatesQuery;
import top.continew.admin.common.model.req.ShippingTemplatesReq;
import top.continew.admin.common.model.resp.ShippingTemplatesDetailResp;
import top.continew.admin.common.model.resp.ShippingTemplatesResp;

import java.util.List;

/**
 * 运费模板业务实现
 *
 * @author joe
 * @since 2024/08/08 10:04
 */
@Service
@RequiredArgsConstructor
public class ShippingTemplatesServiceImpl extends BaseServiceImpl<ShippingTemplatesMapper, ShippingTemplatesDO, ShippingTemplatesResp, ShippingTemplatesDetailResp, ShippingTemplatesQuery, ShippingTemplatesReq> implements ShippingTemplatesService {
    private final ShippingTemplatesRegionServiceImpl shippingTemplatesRegionService;
    private final ShippingTemplatesFreeServiceImpl shippingTemplatesFreeService;

    /**
     * 新增模板信息
     * 
     * @param shippingTemplatesReq 新增参数
     * @return bool
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean createNewTemplate(ShippingTemplatesReq shippingTemplatesReq) {
        // 判断模板名称是否重复
        ShippingTemplatesDO templates = baseMapper.getByName(shippingTemplatesReq.getName());
        CheckUtils.throwIfNotNull(templates, "模板名称已存在,请更换模板名称!");

        List<ShippingTemplatesRegionReq> shippingTemplatesRegionRequestList = shippingTemplatesReq
            .getShippingTemplatesRegionRequestList();
        CheckUtils.throwIf(CollUtil.isEmpty(shippingTemplatesRegionRequestList), "区域运费最少需要一条默认的全国区域");

        ShippingTemplatesDO shippingTemplates = new ShippingTemplatesDO();
        BeanUtil.copyProperties(shippingTemplatesReq, shippingTemplates);
        // 将新账户插入数据库
        baseMapper.insert(shippingTemplates);

        //区域运费
        shippingTemplatesRegionService.saveAll(shippingTemplatesRegionRequestList, shippingTemplatesReq
            .getType(), shippingTemplates.getId());
        List<ShippingTemplatesFreeReq> shippingTemplatesFreeRequestList = shippingTemplatesReq
            .getShippingTemplatesFreeRequestList();
        if (null != shippingTemplatesFreeRequestList && !shippingTemplatesFreeRequestList
            .isEmpty() && shippingTemplatesReq.getAppoint()) {
            shippingTemplatesFreeService.saveAll(shippingTemplatesFreeRequestList, shippingTemplatesReq
                .getType(), shippingTemplates.getId());
        }

        return true;
    }

    /**
     * 修改模板信息
     * 
     * @param id                   Long 模板id
     * @param shippingTemplatesReq 新增参数
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateTemplate(Long id, ShippingTemplatesReq shippingTemplatesReq) {
        ShippingTemplatesDO shippingTemplatesDO = baseMapper.selectById(id);

        CheckUtils.throwIfNull(shippingTemplatesDO, "模板不存在!");

        BeanUtil.copyProperties(shippingTemplatesReq, shippingTemplatesDO);
        baseMapper.updateById(shippingTemplatesDO);

        //区域运费
        List<ShippingTemplatesRegionReq> shippingTemplatesRegionRequestList = shippingTemplatesReq
            .getShippingTemplatesRegionRequestList();

        CheckUtils.throwIf(shippingTemplatesRegionRequestList.isEmpty(), "请设置区域配送信息！");

        shippingTemplatesRegionService.saveAll(shippingTemplatesRegionRequestList, shippingTemplatesReq
            .getType(), shippingTemplatesDO.getId());

        List<ShippingTemplatesFreeReq> shippingTemplatesFreeRequestList = shippingTemplatesReq
            .getShippingTemplatesFreeRequestList();
        if (CollUtil.isNotEmpty(shippingTemplatesFreeRequestList) && shippingTemplatesReq.getAppoint()) {
            shippingTemplatesFreeService.saveAll(shippingTemplatesFreeRequestList, shippingTemplatesReq
                .getType(), shippingTemplatesDO.getId());
        }

        return true;
    }

    /**
     * 获取模板信息
     * 
     * @param id 模板id
     * @return ShippingTemplates
     */
    @Override
    public ShippingTemplatesDO getInfo(Long id) {
        ShippingTemplatesDO template = baseMapper.selectById(id);
        return template;
    }

    /**
     * 获取模板信息
     *
     * @return ShippingTemplates
     */
    @Override
    public List<ShippingTemplatesDO> getAll() {
        List<ShippingTemplatesDO> templates = baseMapper.getAll();
        return templates;
    }

    /**
     * 删除运费模板
     * 
     * @param id Long
     * @return boolean
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean remove(Long id) {
        shippingTemplatesRegionService.delete(id);
        shippingTemplatesFreeService.delete(id);
        baseMapper.deleteById(id);
        return true;
    }

}