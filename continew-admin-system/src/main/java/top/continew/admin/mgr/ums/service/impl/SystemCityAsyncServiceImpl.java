/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import cn.hutool.core.collection.CollUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import top.continew.admin.common.constant.StoreConstants;
import top.continew.admin.common.mapper.SystemCityMapper;
import top.continew.admin.common.model.entity.SystemCityDO;
import top.continew.admin.common.model.query.SystemCityQuery;
import top.continew.admin.common.model.req.SystemCityReq;
import top.continew.admin.common.model.resp.SystemCityDetailResp;
import top.continew.admin.common.model.resp.SystemCityResp;
import top.continew.admin.common.model.vo.SystemCityTreeVO;
import top.continew.admin.mgr.ums.service.SystemCityAsyncService;
import top.continew.starter.cache.redisson.util.RedisUtils;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 城市业务实现
 *
 * @author joe
 * @since 2024/08/08 21:46
 */
@Service
@RequiredArgsConstructor
public class SystemCityAsyncServiceImpl extends BaseServiceImpl<SystemCityMapper, SystemCityDO, SystemCityResp, SystemCityDetailResp, SystemCityQuery, SystemCityReq> implements SystemCityAsyncService {

    public void asyncList(Long pid) {
        List<SystemCityDO> systemCityList = baseMapper.selectListByParentId(pid);
        if (systemCityList != null && !systemCityList.isEmpty()) {
            RedisUtils.set(StoreConstants.CITY_LIST, systemCityList);
        }

    }

    @Override
    public void setListTree() {
        // 获取所有城市数据
        List<SystemCityDO> allCities = baseMapper.selectList();

        // 用于存储顶层节点
        List<SystemCityTreeVO> topLevelNodes = new ArrayList<>();
        // 用于存储子节点映射
        Map<Long, List<SystemCityTreeVO>> childNodeMap = new HashMap<>();

        if (!CollUtil.isEmpty(allCities)) {
            for (SystemCityDO city : allCities) {
                SystemCityTreeVO treeVo = new SystemCityTreeVO();
                BeanUtils.copyProperties(city, treeVo);

                if (city.getParentId() == null || city.getParentId() == 0) {
                    // 顶层节点
                    topLevelNodes.add(treeVo);
                } else {
                    // 子节点，按 parentId 分组存储
                    childNodeMap.computeIfAbsent(city.getParentId(), k -> new ArrayList<>()).add(treeVo);
                }
            }
        }

        // 将顶层节点存储到 Redis
        RedisUtils.set(StoreConstants.CITY_LIST_TREE_TOP, topLevelNodes);

        // 将每个节点的子节点存储到 Redis
        for (Map.Entry<Long, List<SystemCityTreeVO>> entry : childNodeMap.entrySet()) {
            RedisUtils.set(StoreConstants.CITY_LIST_TREE_CHILDREN + entry.getKey(), entry.getValue());
        }
    }

    @Override
    public List<SystemCityTreeVO> getAllDescendants(Long topLevelCityId) {
        // 从 Redis 中获取顶层节点及其所有子节点
        List<SystemCityTreeVO> topLevelChildren = RedisUtils.get(StoreConstants.CITY_LIST_TREE_TOP + topLevelCityId);

        if (topLevelChildren == null) {
            // 如果 Redis 中没有数据，可以选择从数据库中重新加载
            // 获取所有城市数据
            List<SystemCityDO> allCities = baseMapper.selectList();
            // 用于存储子节点映射
            Map<Long, List<SystemCityTreeVO>> childNodeMap = new HashMap<>();

            if (!CollUtil.isEmpty(allCities)) {
                for (SystemCityDO city : allCities) {
                    SystemCityTreeVO treeVo = new SystemCityTreeVO();
                    BeanUtils.copyProperties(city, treeVo);

                    // 子节点，按 parentId 分组存储
                    childNodeMap.computeIfAbsent(city.getParentId(), k -> new ArrayList<>()).add(treeVo);
                }
            }

            // 获取顶层节点的子节点（市）
            topLevelChildren = childNodeMap.get(topLevelCityId);
            if (topLevelChildren != null) {
                for (SystemCityTreeVO city : topLevelChildren) {
                    // 获取市下的所有区，并放入市的 child 中
                    List<SystemCityTreeVO> districts = getChildren(city.getCityId(), childNodeMap);
                    city.setChild(districts);
                }
            }

            // 重新将结果存入 Redis
            RedisUtils.set(StoreConstants.CITY_LIST_TREE_TOP + topLevelCityId, topLevelChildren);
        }

        return topLevelChildren;
    }

    private List<SystemCityTreeVO> getChildren(Long parentId, Map<Long, List<SystemCityTreeVO>> childNodeMap) {
        List<SystemCityTreeVO> children = childNodeMap.get(parentId);

        if (children == null) {
            return new ArrayList<>();
        }

        for (SystemCityTreeVO child : children) {
            // 递归获取每个子节点的子节点
            List<SystemCityTreeVO> subChildren = getChildren(child.getId(), childNodeMap);
            // 将子节点设置到当前节点的 child 属性中
            child.setChild(subChildren);
        }

        return children;
    }

    @Override
    public List<SystemCityTreeVO> getTopLevelNodes() {
        RedisUtils.get(StoreConstants.CITY_LIST_TREE_TOP);
        // 从 Redis 获取顶层节点
        return RedisUtils.get(StoreConstants.CITY_LIST_TREE_TOP);
    }

    @Override
    public List<SystemCityTreeVO> getChildNodes(Long parentId) {
        // 从 Redis 获取指定父节点的子节点
        return RedisUtils.get(StoreConstants.CITY_LIST_TREE_CHILDREN + parentId);
    }

    /**
     * 数据整体刷入redis
     */
    @Async
    public void async(Long pid) {
        asyncList(pid);
        setListTree();
    }
}