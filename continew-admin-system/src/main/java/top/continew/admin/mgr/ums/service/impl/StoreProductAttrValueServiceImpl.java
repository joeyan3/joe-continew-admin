/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import top.continew.admin.mgr.ums.service.StoreProductAttrValueService;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;
import top.continew.admin.common.mapper.StoreProductAttrValueMapper;
import top.continew.admin.common.model.entity.StoreProductAttrValueDO;
import top.continew.admin.common.model.query.StoreProductAttrValueQuery;
import top.continew.admin.common.model.req.StoreProductAttrValueReq;
import top.continew.admin.common.model.resp.StoreProductAttrValueDetailResp;
import top.continew.admin.common.model.resp.StoreProductAttrValueResp;

import java.util.List;

/**
 * 商品属性值业务实现
 *
 * @author joe
 * @since 2024/08/17 22:19
 */
@Service
@RequiredArgsConstructor
public class StoreProductAttrValueServiceImpl extends BaseServiceImpl<StoreProductAttrValueMapper, StoreProductAttrValueDO, StoreProductAttrValueResp, StoreProductAttrValueDetailResp, StoreProductAttrValueQuery, StoreProductAttrValueReq> implements StoreProductAttrValueService {

    /**
     *
     * @param productId 商品id
     * @param attrId    属性id
     * @return 商品属性集合
     */
    @Override
    public List<StoreProductAttrValueDO> getListByProductIdAndAttrId(Long productId, Long attrId, Integer type) {

        return baseMapper.getListByProductIdAndAttrId(productId, attrId, type);
    }

    /**
     * 根据产品属性查询
     *
     * @param query 查询参数
     * @return 查询结果
     */
    @Override
    public List<StoreProductAttrValueResp> getByEntity(StoreProductAttrValueQuery query) {
        QueryWrapper<StoreProductAttrValueDO> queryWrapper = this.buildQueryWrapper(query);
        return baseMapper.getByEntity(queryWrapper);
    }

    /**
     * 根据商品id删除AttrValue
     * 
     * @param productId 商品id
     * @param type      类型区分是是否添加营销
     * @reture 删除结果
     */
    @Override
    public void removeByProductId(Long productId, Integer type) {
        baseMapper.removeByProductId(productId, type);
    }

    /**
     * 根据id、类型查询
     * 
     * @param id   ID
     * @param type 类型
     * @return StoreProductAttrValue
     */
    @Override
    public StoreProductAttrValueResp getByIdAndProductIdAndType(Long id, Long productId, Integer type) {

        return baseMapper.getByIdAndProductIdAndType(id, productId, type);
    }

    /**
     * 根据sku查询
     * 
     * @param productId 商品id
     * @param suk       sku
     * @param type      规格类型
     * @return StoreProductAttrValue
     */
    @Override
    public StoreProductAttrValueResp getByProductIdAndSkuAndType(Long productId, String suk, Integer type) {

        return baseMapper.getByProductIdAndSkuAndType(productId, suk, type);
    }

    /**
     * 添加(退货)/扣减库存
     * 
     * @param id            秒杀商品id
     * @param num           数量
     * @param operationType 类型：add—添加，sub—扣减
     * @param type          活动类型 0=商品，1=秒杀，2=砍价，3=拼团
     */
    @Override
    public void operationStock(Long id, Integer num, String operationType, Integer type) {
        baseMapper.operationStock(id, num, operationType, type);
    }

    /**
     * 删除商品规格属性值
     * 
     * @param productId 商品id
     * @param type      商品类型
     * @return Boolean
     */
    @Override
    public void deleteByProductIdAndType(Long productId, Integer type) {
        baseMapper.deleteByProductIdAndType(productId, type);
    }

    /**
     * 获取商品规格列表
     * 
     * @param productId 商品id
     * @param type      商品类型
     * @return List
     */
    @Override
    public List<StoreProductAttrValueResp> getListByProductIdAndType(Long productId, Integer type) {
        return baseMapper.getListByProductIdAndType(productId, type);
    }

    /**
     * 批量保存
     */
    @Override
    public void saveBatch(List<StoreProductAttrValueDO> attrValueList) {
        baseMapper.insertBatch(attrValueList);
    }

    /**
     * 更新
     */
    @Override
    public void update(StoreProductAttrValueDO storeProductAttrValueDO) {
        baseMapper.updateById(storeProductAttrValueDO);
    }

    /**
     * 批量更新
     */
    @Override
    public void updateBatchById(List<StoreProductAttrValueDO> attrList) {
        baseMapper.updateBatchById(attrList);
    }

}