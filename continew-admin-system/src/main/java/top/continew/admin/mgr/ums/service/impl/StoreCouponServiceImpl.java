/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import top.continew.admin.common.constant.StoreConstants;
import top.continew.admin.common.mapper.StoreCouponMapper;
import top.continew.admin.common.model.entity.CategoryDO;
import top.continew.admin.common.model.entity.StoreCouponDO;
import top.continew.admin.common.model.query.StoreCouponQuery;
import top.continew.admin.common.model.req.StoreCouponReq;
import top.continew.admin.common.model.resp.StoreCouponDetailResp;
import top.continew.admin.common.model.resp.StoreCouponResp;
import top.continew.admin.common.model.resp.StoreProductResp;
import top.continew.admin.common.util.DateUtils;
import top.continew.admin.mgr.ums.service.CategoryService;
import top.continew.admin.mgr.ums.service.StoreCouponService;
import top.continew.starter.core.util.validate.CheckUtils;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;

import java.util.List;

/**
 * 优惠券业务实现
 *
 * @author joe
 * @since 2024/08/19 16:14
 */
@Service
@RequiredArgsConstructor
public class StoreCouponServiceImpl extends BaseServiceImpl<StoreCouponMapper, StoreCouponDO, StoreCouponResp, StoreCouponDetailResp, StoreCouponQuery, StoreCouponReq> implements StoreCouponService {
    //    private final StoreProductService storeProductService;
    private final CategoryService categoryService;

    /**
     * 新增优惠券表
     * 
     * @param request StoreCouponReq 新增参数
     */
    @Override
    public Boolean create(StoreCouponReq request) {
        StoreCouponDO storeCoupon = new StoreCouponDO();
        BeanUtils.copyProperties(request, storeCoupon);
        // 验证
        validateCouponData(storeCoupon, request);
        // 设置时间
        setCouponTimeFields(storeCoupon, request);

        return save(storeCoupon);
    }

    private void validateCouponData(StoreCouponDO storeCoupon, StoreCouponReq request) {
        if (storeCoupon.getIsLimited()) {
            CheckUtils.throwIf(storeCoupon.getTotal() == null || storeCoupon.getTotal() == 0, "请输入数量！");
        }

        if (request.getUseType() > 1) {
            CheckUtils.throwIf(StrUtil.isBlank(request.getPrimaryKey()), "请选择商品/分类！");
        }

        if (request.getIsForever()) {
            CheckUtils.throwIf(storeCoupon.getReceiveStartTime() == null || storeCoupon
                .getReceiveEndTime() == null, "请选择领取时间范围！");
            int compareDate = DateUtils.compareDate(DateUtils.dateToStr(storeCoupon
                .getReceiveStartTime(), StoreConstants.DATE_FORMAT), DateUtils.dateToStr(storeCoupon
                    .getReceiveEndTime(), StoreConstants.DATE_FORMAT), StoreConstants.DATE_FORMAT);
            CheckUtils.throwIf(compareDate > -1, "请选择正确的领取时间范围！");
        }

        if (!request.getIsFixedTime()) {
            CheckUtils.throwIf(storeCoupon.getDay() == null || storeCoupon.getDay() == 0, "请输入天数！");
        }
    }

    private void setCouponTimeFields(StoreCouponDO storeCoupon, StoreCouponReq request) {
        storeCoupon.setLastTotal(storeCoupon.getTotal());

        if (!request.getIsForever()) {
            storeCoupon.setReceiveStartTime(DateUtils.nowDateTime());
        }

        if (!request.getIsFixedTime()) {
            storeCoupon.setUseStartTime(null);
            storeCoupon.setUseEndTime(null);
        }
    }

    //    public Boolean create(StoreCouponReq request) {
    //        StoreCouponDO storeCoupon = new StoreCouponDO();
    //        BeanUtils.copyProperties(request, storeCoupon);
    //
    //        CheckUtils.throwIf((storeCoupon.getIsLimited() && (storeCoupon.getTotal() == null || storeCoupon
    //                .getTotal() == 0)), "请输入数量！");
    //        CheckUtils.throwIf((request.getUseType() > 1 && (StrUtil.isBlank(request.getPrimaryKey()))), "请选择商品/分类！");
    //
    //        storeCoupon.setLastTotal(storeCoupon.getTotal());
    //        if (!request.getIsForever()) {
    //            //开始时间设置为当前时间
    //            storeCoupon.setReceiveStartTime(DateUtils.nowDateTime());
    //        } else {
    //
    //            CheckUtils.throwIf((storeCoupon.getReceiveStartTime() == null || storeCoupon
    //                    .getReceiveEndTime() == null), "请选择领取时间范围！");
    //            int compareDate = DateUtils.compareDate(DateUtils.dateToStr(storeCoupon
    //                    .getReceiveStartTime(), StoreConstants.DATE_FORMAT), DateUtils.dateToStr(storeCoupon
    //                    .getReceiveEndTime(), StoreConstants.DATE_FORMAT), StoreConstants.DATE_FORMAT);
    //
    //            CheckUtils.throwIf((compareDate > -1), "请选择正确的领取时间范围！");
    //        }
    //
    //        //非固定时间, 领取后多少天
    //        if (!request.getIsFixedTime()) {
    //            CheckUtils.throwIf((storeCoupon.getDay() == null || storeCoupon.getDay() == 0), "请输入天数！");
    //            storeCoupon.setUseStartTime(null);
    //            storeCoupon.setUseEndTime(null);
    //        }
    //        return save(storeCoupon);
    //    }
    //

    /**
     * 获取详情
     * 
     * @param id Long id
     * @return StoreCoupon
     */
    @Override
    public StoreCouponDO getInfoException(Long id) {
        //获取优惠券信息
        StoreCouponDO storeCoupon = getById(id);
        checkException(storeCoupon);

        return storeCoupon;
    }

    /**
     * 检测当前优惠券是否正常
     * 
     * @param storeCoupon StoreCoupon 优惠券对象`
     * @author Mr.Zhang
     * @since 2020-05-18
     */
    private void checkException(StoreCouponDO storeCoupon) {

        CheckUtils.throwIf((storeCoupon == null || storeCoupon.getIsDel() || !storeCoupon.getStatus()), "请选择领取时间范围！");

        //看是否过期
        if (!(storeCoupon.getReceiveEndTime() == null || "".equals(storeCoupon.getReceiveEndTime()))) {
            //非永久可领取
            String date = DateUtils.nowDateTimeStr();
            int result = DateUtils.compareDate(date, DateUtils.dateToStr(storeCoupon
                .getReceiveEndTime(), StoreConstants.DATE_FORMAT), StoreConstants.DATE_FORMAT);

            CheckUtils.throwIf((result == 1), "已超过优惠券领取最后期限！");

        }

        //看是否有剩余数量
        if (storeCoupon.getIsLimited()) {
            //考虑到并发溢出的问题用大于等于
            CheckUtils.throwIf((storeCoupon.getLastTotal() < 1), "此优惠券已经被领完了！");
        }
    }

    /**
     * 优惠券详情
     * 
     * @param id Integer 获取可用优惠券的商品id
     * @return StoreCouponInfoResponse
     */
    @Override
    public StoreCouponResp info(Long id) {
        StoreCouponDO storeCoupon = getById(id);

        CheckUtils.throwIf((ObjectUtil.isNull(storeCoupon) || storeCoupon.getIsDel() || !storeCoupon
            .getStatus()), "优惠券信息不存在或者已失效！");

        List<StoreProductResp> productList = null;
        List<CategoryDO> categoryList = null;
        //        if (StrUtil.isNotBlank(storeCoupon.getPrimaryKey()) && storeCoupon.getUseType() > 1) {
        //            List<Long> primaryIdList = CommonUtil.stringToArrayLong(storeCoupon.getPrimaryKey());
        //            if (storeCoupon.getUseType() == 2) {
        //                productList = storeProductService.getListInIds(primaryIdList);
        //            }
        //            if (storeCoupon.getUseType() == 3) {
        //                categoryList = categoryService.getByIds(primaryIdList);
        //            }
        //        }

        StoreCouponResp coupon = new StoreCouponResp();
        BeanUtils.copyProperties(storeCoupon, coupon);
        coupon.setIsForever(false);
        if (ObjectUtil.isNotNull(coupon.getReceiveEndTime())) {
            coupon.setIsForever(true);
        }

        coupon.setProduct(productList);
        coupon.setCategory(categoryList);
        return coupon;
    }

    /**
     * 根据优惠券id获取
     * 
     * @param ids 优惠券id集合
     * @return List<StoreCoupon>
     */
    @Override
    public List<StoreCouponResp> getByIds(List<Long> ids) {
        return baseMapper.getByIds(ids);
    }

    /**
     * 扣减数量
     * 
     * @param id        优惠券id
     * @param num       数量
     * @param isLimited 是否限量
     */
    @Override
    public void deduction(Long id, Integer num, Boolean isLimited) {
        baseMapper.deduction(id, num, isLimited);
    }

    /**
     * 删除优惠券
     * 
     * @param id 优惠券id
     * @return Boolean
     */
    @Override
    public void deleteCoupon(Long id) {
        baseMapper.deleteCoupon(id);
    }

}