/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service;

import top.continew.starter.extension.crud.service.BaseService;
import top.continew.admin.common.model.query.StoreProductRelationQuery;
import top.continew.admin.common.model.req.StoreProductRelationReq;
import top.continew.admin.common.model.resp.StoreProductRelationDetailResp;
import top.continew.admin.common.model.resp.StoreProductRelationResp;

import java.util.List;

/**
 * 商品点赞和收藏业务接口
 *
 * @author joe
 * @since 2024/08/18 15:12
 */
public interface StoreProductRelationService extends BaseService<StoreProductRelationResp, StoreProductRelationDetailResp, StoreProductRelationQuery, StoreProductRelationReq> {
    List<StoreProductRelationResp> getList(Long productId, String type);

    List<StoreProductRelationResp> getLikeOrCollectByUser(Long userId, Long productId, String type);

    Integer getCollectCountByUid(Long uid);

    void deleteByProId(Long productId);

    void deleteByProIdAndUid(Long productId, Long uid);

    Integer getCountByDate(String date);

    Integer getCountByDateAndProId(Long productId, String date);
}