/*
 * Copyright (c) 2022-present Charles7c Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.continew.admin.mgr.ums.service.impl;

import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import top.continew.admin.mgr.ums.service.StoreProductDescriptionService;
import top.continew.starter.extension.crud.service.impl.BaseServiceImpl;
import top.continew.admin.common.mapper.StoreProductDescriptionMapper;
import top.continew.admin.common.model.entity.StoreProductDescriptionDO;
import top.continew.admin.common.model.query.StoreProductDescriptionQuery;
import top.continew.admin.common.model.req.StoreProductDescriptionReq;
import top.continew.admin.common.model.resp.StoreProductDescriptionDetailResp;
import top.continew.admin.common.model.resp.StoreProductDescriptionResp;

/**
 * 商品描述业务实现
 *
 * @author joe
 * @since 2024/08/18 14:17
 */
@Service
@RequiredArgsConstructor
public class StoreProductDescriptionServiceImpl extends BaseServiceImpl<StoreProductDescriptionMapper, StoreProductDescriptionDO, StoreProductDescriptionResp, StoreProductDescriptionDetailResp, StoreProductDescriptionQuery, StoreProductDescriptionReq> implements StoreProductDescriptionService {

    /**
     * 根据商品id和type删除对应描述
     * 
     * @param productId 商品id
     * @param type      类型
     */
    @Override
    public void deleteByProductId(Long productId, Integer type) {
        baseMapper.deleteByProductId(productId, type);
    }

    /**
     * 获取详情
     * 
     * @param productId 商品id
     * @param type      商品类型
     * @return StoreProductDescription
     */
    @Override
    public StoreProductDescriptionResp getByProductIdAndType(Long productId, Integer type) {
        return baseMapper.getByProductIdAndType(productId, type);
    }

    @Override
    public void saveSingle(StoreProductDescriptionDO entity) {
        baseMapper.insert(entity);
    }

}