import{A as s,as as e}from"./index-7XqkrlSF.js";function o(o){const r=()=>e(o),t=s(r());return{form:t,resetForm:()=>{for(const s in t)delete t[s];Object.assign(t,r())}}}export{o as u};
