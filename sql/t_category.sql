DROP TABLE IF EXISTS `t_category`;
CREATE TABLE `t_category`
(
    `id`          bigint auto_increment comment 'ID',
    `pid`         bigint UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级ID',
    `path`        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '/0/' COMMENT '路径',
    `name`        varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '分类名称',
    `type`        smallint(2) NULL DEFAULT 1 COMMENT '类型，1 产品分类，2 附件分类，3 文章分类， 4 设置分类， 5 菜单分类，6 配置分类， 7 秒杀配置',
    `url`         varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '地址',
    `extra`       text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '扩展字段 Jsos格式',
    `status`      tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态, 1正常，0失效',
    `sort`        int(5) NOT NULL DEFAULT 99999 COMMENT '排序',
    `create_time` timestamp(0)                                                  NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp(0)                                                  NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP (0) COMMENT '更新时间',
    update_user   bigint null comment '修改人',
    create_user   bigint null comment '创建人',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX         `status+pid`(`pid`, `status`) USING BTREE,
    INDEX         `id+status+url`(`path`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 742 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '分类表' ROW_FORMAT = Dynamic;
