
DROP TABLE IF EXISTS `t_store_coupon_user`;
CREATE TABLE `t_store_coupon_user`
(
    id            bigint auto_increment comment 'ID',

    `coupon_id` bigint NOT NULL COMMENT '优惠券发布id',
    `cid` bigint UNSIGNED NOT NULL DEFAULT 0 COMMENT '兑换的项目id',
    `uid` bigint UNSIGNED NOT NULL DEFAULT 0 COMMENT '领取人id',
    `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '优惠券名称',
    `money` decimal(8, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '优惠券的面值',
    `min_price` decimal(8, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '最低消费多少金额可用优惠券',
    `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'send' COMMENT '获取方式，send后台发放, 用户领取 get',
    `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态（0：未使用，1：已使用, 2:已失效）',
    `start_time` timestamp(0) NULL DEFAULT NULL COMMENT '开始使用时间',
    `end_time` timestamp(0) NULL DEFAULT NULL COMMENT '过期时间',
    `use_time` timestamp(0) NULL DEFAULT NULL COMMENT '使用时间',
    `use_type` tinyint(1) NULL DEFAULT 1 COMMENT '使用类型 1 全场通用, 2 商品券, 3 品类券',
    `primary_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属商品id / 分类id',

    update_user   bigint null comment '修改人',
    create_user   bigint null comment '创建人',
    `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp(0) COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `cid`(`cid`) USING BTREE,
    INDEX `uid`(`uid`) USING BTREE,
    INDEX `end_time`(`end_time`) USING BTREE,
    INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '优惠券记录表' ROW_FORMAT = Compact;
