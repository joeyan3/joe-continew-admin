
DROP TABLE IF EXISTS `t_store_product_description`;
CREATE TABLE `t_store_product_description`
(
    id            bigint auto_increment comment 'ID',
    `product_id` bigint NOT NULL DEFAULT 0 COMMENT '商品ID',
    `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品详情',
    `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '商品类型',

    update_user   bigint null comment '修改人',
    create_user   bigint null comment '创建人',
    `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp(0),
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品描述表' ROW_FORMAT = Compact;
