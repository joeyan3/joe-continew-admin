DROP TABLE IF EXISTS `t_shipping_templates`;
CREATE TABLE `t_shipping_templates`
(

    id            bigint auto_increment comment 'ID',

    update_user   bigint null comment '修改人',
    create_user   bigint null comment '创建人',

    `name`        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板名称',
    `type`        tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT '计费方式',
    `appoint`     tinyint(1) NOT NULL DEFAULT 0 COMMENT '指定包邮',
    `sort`        int(11) NOT NULL DEFAULT 0 COMMENT '排序',
    `create_time` timestamp(0)                                                  NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp(0)                                                  NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP (0) COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '运费模板' ROW_FORMAT = Dynamic;
