DROP TABLE IF EXISTS `t_store_product_attr`;
CREATE TABLE `t_store_product_attr`
(
    `id`          bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `product_id`  bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品ID',
    `attr_name`   varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NOT NULL COMMENT '属性名',
    `attr_values` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '属性值',
    `type`        tinyint(1) NULL DEFAULT 0 COMMENT '活动类型 0=商品，1=秒杀，2=砍价，3=拼团',
    `is_del`      tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除,0-否，1-是',
    update_user   bigint null comment '修改人',
    create_user   bigint null comment '创建人',
    `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp(0),

    PRIMARY KEY (`id`) USING BTREE,
    INDEX         `store_id`(`product_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品属性表' ROW_FORMAT = Compact;
