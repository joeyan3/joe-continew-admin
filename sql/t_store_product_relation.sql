
DROP TABLE IF EXISTS `t_store_product_relation`;
CREATE TABLE `t_store_product_relation`
(
    id            bigint auto_increment comment 'ID',
    `uid` bigint UNSIGNED NOT NULL COMMENT '用户ID',
    `product_id` bigint UNSIGNED NOT NULL COMMENT '商品ID',
    `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型(收藏(collect）、点赞(like))',
    `category` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '某种类型的商品(普通商品、秒杀商品)',

    update_user   bigint null comment '修改人',
    create_user   bigint null comment '创建人',
    `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp(0),
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品点赞和收藏表' ROW_FORMAT = Dynamic;
