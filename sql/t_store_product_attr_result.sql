DROP TABLE IF EXISTS `t_store_product_attr_result`;
CREATE TABLE `t_store_product_attr_result`
(
    `id`          bigint                                                  NOT NULL AUTO_INCREMENT COMMENT '主键',
    `product_id`  bigint UNSIGNED                                          NOT NULL COMMENT '商品ID',
    `result`      longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品属性参数',
    `change_time` int(10) UNSIGNED                                          NOT NULL COMMENT '上次修改时间',
    `type`        tinyint(1)                                                NULL DEFAULT 0 COMMENT '活动类型 0=商品，1=秒杀，2=砍价，3=拼团',
    update_user   bigint null comment '修改人',
    create_user   bigint null comment '创建人',
    `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp(0),

    PRIMARY KEY (`id`) USING BTREE,
    INDEX `product_id` (`product_id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '商品属性详情表'
  ROW_FORMAT = Compact;
