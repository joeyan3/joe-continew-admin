
DROP TABLE IF EXISTS `t_store_product_log`;
CREATE TABLE `t_store_product_log`
(
    id            bigint auto_increment comment 'ID',
    `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型visit,cart,order,pay,collect,refund',
    `product_id` bigint NOT NULL DEFAULT 0 COMMENT '商品ID',
    `uid` bigint NOT NULL DEFAULT 0 COMMENT '用户ID',
    `visit_num` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否浏览',
    `cart_num` int(11) NOT NULL DEFAULT 0 COMMENT '加入购物车数量',
    `order_num` int(11) NOT NULL DEFAULT 0 COMMENT '下单数量',
    `pay_num` int(11) NOT NULL DEFAULT 0 COMMENT '支付数量',
    `pay_price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '支付金额',
    `cost_price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '商品成本价',
    `pay_uid` int(11) NOT NULL DEFAULT 0 COMMENT '支付用户ID',
    `refund_num` int(11) NOT NULL DEFAULT 0 COMMENT '退款数量',
    `refund_price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '退款金额',
    `collect_num` tinyint(1) NOT NULL DEFAULT 0 COMMENT '收藏',
    `add_time` bigint(14) NOT NULL DEFAULT 0 COMMENT '添加时间',

    update_user   bigint null comment '修改人',
    create_user   bigint null comment '创建人',
    `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp(0),
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品日志表' ROW_FORMAT = Compact;
