DROP TABLE IF EXISTS `t_store_coupon`;
CREATE TABLE `t_store_coupon`
(
    `id`                 bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '优惠券表ID',
    `name`               varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL DEFAULT '' COMMENT '优惠券名称',
    `money`              decimal(8, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '兑换的优惠券面值',
    `is_limited`         tinyint(1) NULL DEFAULT 0 COMMENT '是否限量, 默认0 不限量， 1限量',
    `total`              int(11) NOT NULL DEFAULT 0 COMMENT '发放总数',
    `last_total`         int(11) NULL DEFAULT 0 COMMENT '剩余数量',
    `use_type`           tinyint(2) NOT NULL DEFAULT 1 COMMENT '使用类型 1 全场通用, 2 商品券, 3 品类券',
    `primary_key`        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '所属商品id / 分类id',
    `min_price`          decimal(8, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '最低消费，0代表不限制',
    `receive_start_time` timestamp(0)                                                  NOT NULL COMMENT '可领取开始时间',
    `receive_end_time`   timestamp(0) NULL DEFAULT NULL COMMENT '可领取结束时间',
    `is_fixed_time`      tinyint(1) NULL DEFAULT 0 COMMENT '是否固定使用时间, 默认0 否， 1是',
    `use_start_time`     timestamp(0) NULL DEFAULT NULL COMMENT '可使用时间范围 开始时间',
    `use_end_time`       timestamp(0) NULL DEFAULT NULL COMMENT '可使用时间范围 结束时间',
    `day`                int(4) NULL DEFAULT 0 COMMENT '天数',
    `type`               tinyint(2) NOT NULL DEFAULT 1 COMMENT '优惠券类型 1 手动领取, 2 新人券, 3 赠送券',
    `sort`               int(11) UNSIGNED NOT NULL DEFAULT 1 COMMENT '排序',
    `status`             tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态（0：关闭，1：开启）',
    `is_del`             tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除 状态（0：否，1：是）',

    update_user   bigint null comment '修改人',
    create_user   bigint null comment '创建人',
    `create_time`        timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`        timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP (0) COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX                `state`(`status`) USING BTREE,
    INDEX                `is_del`(`is_del`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '优惠券表' ROW_FORMAT = Dynamic;
