DROP TABLE IF EXISTS `t_shipping_templates_free`;
CREATE TABLE `t_shipping_templates_free`
(

    id            bigint auto_increment comment 'ID',

    update_user   bigint null comment '修改人',
    create_user   bigint null comment '创建人',

    `temp_id` bigint(11) NOT NULL DEFAULT 0 COMMENT '模板ID',
    `city_id` int(11) NOT NULL DEFAULT 0 COMMENT '城市ID',
    `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '描述',
    `number` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '包邮件数',
    `price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '包邮金额',
    `type`   tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT '计费方式',
    `uniqid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分组唯一值',
    `status` tinyint(1) NULL DEFAULT 0 COMMENT '是否无效',

    `create_time` timestamp(0)                                                  NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp(0)                                                  NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP (0) COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '运费模板包邮' ROW_FORMAT = Dynamic;
