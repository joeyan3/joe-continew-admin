
DROP TABLE IF EXISTS `t_store_product_coupon`;
CREATE TABLE `t_store_product_coupon`
(
    id            bigint auto_increment comment 'ID',
    `product_id` bigint NOT NULL DEFAULT 0 COMMENT '商品id',
    `issue_coupon_id` bigint NOT NULL DEFAULT 0 COMMENT '优惠劵id',
    `add_time` int(10) NOT NULL DEFAULT 0 COMMENT '添加时间',

    update_user   bigint null comment '修改人',
    create_user   bigint null comment '创建人',
    `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp(0) COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品优惠券表' ROW_FORMAT = Compact;
